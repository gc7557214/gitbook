# Create a Wallet



<figure><img src="https://1397868517-files.gitbook.io/~/files/v0/b/gitbook-x-prod.appspot.com/o/spaces%2F-MHREX7DHcljbY5IkjgJ-1972196547%2Fuploads%2FK6UvFRTZMlzkSlkW9jrB%2Fhow-to-make-a-crypto-wallet-header.png?alt=media&#x26;token=5a31463f-f214-4f7f-a6dd-e8e17e6095b9" alt=""><figcaption></figcaption></figure>

To get started on your PancakeSwap journey on Aptos, the first thing you'll need is to set up a wallet that supports Aptos Chain. Wallets are available both on desktop computers and on smartphone devices. You'll need to choose the wallet that fits your needs best.

{% hint style="danger" %}
**When you're setting up a wallet, be sure to:**

* ✅ **Download and install only the latest version from an official source.**
* ✅ **Follow the setup guide carefully.**
* ✅ **Safely back up your recovery phrases.**
* ❌ **NEVER share your recovery phrases with anyone, under any circumstances.**
* ❌ **NEVER input your recovery phrase to a website or app, other than your wallet app.**
{% endhint %}



## Smartphone/Mobile or Desktop wallet? <a href="#smartphone-mobile-or-desktop-wallet" id="smartphone-mobile-or-desktop-wallet"></a>

{% hint style="warning" %}
At the time of writing, there are a limited number of mobile wallets with Aptos support. Chefs will include more wallets in the tutorial when they are available.

**We recommend using** [**desktop wallets**](https://docs.pancakeswap.finance/get-started-aptos/wallet-guide#desktop-web-browser-wallets) **until more mobile wallets are ready.**
{% endhint %}



Mobile device wallets and desktop-based wallets have different strengths and weaknesses. Consider which fits your needs better to help decide which type of wallet to use.

| Text                              | Mobile | Desktop |
| --------------------------------- | ------ | ------- |
| Use anywhere                      | ✅      | ➖       |
| Easy to use                       | ✅      | ➖       |
| More secure                       | ➖      | ✅       |
| Accessibility friendly            | ➖      | ✅       |
| Damage/loss/theft resistant       | ➖      | ✅       |
| Power/connection outage resistant | ✅      | ➖       |

## **Smartphone/Mobile wallets** <a href="#smartphone-mobile-wallets" id="smartphone-mobile-wallets"></a>

Smartphone/Mobile wallets allow you to access your crypto almost anywhere. Wallets are available on both Android and iOS devices.

### Which mobile wallet should I choose? <a href="#which-mobile-wallet-should-i-choose" id="which-mobile-wallet-should-i-choose"></a>

This comparison table gives an overview of the most popular mobile wallets used with PancakeSwap.

| Text                       | SafePal         | Trust Wallet | Blocto |
| -------------------------- | --------------- | ------------ | ------ |
| Aptos Chain support        | ✅               | ✅            | ✅      |
| Built-in DApp browser      | ✅               | ✅ (iOS only) | ✅      |
| Hardware wallet compatible | ✅(Only SafePal) | ➖            | ➖      |
| Open source (auditability) | ➖               | ✅            | ➖      |

You can find more in-depth information about each wallet below, as well as download links and installation guides.

{% tabs %}
{% tab title="SafePal" %}
<figure><img src="https://files.gitbook.com/v0/b/gitbook-x-prod.appspot.com/o/spaces%2F-MHREX7DHcljbY5IkjgJ-1972196547%2Fuploads%2FZuCcaLK4UtU3jCU0MDaz%2F7cccc2d2-c9db-4d38-8641-5a94cfa6083d_4x.png.webp?alt=media&#x26;token=4091057f-c068-482f-8cf5-e24f856b971d" alt=""><figcaption><p>SafePal is available as both a software and hardware wallet. The wallet is easy to install and create, and comes ready to support Aptos, BEP2 (BNB Beacon Chain)<strong>,</strong> BEP20 (BNB Smart Chain) right away.​</p></figcaption></figure>

**Pros:**

* Easily switch between several crypto networks
* Has the option for Google Authenticator built in
* Supports a large number of languages
* Setup process is very simple
* Is available as both a software and hardware wallet (work together)

**Cons:**

* Isn't fully open source
* Does not give user notifications
* Does not enforce strong security during initial setup and wallet creation

​[**Download SafePal**](https://safepal.io/download) (Automatically detects device)

[**SafePal Setup Guide for Aptos**](https://safepalsupport.zendesk.com/hc/en-us/articles/10061372532891-How-to-add-send-Aptos-mainnet-coin-APT-with-SafePal-Software-Wallet)\

{% endtab %}

{% tab title="Trust Wallet" %}


<figure><img src="../.gitbook/assets/image.png" alt=""><figcaption></figcaption></figure>

Trust Wallet is a popular wallet for smart devices. It supports DApps, NFT collectibles (with Opensea.io support), and in-wallet staking.

**Pros:**

* Open source for auditability
* Backed by the Binance cryptocurrency exchange
* Easily switch between several crypto networks
* Includes Aptos networks by default

**Cons:**

* Currently only iOS version supports Aptos
* Language tied to phone settings
* Limited DApps
* Transaction notifications can be delayed

[**Download Trust Wallet**](https://trustwallet.com) (Automatically detects device)\
[**Trust Wallet Setup Guide for Aptos**](https://community.trustwallet.com/t/trust-wallet-adds-native-support-for-aptos-apt/575701)
{% endtab %}

{% tab title="Blocto" %}
![](https://1397868517-files.gitbook.io/\~/files/v0/b/gitbook-x-prod.appspot.com/o/spaces%2F-MHREX7DHcljbY5IkjgJ-1972196547%2Fuploads%2FJm0EBkdufu2cTXRSuvCx%2Fimage.png?alt=media\&token=1cb835f4-b411-4d53-9d7a-c8a4f287c8ad)



Blocto offers a user-friendly solution and an all-in-one package for using Dapps and managing your assets.

**Highlights:**

* Supports both BNB Chain and Aptos Chain and other major crypto networks out-of-the-box.&#x20;
* Easy to set up.
* Supports both iOS and Android.
* Email login.
* Smart contract wallet eliminates the need for managing your own private key and also provides advanced features like account recovery
* The initial setup gas fee for the smart contract wallet is covered

**Note:**

* **Requires Email address to get started**
* Custodial model
* A bit complicated if you want to export your private key and manage it or use it in other wallets. [Tutorial](https://portto.zendesk.com/hc/en-us/articles/4411564072217-Can-I-export-my-private-key-from-Blocto-and-use-it-in-Metamask-Phantom-Trust-Wallet-).

****[**Download Blocto Wallet**](https://portto.com/download)

### **Blocto Wallet Setup Guide**

1 - Set up by entering your Email address. Then enter the one-time password received by Email.



<img src="https://1397868517-files.gitbook.io/~/files/v0/b/gitbook-x-prod.appspot.com/o/spaces%2F-MHREX7DHcljbY5IkjgJ-1972196547%2Fuploads%2FWy3RzBqYSwmwB1G9jkBl%2Fblocto-setup-guide-mobile-1.png?alt=media&#x26;token=e5f59655-3a52-4b0c-81ea-2d040adf1b66" alt="" data-size="original">![](https://1397868517-files.gitbook.io/\~/files/v0/b/gitbook-x-prod.appspot.com/o/spaces%2F-MHREX7DHcljbY5IkjgJ-1972196547%2Fuploads%2F7NfRtODQRNoVgIzxnu4I%2Fblocto-setup-guide-mobile-2.png?alt=media\&token=16bdf791-f12b-4157-9821-8a0e24aeedc7)



2 - Click the plus button, then choose "APT - Aptos"



![](https://1397868517-files.gitbook.io/\~/files/v0/b/gitbook-x-prod.appspot.com/o/spaces%2F-MHREX7DHcljbY5IkjgJ-1972196547%2Fuploads%2FwBMyU3MDLIRXxAuHD9oO%2Fblocto-setup-guide-mobile-3.png?alt=media\&token=b8281b4e-bfee-401f-877c-8b0ba968ba5d)![](https://1397868517-files.gitbook.io/\~/files/v0/b/gitbook-x-prod.appspot.com/o/spaces%2F-MHREX7DHcljbY5IkjgJ-1972196547%2Fuploads%2FnunqnDvXzpdBSKAF9la4%2Fblocto-setup-guide-mobile-4.png?alt=media\&token=f1808ab6-3651-4c8f-940a-164806d99dcf)



3 - Done! You can now deposit APT into the Aptos wallet and start using PancakeSwap on Aptos by going to the "Discover" page!

![](https://1397868517-files.gitbook.io/\~/files/v0/b/gitbook-x-prod.appspot.com/o/spaces%2F-MHREX7DHcljbY5IkjgJ-1972196547%2Fuploads%2FzuXdbfCHKQFyUyYJwpfk%2Fblocto-setup-guide-mobile-6.png?alt=media\&token=372ce53b-855c-4014-9e24-3ab089ee57e4)![](https://1397868517-files.gitbook.io/\~/files/v0/b/gitbook-x-prod.appspot.com/o/spaces%2F-MHREX7DHcljbY5IkjgJ-1972196547%2Fuploads%2FgyitonegWl1YTbDr3Sim%2Fblocto-setup-guide-mobile-7.png?alt=media\&token=983156cb-1310-495f-b022-8169e8a8fbba)
{% endtab %}
{% endtabs %}



## **Desktop/Web Browser wallets** <a href="#desktop-web-browser-wallets" id="desktop-web-browser-wallets"></a>

Desktop wallets are available on your home computer or laptop computer. Wallets on your computer can run as standalone applications, or as web browser plugins for popular browsers like Chrome and Firefox.

### Which desktop wallet should I choose? <a href="#which-desktop-wallet-should-i-choose" id="which-desktop-wallet-should-i-choose"></a>

This comparison table gives an overview of the most popular desktop wallets used with PancakeSwap on Aptos

| Text                       | Petra | Martian | Pontem | Fewcha | Blocto |
| -------------------------- | ----- | ------- | ------ | ------ | ------ |
| Aptos Chain support        | ✅     | ✅       | ✅      | ✅      | ✅      |
| Hardware wallet compatible | ➖     | ➖       | ➖      | ➖      | ➖      |
| Open source (auditability) | ✅     | ❓       | ✅      | ❓      | ❓      |

_❓ - as of writing, we are unsure about the status of this information_

You can find more in-depth information about each wallet below, as well as download links and installation guides.



{% tabs %}
{% tab title="Petra" %}


<figure><img src="https://1397868517-files.gitbook.io/~/files/v0/b/gitbook-x-prod.appspot.com/o/spaces%2F-MHREX7DHcljbY5IkjgJ-1972196547%2Fuploads%2FOGiueKySOjuLibicnBRy%2Fimage.png?alt=media&#x26;token=d639f7f5-ec71-45ad-a3a5-cfb047300fde" alt=""><figcaption></figcaption></figure>

Petra wallet is an extension that lets you explore Aptos in your browser.

**Highlights:**

* Built by Aptos Labs
* Easy to use
* Supports multiple accounts
* Built-in NFT support
* Supports Aptos testnet and one-click testnet APT faucet

**Note:**

* Desktop only
* Only supports Aptos chains

****[**Download Petra Wallet**](https://petra.app/)****

****[**Petra Wallet Setup Guide**](https://petra.app/docs/use)
{% endtab %}

{% tab title="Martian" %}


<figure><img src="https://1397868517-files.gitbook.io/~/files/v0/b/gitbook-x-prod.appspot.com/o/spaces%2F-MHREX7DHcljbY5IkjgJ-1972196547%2Fuploads%2FSYP3IAWpq8xmB8XXrIis%2Fimage.png?alt=media&#x26;token=ec60f757-969a-423d-b3a0-a3166dba62df" alt=""><figcaption></figcaption></figure>



Martian is an Aptos Chain wallet that allows you to store, send, receive tokens and manage & mint NFTs. Its mobile version is coming soon.

**Highlights:**

* Mobile version coming soon
* Easy to use
* Supports multiple accounts
* Built-in NFT support
* Supports Aptos testnet

****[**Download Martian Wallet**](https://martianwallet.xyz/)
{% endtab %}

{% tab title="Pontem" %}


<figure><img src="https://1397868517-files.gitbook.io/~/files/v0/b/gitbook-x-prod.appspot.com/o/spaces%2F-MHREX7DHcljbY5IkjgJ-1972196547%2Fuploads%2FZNQDhNKv5WU3liD0Akhk%2Fpontem-setup-desktop-banner.png?alt=media&#x26;token=47dc5f59-cbde-4966-8bd4-333d04da31c0" alt=""><figcaption></figcaption></figure>

Pontem Wallet is an Aptos Chain wallet that allows you to store, send, receive tokens, and connect to dApps on the Aptos ecosystem.

**Highlights:**

* Mobile version coming soon
* Easy to use
* Supports multiple accounts
* Built-in NFT support
* Supports Aptos testnet

****[**Download Pontem Wallet**](https://pontem.network/pontem-wallet)
{% endtab %}

{% tab title="Fewcha" %}


<figure><img src="https://1397868517-files.gitbook.io/~/files/v0/b/gitbook-x-prod.appspot.com/o/spaces%2F-MHREX7DHcljbY5IkjgJ-1972196547%2Fuploads%2F9ov4LFq4T9k3zAliFuf2%2Fimage.png?alt=media&#x26;token=05370825-3e40-4cf7-a0c7-035c6d13d54b" alt=""><figcaption></figcaption></figure>

Fewcha is a trusted crypto wallet optimized for Aptos and SUI ecosystem that unlocks a universe of applications in the web3.

**Highlights:**

* Support Aptos and Sui Blockchain
* First wallet to support pure blockchain using Move language
* Friendly UX/UI
* Simple, elegant and easy to use
* In-app Swapper
* In-app Stake
* Multiple languages
* EBCI Protocol - a protocol that prevents multiple pop-ups when interacting with dApp, it’ll help users manage what dApp they are signing for a transaction.
* Support Hardware Wallet Connection (Keystone Wallet)

****

****[**Download FewCha Wallet**](https://fewcha.app/)****

****

### **Fewcha wallet setup guide**

****

1\. **** Once Fewcha Wallet extension finishes downloading, a welcome tab will appear. Here you can select “Create a new wallet” (or “Use an existing wallet” if you’ve already had one.)

<figure><img src="https://lh5.googleusercontent.com/xzkPxptL1JdM_IcX3jEH8J1JpEC9uWabA5QUJzKBRIqszgGeOUh67QuQx7Y6BCzv1UIXshZuxrCCgZe6wYhxCZ4JbjcbtYIdP_zO9A6VDiQgypsbr2wlvqUjhioyPsRDVOYTBJv_smcjhODx3zHnqrTvxiJzWMhZa3e4TvG3lfAqFtE6-47N08s" alt=""><figcaption></figcaption></figure>

2\. **** Select your default blockchain. Create a password that’s hard to predict yet easy to remember. Make sure you have read and agreed to the Terms of Service and Privacy Policy. Then click “Continue”.

3\. For the Recovery Phrase, please write it down or copy to save it somewhere that you can remember.

<figure><img src="https://1397868517-files.gitbook.io/~/files/v0/b/gitbook-x-prod.appspot.com/o/spaces%2F-MHREX7DHcljbY5IkjgJ-1972196547%2Fuploads%2F9sg0FYB5fkzN09eQ8fZT%2Fimage.png?alt=media&#x26;token=da2c3e5f-2616-476d-a028-0dcfac22829c" alt=""><figcaption></figcaption></figure>

> * Safely backup your recovery phrases. If you lose your recovery phrase, you may lose access to your wallet and the crypto within it forever.&#x20;
> * **NEVER** share your recovery phrases with anyone, under any circumstances.
> * **NEVER** input your recovery phrase to a website or app, other than your wallet app**.**

<figure><img src="https://1397868517-files.gitbook.io/~/files/v0/b/gitbook-x-prod.appspot.com/o/spaces%2F-MHREX7DHcljbY5IkjgJ-1972196547%2Fuploads%2FyOJFKwyjWnVU73lq9QHx%2Fimage.png?alt=media&#x26;token=9fb00bad-962e-43b2-86aa-b265dfd1dfaf" alt=""><figcaption></figcaption></figure>

4\. Click Finish to complete this setup process. And done! Once your wallet is created, you can access it at any time by clicking on the Fewcha Wallet icon on the extension bar.&#x20;

<figure><img src="https://1397868517-files.gitbook.io/~/files/v0/b/gitbook-x-prod.appspot.com/o/spaces%2F-MHREX7DHcljbY5IkjgJ-1972196547%2Fuploads%2F5eyEl40EcBWNasMmXeQF%2Fimage.png?alt=media&#x26;token=437fdaae-b146-4607-9707-d008f396fff5" alt=""><figcaption></figcaption></figure>

To start using PancakeSwap on Aptos, you will also need to have some $APT tokens to pay gas fees. You can learn how to get some here.
{% endtab %}

{% tab title="Blocto" %}


<figure><img src="https://1397868517-files.gitbook.io/~/files/v0/b/gitbook-x-prod.appspot.com/o/spaces%2F-MHREX7DHcljbY5IkjgJ-1972196547%2Fuploads%2FJm0EBkdufu2cTXRSuvCx%2Fimage.png?alt=media&#x26;token=1cb835f4-b411-4d53-9d7a-c8a4f287c8ad" alt=""><figcaption></figcaption></figure>

Blocto offers a user-friendly solution and an all-in-one package for using Dapps and managing your assets.

**Highlights:**

* Supports both BNB Chain and Aptos Chain and other major crypto networks out-of-the-box.&#x20;
* No app or extension is required for using on desktop.
* Supports both desktop and mobile.
* Email login.
* Smart contract wallet eliminates the need for managing your own private key and also provides advanced features like account recovery
* The initial setup gas fee for the smart contract wallet is covered

**Note:**

* **Requires Email address to get started**
* Custodial model
* A bit complicated if you want to export your private key and manage it or use it in other wallets. [Tutorial](https://portto.zendesk.com/hc/en-us/articles/4411564072217-Can-I-export-my-private-key-from-Blocto-and-use-it-in-Metamask-Phantom-Trust-Wallet-).

### **Blocto wallet setup guide**

1 - After selecting "Blocto" in the wallet connection window, you should be presented with a sign-in window. Type in your Email address and click "Register". If you have previously setup a Blocto account, please type in the Email address and click "sign in".

<figure><img src="https://1397868517-files.gitbook.io/~/files/v0/b/gitbook-x-prod.appspot.com/o/spaces%2F-MHREX7DHcljbY5IkjgJ-1972196547%2Fuploads%2FnOWcn66Z21du3vGWcTh1%2Fblocto-setup-desktop-1.png?alt=media&#x26;token=d7129e0a-a9dc-4deb-8f87-ee605775ced0" alt=""><figcaption></figcaption></figure>

2 - Then enter the one-time password received by Email.

<figure><img src="https://1397868517-files.gitbook.io/~/files/v0/b/gitbook-x-prod.appspot.com/o/spaces%2F-MHREX7DHcljbY5IkjgJ-1972196547%2Fuploads%2FYDqY1jUnYtzikorEDowR%2Fblocto-setup-desktop-2.png?alt=media&#x26;token=1945f23f-bb17-448c-8eed-38756ed25ee6" alt=""><figcaption></figcaption></figure>

3 - Click "Create Aptos wallet". Then click "Confirm" to connect.

![](https://1397868517-files.gitbook.io/\~/files/v0/b/gitbook-x-prod.appspot.com/o/spaces%2F-MHREX7DHcljbY5IkjgJ-1972196547%2Fuploads%2FkweoG0VERG21frkyxiJk%2Fblocto-setup-desktop-3.png?alt=media\&token=3a95f8a3-758b-4f0d-a178-198ad147531a)![](https://1397868517-files.gitbook.io/\~/files/v0/b/gitbook-x-prod.appspot.com/o/spaces%2F-MHREX7DHcljbY5IkjgJ-1972196547%2Fuploads%2FEexidEXxUY4fAwr6sQ5K%2Fblocto-setup-desktop-4.png?alt=media\&token=a1528290-3a61-4543-81f3-46771d86a2d2)



4 - Done! Please note that you will need to fund your Aptos address with APT to start using PancakeSwap on Aptos. If you want to access the same account on other devices, simply sign in using the same Email address.


{% endtab %}
{% endtabs %}

**​**

{% hint style="danger" %}
NEVER, in any situation, should you ever give someone your private key or recovery phrase ("seed phrase"). This will give someone complete access to your crypto!The genuine PancakeSwap site and staff will never ask you to input your seed phrase.
{% endhint %}

