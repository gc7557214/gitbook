# Connect Your Wallet to PancakeSwap

Connect Your Wallet to PancakeSwap

<figure><img src="https://1397868517-files.gitbook.io/~/files/v0/b/gitbook-x-prod.appspot.com/o/spaces%2F-MHREX7DHcljbY5IkjgJ-1972196547%2Fuploads%2F1SiTrwBvVUF2EqtIzhOl%2Fhow-to-connect-wallet-header.png?alt=media&#x26;token=3cbe62fc-8324-4420-9d5e-2da8ac27c9e9" alt=""><figcaption></figcaption></figure>

## Connect your wallet <a href="#connect-your-wallet" id="connect-your-wallet"></a>

<figure><img src="https://1397868517-files.gitbook.io/~/files/v0/b/gitbook-x-prod.appspot.com/o/spaces%2F-MHREX7DHcljbY5IkjgJ-1972196547%2Fuploads%2FKC7EZ7He7k6noo48ILu5%2Fwallet-connection-aptos.png?alt=media&#x26;token=3852b18a-f962-48fc-8c63-35e067bc7226" alt=""><figcaption></figcaption></figure>

1. 1.Click "Connect Wallet" on the top-righthand corner.
2. 2.For most of the browser extension wallets, simple select their icon, and follow the instruction in the pop-up window.
3. 3.For Blocto, please use your Email address to register or sign in. To learn more, please check out [here](https://docs.pancakeswap.finance/get-started-aptos/wallet-guide).

**Remember - NEVER, under any situation, should you ever give someone your private key or recovery phrases.**

## **Connect to Aptos Mainnet** <a href="#connect-to-aptos-mainnet" id="connect-to-aptos-mainnet"></a>

Aptos has multiple networks, including multiple devnets and testnets. But to use PancakeSwap on Aptos Chain, please be sure your wallet is connected to **Aptos Mainnet**.

Here's how:

{% tabs %}
{% tab title="Petra" %}
<figure><img src="https://1397868517-files.gitbook.io/~/files/v0/b/gitbook-x-prod.appspot.com/o/spaces%2F-MHREX7DHcljbY5IkjgJ-1972196547%2Fuploads%2FCszMaKSs3h4WcbCv9vQZ%2Faptos-network-switching-petra.gif?alt=media&#x26;token=f871bdf0-81ff-4f93-aa26-b67f988f209c" alt=""><figcaption></figcaption></figure>
{% endtab %}

{% tab title="Martian" %}
<figure><img src="https://1397868517-files.gitbook.io/~/files/v0/b/gitbook-x-prod.appspot.com/o/spaces%2F-MHREX7DHcljbY5IkjgJ-1972196547%2Fuploads%2FgNCvCQSYv5OqQcUdXlOs%2Faptos-network-switching-martian.gif?alt=media&#x26;token=9e832b8a-f6c1-48b8-b661-0e5caf7c7b97" alt=""><figcaption></figcaption></figure>
{% endtab %}

{% tab title="Pontem" %}
<figure><img src="https://1397868517-files.gitbook.io/~/files/v0/b/gitbook-x-prod.appspot.com/o/spaces%2F-MHREX7DHcljbY5IkjgJ-1972196547%2Fuploads%2FU77SR8R4g5EIqSgADMIv%2Faptos-network-switching-pontem.gif?alt=media&#x26;token=6dc1f7b1-8c1b-488e-a6c0-92d1d66a1fc4" alt=""><figcaption></figcaption></figure>
{% endtab %}

{% tab title="Fewcha" %}
<figure><img src="https://1397868517-files.gitbook.io/~/files/v0/b/gitbook-x-prod.appspot.com/o/spaces%2F-MHREX7DHcljbY5IkjgJ-1972196547%2Fuploads%2Fwh3JETrmFGye4gkprykj%2Faptos-network-switching-fewcha.gif?alt=media&#x26;token=7cdf8ce0-b8c7-44c4-953b-0a6ebaf03828" alt=""><figcaption></figcaption></figure>


{% endtab %}
{% endtabs %}
