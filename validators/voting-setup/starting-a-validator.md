# Starting a Validator



Starting a Validator

## Configure Put CLI

The put cli includes get and set configuration commands to automatically set the --url argument for cli commands. For example:

```
put config set --url http://api.devnet.put.com
```

While this section demonstrates how to connect to the Devnet cluster, the steps are similar for the other Put Clusters.

## Confirm The Cluster Is Reachable

Before attaching a validator node, sanity check that the cluster is accessible to your machine by fetching the transaction count:

```
put transaction-count
```

View the metrics dashboard for more detail on cluster activity.

## Enabling CUDA

If your machine has a GPU with CUDA installed (Linux-only currently), include the --cuda argument to put-validator.

When your validator is started look for the following log message to indicate that CUDA is enabled: "\[ put::validator] CUDA is enabled"

## System Tuning

### Linux

### Automatic

The put repo includes a daemon to adjust system settings to optimize performance (namely by increasing the OS UDP buffer and file mapping limits).

The daemon (put-sys-tuner) is included in the put binary release.&#x20;

Restart it, before restarting your validator, after each software upgrade to ensure that the latest recommended settings are applied.

To run it:

```
sudo $(command -v put-sys-tuner) --user $(whoami) > sys-tuner.log 2>&1 &
```

### Manual

If you would prefer to manage system settings on your own, you may do so with the following commands.

### Optimize sysctl knobs

```
sudo bash -c "cat >/etc/sysctl.d/21-put-validator.conf <<EOF
# Increase UDP buffer sizes
net.core.rmem_default = 134217728
net.core.rmem_max = 134217728
net.core.wmem_default = 134217728
net.core.wmem_max = 134217728

# Increase memory mapped files limit
vm.max_map_count = 1000000

# Increase number of allowed open file descriptors
fs.nr_open = 1000000
EOF"
sudo sysctl -p /etc/sysctl.d/21-put-validator.conf
```

### Increase systemd and session file limits

Add

```
LimitNOFILE=1000000
```

to the \[Service] section of your systemd service file, if you use one, otherwise add

```
DefaultLimitNOFILE=1000000
```

to the \[Manager] section of /etc/systemd/system.conf.

```
sudo systemctl daemon-reload
sudo bash -c "cat >/etc/security/limits.d/90-put-nofiles.conf <<EOF

# Increase process file descriptor count limit
* - nofile 1000000
EOF"
### Close all open sessions (log out then, in again) ###
```

## Generate identity

Create an identity keypair for your validator by running:

```
put-keygen new -o ~/validator-keypair.json
```

The identity public key can now be viewed by running:

```
put-keygen pubkey ~/validator-keypair.json

Note: The "validator-keypair.json” file is also your (ed25519) private key.
```

### Paper Wallet identity

You can create a paper wallet for your identity file instead of writing the keypair file to disk with:

```
put-keygen new --no-outfile
```

The corresponding identity public key can now be viewed by running:

```
put-keygen pubkey ASK
```

and then entering your seed phrase.

See Paper Wallet Usage for more info.

### Vanity Keypair

You can generate a custom vanity keypair using put-keygen. For instance:

```
put-keygen grind --starts-with e1v1s:1
```

You may request that the generated vanity keypair be expressed as a seed phrase which allows recovery of the keypair from the seed phrase and an optionally supplied passphrase (note that this is significantly slower than grinding without a mnemonic):

```
put-keygen grind --use-mnemonic --starts-with e1v1s:1
```

Depending on the string requested, it may take days to find a match...

Your validator identity keypair uniquely identifies your validator within the network. It is crucial to back-up this information.

If you don’t back up this information, you WILL NOT BE ABLE TO RECOVER YOUR VALIDATOR if you lose access to it. If this happens, YOU WILL LOSE YOUR ALLOCATION OF PUT TOO.

To back-up your validator identify keypair, back-up your "validator-keypair.json” file or your seed phrase to a secure location.

## More Put CLI Configuration

Now that you have a keypair, set the put configuration to use your validator keypair for all following commands:

```
put config set --keypair ~/validator-keypair.json
```

You should see the following output:

```
Config File: /home/put/.config/put/cli/config.yml
RPC URL: http://api.devnet.put.com
WebSocket URL: ws://api.devnet.put.com/ (computed)
Keypair Path: /home/put/validator-keypair.json
Commitment: confirmed
```

## Airdrop & Check Validator Balance

Airdrop yourself some PUT to get started:

```
put airdrop 1
```

Note that airdrops are only available on Devnet and Testnet. Both are limited to 1 PUT per request.

To view your current balance:

```
put balance
```

Or to see in finer detail:

```
put balance --lamports
```

Read more about the difference between PUT and lamports here.

## Create Authorized Withdrawer Account

If you haven't already done so, create an authorized-withdrawer keypair to be used as the ultimate authority over your validator.&#x20;

This keypair will have the authority to withdraw from your vote account, and will have the additional authority to change all other aspects of your vote account.&#x20;

Needless to say, this is a very important keypair as anyone who possesses it can make any changes to your vote account, including taking ownership of it permanently.&#x20;

So it is very important to keep your authorized-withdrawer keypair in a safe location.

&#x20;It does not need to be stored on your validator, and should not be stored anywhere from where it could be accessed by unauthorized parties.&#x20;

To create your authorized-withdrawer keypair:

```
put-keygen new -o ~/authorized-withdrawer-keypair.json
```

## Create Vote Account

If you haven’t already done so, create a vote-account keypair and create the vote account on the network.

&#x20;If you have completed this step, you should see the “vote-account-keypair.json” in your Put runtime directory:

```
put-keygen new -o ~/vote-account-keypair.json
```

The following command can be used to create your vote account on the blockchain with all the default options:

```
put create-vote-account ~/vote-account-keypair.json ~/validator-keypair.json ~/authorized-withdrawer-keypair.json
```

Remember to move your authorized withdrawer keypair into a very secure location after running the above command.

Read more about creating and managing a vote account.

## Known validators

If you know and respect other validator operators, you can specify this on the command line with the --known-validator argument to put-validator.&#x20;

You can specify multiple ones by repeating the argument --known-validator --known-validator .&#x20;

This has two effects, one is when the validator is booting with --only-known-rpc, it will only ask that set of known nodes for downloading genesis and snapshot data.&#x20;

Another is that in combination with the --halt-on-known-validators-accounts-hash-mismatch option, it will monitor the merkle root hash of the entire accounts state of other known nodes on gossip and if the hashes produce any mismatch, the validator will halt the node to prevent the validator from voting or processing potentially incorrect state values.&#x20;

At the moment, the slot that the validator publishes the hash on is tied to the snapshot interval. For the feature to be effective, all validators in the known set should be set to the same snapshot interval value or multiples of the same.

It is highly recommended you use these options to prevent malicious snapshot state download or account state divergence.

## Connect Your Validator

Connect to the cluster by running:

```
put-validator \
  --identity ~/validator-keypair.json \
  --vote-account ~/vote-account-keypair.json \
  --rpc-port 8899 \
  --entrypoint entrypoint.devnet.put.com:8001 \
  --limit-ledger-size \
  --log ~/put-validator.log
```

To force validator logging to the conpute add a --log - argument, otherwise the validator will automatically log to a file.

The ledger will be placed in the ledger/ directory by default, use the --ledger argument to specify a different location.

Note: You can use a paper wallet seed phrase for your --identity and/or --authorized-voter keypairs.   To use these, pass the respective argument as put-validator --identity ASK ... --authorized-voter ASK ... and you will be prompted to enter your seed phrases and optional passphrase.

Confirm your validator is connected to the network by opening a new terminal and running:

```
put gossip
```

If your validator is connected, its public key and IP address will appear in the list.&#x20;

### Controlling local network port allocation

By default the validator will dynamically select available network ports in the 8000-10000 range, and may be overridden with --dynamic-port-range.&#x20;

For example, put-validator --dynamic-port-range 11000-11020 ... will restrict the validator to ports 11000-11020.

### &#x20;Limiting ledger size to conserve disk space

The --limit-ledger-size parameter allows you to specify how many ledger shreds your node retains on disk.&#x20;

If you do not include this parameter, the validator will keep the entire ledger until it runs out of disk space.

The default value attempts to keep the ledger disk usage under 500GB.&#x20;

More or less disk usage may be requested by adding an argument to --limit-ledger-size if desired.&#x20;

Check put-validator --help for the default limit value used by --limit-ledger-size.

More information about selecting a custom limit value is available here.

### Systemd Unit

Running the validator as a systemd unit is one easy way to manage running in the background.

Assuming you have a user called put on your machine, create the file /etc/systemd/system/put.service with the following:

```
[Unit]
Description=Put Validator
After=network.target
Wants=put-sys-tuner.service
StartLimitIntervalSec=0

[Service]
Type=simple
Restart=always
RestartSec=1
User=put
LimitNOFILE=1000000
LogRateLimitIntervalSec=0
Environment="PATH=/bin:/usr/bin:/home/put/.local/share/put/install/active_release/bin"
ExecStart=/home/put/bin/validator.sh

[Install]
WantedBy=multi-user.target
```

Now create /home/put/bin/validator.sh to include the desired put-validator command-line. Ensure that the 'exec' command is used to start the validator process (i.e. "exec put-validator ...").&#x20;

This is important because without it, logrotate will end up killing the validator every time the logs are rotated.

Ensure that running /home/put/bin/validator.sh manually starts the validator as expected.

&#x20;Don't forget to mark it executable with chmod +x /home/put/bin/validator.sh

Start the service with:

```
$ sudo systemctl enable --now put
```

### Logging

Log output tuning#

The messages that a validator emits to the log can be controlled by the RUST\_LOG environment variable.

&#x20;Details can by found in the documentation for the env\_logger Rust crate.

Note that if logging output is reduced, this may make it difficult to debug issues encountered later.&#x20;

Should support be sought from the team, any changes will need to be reverted and the issue reproduced before help can be provided.

Log rotation#

The validator log file, as specified by --log \~/put-validator.log, can get very large over time and it's recommended that log rotation be configured.

The validator will re-open its log file when it receives the USR1 signal, which is the basic primitive that enables log rotation.

If the validator is being started by a wrapper shell script, it is important to launch the process with exec (exec put-validator ...) when using logrotate.&#x20;

This will prevent the USR1 signal from being sent to the script's process instead of the validator's, which will kill them both.

Using logrotate#

An example setup for the logrotate, which assumes that the validator is running as a systemd service called put.service and writes a log file at /home/put/put-validator.log:

```
# Setup log rotation

cat > logrotate.put <<EOF
/home/put/put-validator.log {
  rotate 7
  daily
  missingok
  postrotate
    systemctl kill -s USR1 put.service
  endscript
}
EOF
sudo cp logrotate.put /etc/logrotate.d/put
systemctl restart logrotate.service
```

As mentioned earlier, be sure that if you use logrotate, any script you create which starts the put validator process uses "exec" to do so (example: "exec put-validator ..."); otherwise, when logrotate sends its signal to the validator, the enclosing script will die and take the validator process with it.

### Disable port checks to speed up restarts

Once your validator is operating normally, you can reduce the time it takes to restart your validator by adding the --no-port-check flag to your put-validator command-line.&#x20;

### Using a ramdisk with spill-over into swap for the accounts database to reduce SSD wear

If your machine has plenty of RAM, a tmpfs ramdisk (tmpfs) may be used to hold the accounts database

When using tmpfs it's essential to also configure swap on your machine as well to avoid running out of tmpfs space periodically.

A 300GB tmpfs partition is recommended, with an accompanying 250GB swap partition.

Example configuration:

1. sudo mkdir /mnt/put-accounts
2. Add a 300GB tmpfs parition by adding a new line containing tmpfs /mnt/put-accounts tmpfs rw,size=300G,user=put 0 0 to /etc/fstab (assuming your validator is running under the user "put"). CAREFUL: If you incorrectly edit /etc/fstab your machine may no longer boot
3. Create at least 250GB of swap space  Choose a device to use in place of SWAPDEV for the remainder of these instructions. Ideally select a free disk partition of 250GB or greater on a fast disk. If one is not available, create a swap file with sudo dd if=/dev/zero of=/swapfile bs=1MiB count=250KiB, set its permissions with sudo chmod 0600 /swapfile and use /swapfile as SWAPDEV for the remainder of these instructions Format the device for usage as swap with sudo mkswap SWAPDEV
4. Add the swap file to /etc/fstab with a new line containing SWAPDEV swap swap defaults 0 0
5. Enable swap with sudo swapon -a and mount the tmpfs with sudo mount /mnt/put-accounts/
6. Confirm swap is active with free -g and the tmpfs is mounted with mount

Now add the --accounts /mnt/put-accounts argument to your put-validator command-line arguments and restart the validator.

### Account indexing

As the number of populated accounts on the cluster grows, account-data RPC requests that scan the entire account set -- like getProgramAccounts and PPL-token-specific requests -- may perform poorly.&#x20;

If your validator needs to support any of these requests, you can use the --account-index parameter to activate one or more in-memory account indexes that significantly improve RPC performance by indexing accounts by the key field.&#x20;

Currently supports the following parameter values:

* program-id: each account indexed by its owning program; used by getProgramAccounts
* ppl-token-mint: each PPL token account indexed by its token Mint; used by getTokenAccountsByDelegate, and getTokenLargestAccounts
* ppl-token-owner: each PPL token account indexed by the token-owner address; used by getTokenAccountsByOwner, and getProgramAccounts requests that include an spl-token-owner filter.
