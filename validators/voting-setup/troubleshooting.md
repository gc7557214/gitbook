# Troubleshooting

There is a #validator-support Discord channel available to reach other testnet participants, https://discord.gg/pquxPsq.

## Useful Links & Discussion

```
Network Explorer
Testnet Metrics Dashboard
Validator chat channels
    #validator-support General support channel for any Validator related queries.
    #testnet-announcements The single source of truth for critical information relating Testnet
Core software repo
```

Can't find what you're looking for? Send an email to ryan@put.com or reach out to @rshea#2622 on Discord.

## Blockstore

The validator blockstore rocksdb database can be inspected using the ldb tool. ldb is part of the rocksdb code base and is also available in the rocksdb-tools package.

RocksDB Administration and Data Access Tool

## Upgrade

If a new software version introduces a new column family to the blockstore, that new (empty) column will be automatically created. This is the same logic that allows a validator to start fresh without the blockstore directory.

## Downgrade

If a new column family has been introduced to the validator blockstore, a subsequent downgrade of the validator to a version that predates the new column family will cause the validator to fail while opening the blockstore during startup.



List column families:&#x20;

&#x20;   ldb --db=/rocksdb/ list\_column\_families

Warning: Please seek guidance on discord before modifying the validator blockstore.

Drop a column family:&#x20;



&#x20;   ldb --db=/rocksdb drop\_column\_family
