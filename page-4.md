# Page 4

1. Ensure both the BEP2 token and the BEP2E token both exist on each blockchain, with the same total supply. BEP2E should have 3 more methods than typical ERC20 token standard:
   * symbol(): get token symbol
   * decimals(): get the number of the token decimal digits
   * owner(): get **BEP2E contract owner’s address.** This value should be initialized in the BEP2E contract constructor so that the further binding action can verify whether the action is from the BEP2E owner.
     1. 11111
     2. 222222
        * fdf
        * gffggfgfgf
     3. 33333333
   * dfddffddffdfd
   * dfdfdffffffffff
2. Decide the initial circulation on both blockchains. Suppose the total supply is _S_, and the expected initial circulating supply on BC is _K_, then the owner should lock S-K tokens to a system controlled address on BC.
3. Equivalently, _K_ tokens is locked in the special contract on BSC, which handles major binding functions and is named as **TokenHub**. The issuer of the BEP2E token should lock the _K_ amount of that token into TokenHub, resulting in _S-K_ tokens to circulate on BSC. Thus the total circulation across 2 blockchains remains as _S_.
4. The issuer of BEP2 token sends the bind transaction on BC. Once the transaction is executed successfully after proper verification:
   * It transfers _S-K_ tokens to a system-controlled address on BC.
   * A cross-chain bind request package will be created, waiting for Relayers to relay.
5. BSC Relayers will relay the cross-chain bind request package into **TokenHub** on BSC, and the corresponding request and information will be stored into the contract.
6. The contract owner and only the owner can run a special method of TokenHub contract, `ApproveBind`, to verify the binding request to mark it as a success. It will confirm:
   * the token has not been bound;
   * the binding is for the proper symbol, with proper total supply and decimal information;
   * the proper lock are done on both networks;
7. Once the `ApproveBind` method has succeeded, TokenHub will mark the two tokens are bounded and share the same circulation on BSC, and the status will be propagated back to BC. After this final confirmation, the BEP2E contract address and decimals will be written onto the BEP2 token as a new attribute on BC, and the tokens can be transferred across the two blockchains bidirectionally. If the ApproveBind fails, the failure event will also be propagated back to BC to release the locked tokens, and the above steps can be re-tried later.
