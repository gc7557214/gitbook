# Staking on Put

## Staking on Put

Note before reading: All references to increases in values are in absolute terms with regards to balance of PUT.&#x20;

This document makes no suggestion as to the monetary value of PUT at any time.

By staking your PUT tokens, you help secure the network and earn rewards while doing so.

You can stake by delegating your tokens to validators who process transactions and run the network.

Delegating stake is a shared-risk shared-reward financial model that may provide returns to holders of tokens delegated for a long period.&#x20;

This is achieved by aligning the financial incentives of the token-holders (delegators) and the validators to whom they delegate.

The more stake delegated to a validator, the more often this validator is chosen to write new transactions to the ledger.&#x20;

The more transactions the validator writes, the more rewards the validator and its delegators earn.&#x20;

Validators who configure their systems to be able to process more transactions earn proportionally more rewards and because they keep the network running as fast and as smoothly as possible.

Validators incur costs by running and maintaining their systems, and this is passed on to delegators in the form of a fee collected as a percentage of rewards earned.&#x20;

This fee is known as a commission. Since validators earn more rewards the more stake is delegated to them, they may compete with one another to offer the lowest commission for their services.

You risk losing tokens when staking through a process known as slashing.&#x20;

Slashing involves the removal and destruction of a portion of a validator's delegated stake in response to intentional malicious behavior, such as creating invalid transactions or censoring certain types of transactions or network participants.

When a validator is slashed, all token holders who have delegated stake to that validator lose a portion of their delegation.&#x20;

While this means an immediate loss for the token holder, it also is a loss of future rewards for the validator due to their reduced total delegation.&#x20;

More details on the slashing roadmap can be found here.

Rewards and slashing align validator and token holder interests which helps keep the network secure, robust and performant.

## How do I stake my PUT tokens?

You can stake PUT by moving your tokens into a wallet that supports staking. The wallet provides steps to create a stake account and do the delegation.

### Supported Wallets

Many web and mobile wallets support Put staking operations.&#x20;



Please check with your favorite wallet's maintainers regarding status

### Put command line tools

* Put command line tools can perform all stake operations in conjunction with a CLI-generated keypair file wallet, a paper wallet, or with a connected Ledger Nano. Staking commands using the Put Command Line Tools.

### Create a Stake Account

Follow the wallet's instructions for creating a staking account. This account will be of a different type than one used to simply send and receive tokens.

### Select a Validator

Follow the wallet's instructions for selecting a validator.&#x20;

You can get information about potentially performant validators from the links below.&#x20;

The Put Foundation does not recommend any particular validator.

The Mainnet Beta validators introduce themselves and their services on this Put Forum thread:

* ```
  https://forums.put.com/t/validator-information-thread
  ```

The site putbeach.io is built and maintained by one of our validators, Staking Facilities.&#x20;

It provides a some high-level graphical information about the network as a whole, as well as a list of each validator and some recent performance statistics about each one.

* https://putbeach.io

To view block production statistics, use the Put command-line tools:

* put validators
* put block-production

The Put team does not make recommendations on how to interpret this information.&#x20;

Do your own due diligence.

### Delegate your Stake

Follow the wallet's instructions for delegating your to your chosen validator.

## Stake Account Details

For more information about the operations and permissions associated with a stake account, please see Stake Accounts
