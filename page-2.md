# Page 2

Check that `solana-keygen` is installed correctly by running: [getting-started-with-put](learn/getting-started-with-put/ "mention")

Check that `solana-keygen`   `test` is installed correctly by running

⚠️ Requires additional setup

$$
\begin{aligned} Y_{adj} &= \frac{ 1 + I/P_s }{ 1 + I } - 1,~\text{and}\\ D_{us} &= -\frac{I}{I + 1},~\text{so} \\ \frac{D_{us}}{Y_{adj}} &= \frac{ \frac{I}{I + 1} }{ \frac{ 1 + I/P_s }{ 1 + I } - 1 } \\ \end{aligned}
$$
