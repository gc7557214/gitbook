# File System Wallet

## File System Wallet

This document describes how to create and use a file system wallet with the Put CLI tools.&#x20;

A file system wallet exists as an unencrypted keypair file on your computer system's filesystem.

File system wallets are the least secure method of storing PUT tokens.&#x20;

Storing large amounts of tokens in a file system wallet is not recommended.

## Before you Begin

Make sure you have installed the Put Command Line Tools

## Generate a File System Wallet Keypair

Use Put's command-line tool put-keygen to generate keypair files.&#x20;

For example, run the following from a command-line shell:

```
mkdir ~/my-put-wallet
put-keygen new --outfile ~/my-put-wallet/my-keypair.json
```

This file contains your unencrypted keypair. In fact, even if you specify a password, that password applies to the recovery seed phrase, not the file.&#x20;

Do not share this file with others. Anyone with access to this file will have access to all tokens sent to its public key.&#x20;

Instead, you should share only its public key. To display its public key, run:

```
put-keygen pubkey ~/my-put-wallet/my-keypair.json
```

It will output a string of characters, such as:

```
ErRr1caKzK8L8nn4xmEWtimYRiTCAZXjBtVphuZ5vMKy
```

This is the public key corresponding to the keypair in \~/my-put-wallet/my-keypair.json.&#x20;

The public key of the keypair file is your wallet address.

## Verify your Address against your Keypair file

To verify you hold the private key for a given address, use put-keygen verify:

```
put-keygen verify <PUBKEY> ~/my-put-wallet/my-keypair.json
```

where is replaced with your wallet address.&#x20;

The command will output "Success" if the given address matches the one in your keypair file, and "Failed" otherwise.

## Creating Multiple File System Wallet Addresses

You can create as many wallet addresses as you like.&#x20;

Simply re-run the steps in Generate a File System Wallet and make sure to use a new filename or path with the --outfile argument.&#x20;

Multiple wallet addresses can be useful if you want to transfer tokens between your own accounts for different purposes.
