# Command Line Wallets

## Command Line Wallets

Put supports several different types of wallets that can be used to interface directly with the Put command-line tools.

To use a Command Line Wallet, you must first install the Put CLI tools

## File System Wallet

A file system wallet, aka an FS wallet, is a directory in your computer's file system.&#x20;

Each file in the directory holds a keypair.

### File System Wallet Security

A file system wallet is the most convenient and least secure form of wallet. It is convenient because the keypair is stored in a simple file.

You can generate as many keys as you would like and trivially back them up by copying the files.

It is insecure because the keypair files are unencrypted.&#x20;

If you are the only user of your computer and you are confident it is free of malware, an FS wallet is a fine solution for small amounts of cryptocurrency.&#x20;

If, however, your computer contains malware and is connected to the Internet, that malware may upload your keys and use them to take your tokens.&#x20;

Likewise, because the keypairs are stored on your computer as files, a skilled hacker with physical access to your computer may be able to access it.&#x20;

Using an encrypted hard drive, such as FileVault on MacOS, minimizes that risk.

File System Wallet

## Paper Wallet

A paper wallet is a collection of seed phrases written on paper.&#x20;

A seed phrase is some number of words (typically 12 or 24) that can be used to regenerate a keypair on demand.

### Paper Wallet Security

In terms of convenience versus security, a paper wallet sits at the opposite side of the spectrum from an FS wallet.&#x20;

It is terribly inconvenient to use, but offers excellent security.&#x20;

That high security is further amplified when paper wallets are used in conjunction with offline signing.
