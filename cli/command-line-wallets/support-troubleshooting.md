# Support / Troubleshooting

Support / Troubleshooting

If you have questions or are having trouble setting up or using your wallet of choice, please make sure you've read through all the relevant pages in our Wallet Guide.&#x20;

The Put team is working hard to support new features on popular wallets, and we do our best to keep our documents up to date with the latest available features.

If you have questions after reading the docs, feel free to reach out to us on our Telegram.

For technical support, please ask a question on StackOverflow and tag your questions with put.
