# Command-line Guide

## Command-line Guide

In this section, we will describe how to use the Put command-line tools to create a wallet, to send and receive PUT tokens, and to participate in the cluster by delegating stake.

To interact with a Put cluster, we will use its command-line interface, also known as the CLI.&#x20;

We use the command-line because it is the first place the Put core team deploys new functionality.&#x20;

The command-line interface is not necessarily the easiest to use, but it provides the most direct, flexible, and secure access to your Put accounts.

## Getting Started

To get started using the Put Command Line (CLI) tools:

Install the Put Tools&#x20;

Choose a Cluster&#x20;

Create a Wallet&#x20;

Check out our CLI conventions
