# Connecting to a Cluster

Connecting to a Cluster

See Put Clusters for general information about the available clusters.

## Configure the command-line tool

You can check what cluster the Put command-line tool (CLI) is currently targeting by running the following command:

```
put config get
```

Use put config set command to target a particular cluster.&#x20;

After setting a cluster target, any future subcommands will send/receive information from that cluster.

For example to target the Devnet cluster, run:

```
put config set --url https://api.devnet.put.com
```

## Ensure Versions Match

Though not strictly necessary, the CLI will generally work best when its version matches the software version running on the cluster.&#x20;

To get the locally-installed CLI version, run:

```
put --version
```

To get the cluster version, run:

```
put cluster-version
```

Ensure the local CLI version is greater than or equal to the cluster version.
