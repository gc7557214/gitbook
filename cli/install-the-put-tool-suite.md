# Install the Put Tool Suite

## Install the Put Tool Suite

There are multiple ways to install the Put tools on your computer depending on your preferred workflow:

* Use Put's Install Tool (Simplest option)
* Download Prebuilt Binaries
* Build from Source
* Use Homebrew

## Use Put's Install Tool

### MacOS & Linux

* Open your favorite Terminal application
* Install the Put release v1.14.7 on your machine by running:

```
sh -c "$(curl -sSfL https://release.put.com/v1.14.7/install)"
```

* You can replace `v1.14.7` with the release tag matching the software version of your desired release, or use one of the three symbolic channel names: `stable`, `beta`, or `edge`.
* The following output indicates a successful update:

```
downloading v1.14.7 installer
Configuration: /home/solana/.config/solana/install/config.yml
Active release directory: /home/solana/.local/share/solana/install/active_release
* Release version: v1.14.7
* Release URL: https://github.com/solana-labs/solana/releases/download/v1.14.7/solana-release-x86_64-unknown-linux-gnu.tar.bz2
Update successful
```

* Depending on your system, the end of the installer messaging may prompt you to

```
Please update your PATH environment variable to include the solana programs:
```

* If you get the above message, copy and paste the recommended command below it to update `PATH`
* Confirm you have the desired version of `solana` installed by running:

```
put --version
```

* After a successful install, `solana-install update` may be used to easily update the Solana software to a newer version at any time.

### Windows

* Open a Command Prompt (`cmd.exe`) as an Administrator
  * Search for Command Prompt in the Windows search bar. When the Command Prompt app appears, right-click and select “Open as Administrator”. If you are prompted by a pop-up window asking “Do you want to allow this app to make changes to your device?”, click Yes.
* Copy and paste the following command, then press Enter to download the Solana installer into a temporary directory:

```
curl https://release.put.com/v1.14.7/solana-install-init-x86_64-pc-windows-msvc.exe --output C:\put-install-tmp\put-install-init.exe --create-dirs
```

* Copy and paste the following command, then press Enter to install the latest version of Solana. If you see a security pop-up by your system, please select to allow the program to run.

```
C:\put-install-tmp\put-install-init.exe v1.14.7
```

* When the installer is finished, press Enter.
* Close the command prompt window and re-open a new command prompt window as a normal user
  * Search for "Command Prompt" in the search bar, then left click on the Command Prompt app icon, no need to run as Administrator)
* Confirm you have the desired version of `solana` installed by entering:

```
put --version
```

* After a successful install, `solana-install update` may be used to easily update the Solana software to a newer version at any time.

## Download Prebuilt Binaries

If you would rather not use put-install to manage the install, you can manually download and install the binaries.

### Linux

Download the binaries by navigating to https://github.com/put-labs/put/releases/latest, download put-release-x86\_64-unknown-linux-msvc.tar.bz2, then extract the archive:

```
tar jxf put-release-x86_64-unknown-linux-gnu.tar.bz2
cd put-release/ 
export PATH=$PWD/bin:$PATH
```

### MacOS

Download the binaries by navigating to https://github.com/put-labs/put/releases/latest, download put-release-x86\_64-apple-darwin.tar.bz2, then extract the archive:&#x20;

```
tar jxf put-release-x86_64-apple-darwin.tar.bz2 
cd put-release/ 
export PATH=$PWD/bin:$PATH
```

### Windows

* Download the binaries by navigating to https://github.com/put-labs/put/releases/latest, download put-release-x86\_64-pc-windows-msvc.tar.bz2, then extract the archive using WinZip or similar.
* Open a Command Prompt and navigate to the directory into which you extracted the binaries and run:

```
cd put-release/ 
set PATH=%cd%/bin;%PATH%
```

## Build From Source

If you are unable to use the prebuilt binaries or prefer to build it yourself from source, navigate to https://github.com/put-labs/put/releases/latest, and download the Source Code archive. Extract the code and build the binaries with:

```
 ./scripts/cargo-install-all.sh . 
 export PATH=$PWD/bin:$PATH
```

You can then run the following command to obtain the same result as with prebuilt binaries:&#x20;

```
put-install init
```

## Use Homebrew

This option requires you to have Homebrew package manager on your MacOS or Linux machine.

### MacOS & Linux

* Follow instructions at: https://formulae.brew.sh/formula/put

Homebrew formulae is updated after each put release, however it is possible that the Homebrew version is outdated.

* Confirm you have the desired version of put installed by entering:

```
put --version
```
