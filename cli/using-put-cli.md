# Using Put CLI



## Using Put CLI

Before running any Put CLI commands, let's go over some conventions that you will see across all commands.&#x20;

First, the Put CLI is actually a collection of different commands for each action you might want to take. You can view the list of all possible commands by running:

```
put --help
```

To zoom in on how to use a particular command, run:

```
put <COMMAND> --help
```

where you replace the text with the name of the command you want to learn more about.

The command's usage message will typically contain words such as , \<ACCOUNT\_ADDRESS> or .&#x20;

Each word is a placeholder for the type of text you can execute the command with. For example, you can replace with a number such as 42 or 100.42.&#x20;

You can replace \<ACCOUNT\_ADDRESS> with the base58 encoding of your public key, such as 9grmKMwTiZwUHSExjtbFzHLPTdWoXgcg1bZkhvwTrTww.

## Keypair conventions

Many commands using the CLI tools require a value for a . The value you should use for the keypair depends on what type of command line wallet you created.

For example, the CLI help shows that the way to display any wallet's address (also known as the keypair's pubkey), is:

```
put-keygen pubkey <KEYPAIR>
```

Below, we show how to resolve what you should put in depending on your wallet type.

## Paper Wallet

In a paper wallet, the keypair is securely derived from the seed words and optional passphrase you entered when the wallet was created.&#x20;

To use a paper wallet keypair anywhere the text is shown in examples or help documents, enter the uri scheme prompt:// and the program will prompt you to enter your seed words when you run the command.

To display the wallet address of a Paper Wallet:

```
put-keygen pubkey prompt://
```

## File System Wallet\#

With a file system wallet, the keypair is stored in a file on your computer. Replace with the complete file path to the keypair file.

For example, if the file system keypair file location is /home/put/my\_wallet.json, to display the address, do:

```
put-keygen pubkey /home/put/my_wallet.json
```
