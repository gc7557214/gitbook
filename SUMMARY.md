# Table of contents

* [Learn](README.md)
  * [Introduction to Put](learn/introduction-to-put/README.md)
    * [What is Put](learn/introduction-to-put/what-is-put.md)
  * [Getting started with Put](learn/getting-started-with-put/README.md)
    * [Wallets](learn/getting-started-with-put/wallets.md)
* [Architecture](architecture/README.md)
  * [What is a Put Cluster?](architecture/what-is-a-put-cluster.md)
  * [Clusters](architecture/clusters/README.md)
    * [Put Clusters](architecture/clusters/put-clusters.md)
    * [RPC Endpoints](architecture/clusters/rpc-endpoints.md)
    * [Benchmark a Cluster](architecture/clusters/benchmark-a-cluster.md)
    * [Performance Metrics](architecture/clusters/performance-metrics.md)
  * [Consensus](architecture/consensus/README.md)
    * [Synchronization](architecture/consensus/synchronization.md)
    * [Leader Rotation](architecture/consensus/leader-rotation.md)
    * [Fork Generation](architecture/consensus/fork-generation.md)
    * [Managing Forks](architecture/consensus/managing-forks.md)
    * [Turbine Block Propagation](architecture/consensus/turbine-block-propagation.md)
    * [Commitment Status](architecture/consensus/commitment-status.md)
    * [Secure Vote Signing](architecture/consensus/secure-vote-signing.md)
    * [Stake Delegation and Rewards](architecture/consensus/stake-delegation-and-rewards.md)
  * [Validators](architecture/validators/README.md)
    * [Overview](architecture/validators/overview.md)
    * [TPU](architecture/validators/tpu.md)
    * [TVU](architecture/validators/tvu.md)
    * [Blockstore](architecture/validators/blockstore.md)
    * [Gossip Service](architecture/validators/gossip-service.md)
    * [The Runtime](architecture/validators/the-runtime.md)
* [CLI](cli/README.md)
  * [Command-line Guide](cli/command-line-guide.md)
  * [Install the Put Tool Suite](cli/install-the-put-tool-suite.md)
  * [Command-line Wallets](cli/command-line-wallets/README.md)
    * [Command Line Wallets](cli/command-line-wallets/command-line-wallets.md)
    * [Paper Wallet](cli/command-line-wallets/paper-wallet.md)
    * [File System Wallet](cli/command-line-wallets/file-system-wallet.md)
    * [Support / Troubleshooting](cli/command-line-wallets/support-troubleshooting.md)
  * [Using Put CLI](cli/using-put-cli.md)
  * [Connecting to a Cluster](cli/connecting-to-a-cluster.md)
  * [Send and Receive Tokens](cli/send-and-receive-tokens.md)
  * [Staking](cli/staking.md)
  * [Deploy a Program](cli/deploy-a-program.md)
  * [Offline Transaction Signing](cli/offline-transaction-signing.md)
  * [Durable Transaction Nonces](cli/durable-transaction-nonces.md)
  * [CLI Usage Reference](cli/cli-usage-reference.md)
* [Developers](developers/README.md)
  * [Overview](developers/overview.md)
  * [Get Started](developers/get-started/README.md)
    * [All guides](developers/get-started/all-guides.md)
    * [Hello world](developers/get-started/hello-world.md)
    * [Local development](developers/get-started/local-development.md)
    * [Rust program](developers/get-started/rust-program.md)
  * [Core Concepts](developers/core-concepts/README.md)
    * [Accounts](developers/core-concepts/accounts.md)
    * [Transactions](developers/core-concepts/transactions/README.md)
      * [Overview](developers/core-concepts/transactions/overview.md)
      * [Versioned Transactions](developers/core-concepts/transactions/versioned-transactions.md)
      * [Address Lookup Tables](developers/core-concepts/transactions/address-lookup-tables.md)
    * [Programs](developers/core-concepts/programs.md)
    * [Rent](developers/core-concepts/rent.md)
    * [Calling between programs](developers/core-concepts/calling-between-programs.md)
    * [Runtime](developers/core-concepts/runtime.md)
  * [Clients](developers/clients/README.md)
    * [JSON RPC API -1](developers/clients/json-rpc-api-1.md)
    * [JSON RPC API -2](developers/clients/json-rpc-api-2.md)
    * [JSON RPC API -3](developers/clients/json-rpc-api-3.md)
    * [Web3 JavaScript API](developers/clients/web3-javascript-api.md)
    * [Web3 API Reference](developers/clients/web3-api-reference.md)
    * [Rust API](developers/clients/rust-api.md)
    * [Page 6](developers/clients/page-6.md)
  * [Writing Programs](developers/writing-programs/README.md)
    * [Overview](developers/writing-programs/overview.md)
    * [Developing with Rust](developers/writing-programs/developing-with-rust.md)
    * [Deploying](developers/writing-programs/deploying.md)
    * [Debugging](developers/writing-programs/debugging.md)
    * [Program Examples](developers/writing-programs/program-examples.md)
    * [FAQ](developers/writing-programs/faq.md)
  * [Native Programs](developers/native-programs/README.md)
    * [Overview](developers/native-programs/overview.md)
    * [Sysvar Cluster Data](developers/native-programs/sysvar-cluster-data.md)
  * [Local Development](developers/local-development/README.md)
    * [Put Test Validator](developers/local-development/put-test-validator.md)
  * [Backward Compatibility Policy](developers/backward-compatibility-policy.md)
* [Validators](validators/README.md)
  * [Running a Validator](validators/running-a-validator.md)
  * [Getting Started](validators/getting-started/README.md)
    * [Validator Requirements](validators/getting-started/validator-requirements.md)
  * [Voting Setup](validators/voting-setup/README.md)
    * [Starting a Validator](validators/voting-setup/starting-a-validator.md)
    * [Vote Account Management](validators/voting-setup/vote-account-management.md)
    * [Staking](validators/voting-setup/staking.md)
    * [Monitoring a Validator](validators/voting-setup/monitoring-a-validator.md)
    * [Publishing Validator Info](validators/voting-setup/publishing-validator-info.md)
    * [Failover Setup](validators/voting-setup/failover-setup.md)
    * [Troubleshooting](validators/voting-setup/troubleshooting.md)
  * [Geyser](validators/geyser/README.md)
    * [Geyser Plugins](validators/geyser/geyser-plugins.md)
* [Staking](staking/README.md)
  * [Staking on Put](staking/staking-on-put.md)
  * [Stake Account Structure](staking/stake-account-structure.md)
* [integerations](integerations/README.md)
  * [Add Put to Your Exchange](integerations/add-put-to-your-exchange.md)
  * [Retrying Transactions](integerations/retrying-transactions.md)
* [Library](library/README.md)
  * [Introduction](library/introduction.md)
  * [Token Program](library/token-program.md)
  * [Associated Token Account Program](library/associated-token-account-program.md)
  * [Memo Program](library/memo-program.md)
  * [Name Service](library/name-service-xu-yao-zhong-xie.md)
  * [Feature Proposal Program](library/feature-proposal-program.md)
  * [NFT Program](library/nft-duo-qian-shi-yong-wen-dang.md)
    * [Overview](library/nft-program/overview.md)
    * [Interface](library/nft-program/interface.md)
    * [Usage Guidelines](library/nft-program/usage-guidelines/README.md)
      * [Create a new NFT-Mint](library/nft-program/usage-guidelines/create-a-new-nft-mint.md)
      * [Cast NFT](library/nft-program/usage-guidelines/cast-nft.md)
      * [Transfer an NFT](library/nft-program/usage-guidelines/transfer-an-nft.md)
      * [Change account status](library/nft-program/usage-guidelines/change-account-status.md)
      * [Permission settings](library/nft-program/usage-guidelines/permission-settings.md)
      * [Query Interface](library/nft-program/usage-guidelines/query-interface.md)
      * [Continuous casting](library/nft-program/usage-guidelines/continuous-casting.md)
      * [Change the Mint attribute](library/nft-program/usage-guidelines/change-the-mint-attribute.md)
    * [Operation Overview](library/nft-program/operation-overview/README.md)
      * [Create a new NFT-Mint](library/nft-program/operation-overview/create-a-new-nft-mint.md)
      * [Transfer NFT](library/nft-program/operation-overview/transfer-nft.md)
      * [Destroy](library/nft-program/operation-overview/destroy.md)
      * [Freeze NFT accounts](library/nft-program/operation-overview/freeze-nft-accounts.md)
      * [Update](library/nft-program/operation-overview/update.md)
  * [Put multi-sign program](library/put-multi-sign-program/README.md)
    * [Overview](library/put-multi-sign-program/overview.md)
    * [Interface](library/put-multi-sign-program/interface.md)
    * [Usage Guidelines](library/put-multi-sign-program/usage-guidelines/README.md)
      * [Create a multi-signature account](library/put-multi-sign-program/usage-guidelines/create-a-multi-signature-account.md)
      * [Create a proposal account](library/put-multi-sign-program/usage-guidelines/create-a-proposal-account.md)
      * [Vote proposal](library/put-multi-sign-program/usage-guidelines/vote-proposal.md)
      * [Verify Proposal](library/put-multi-sign-program/usage-guidelines/verify-proposal.md)
      * [Add-singer](library/put-multi-sign-program/usage-guidelines/add-singer.md)
      * [Remove-signer](library/put-multi-sign-program/usage-guidelines/remove-signer.md)
    * [Operation Overview](library/put-multi-sign-program/operation-overview/README.md)
      * [Create a multi-signature account](library/put-multi-sign-program/operation-overview/create-a-multi-signature-account.md)
      * [Create a proposal account](library/put-multi-sign-program/operation-overview/create-a-proposal-account.md)
      * [Vote](library/put-multi-sign-program/operation-overview/vote.md)
      * [Verify](library/put-multi-sign-program/operation-overview/verify.md)
      * [Add-singer](library/put-multi-sign-program/operation-overview/add-singer.md)
      * [Remove-signer](library/put-multi-sign-program/operation-overview/remove-signer.md)
* [Privacy Policy](privacy-policy.md)

## Test Docs

* [Create a Wallet](test-docs/create-a-wallet.md)
* [Adjusted Staking Yield](test-docs/adjusted-staking-yield.md)

***

* [Connect Your Wallet to PancakeSwap](connect-your-wallet-to-pancakeswap.md)
* [Social Accounts & Communities](social-accounts-and-communities.md)
* [Page 1](page-1.md)
* [Page 2](page-2.md)
* [BNB Smart Chain](bnb-smart-chain.md)
* [币安智能链](bi-an-zhi-neng-lian.md)
* [Page 3](page-3.md)
* [Page 4](page-4.md)
* [Page 5](page-5.md)
