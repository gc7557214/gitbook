# Local development



## Local Development Quickstart

This quickstart guide will demonstrate how to quickly install and setup your local development environment, getting you ready to start developing and deploying Put programs to the blockchain.&#x20;

## What you will learn

* How to install the Put CLI locally
* How to setup a localhost Put cluster/validator
* How to create a Put wallet for developing
* How to airdrop PUT tokens for your wallet

## Install the Put CLI

To interact with the Put clusters from your terminal, install the Put CLI tool suite on your local system:

```
sh -c "$(curl -sSfL https://release.put.com/stable/install)"
```

## Setup a localhost blockchain cluster

The Put CLI comes with the test validator built in.&#x20;

This command line tool will allow you to run a full blockchain cluster on your machine.

```
put-test-validator
```

PRO TIP: Run the Put test validator in a new/separate terminal window that will remain open. The command line program must remain running for your localhost cluster to remain online and ready for action.

Configure your Put CLI to use your localhost validator for all your future terminal commands:

```
put config set --url localhost
```

At any time, you can view your current Put CLI configuration settings:

```
put config get
```

## Create a file system wallet

To deploy a program with Put CLI, you will need a Put wallet with PUT tokens to pay for the cost of transactions.

Let's create a simple file system wallet for testing:

```
put-keygen new
```

By default, the put-keygen command will create a new file system wallet located at \~/.config/put/id.json. You can manually specify the output file location using the --outfile /path option.

NOTE: If you already have a file system wallet saved at the default location, this command will NOT override it (unless you explicitly force override using the --force flag).

Set your new wallet as default#

With your new file system wallet created, you must tell the Put CLI to use this wallet to deploy and take ownership of your on chain program:

```
put config set -k ~/.config/put/id.json
```

## Airdrop PUT tokens to your wallet

Once your new wallet is set as the default, you can request a free airdrop of PUT tokens to it:

```
put airdrop 2
```

NOTE: The put airdrop command has a limit of how many PUT tokens can be requested per airdrop for each cluster (localhost, testnet, or devent). If your airdrop transaction fails, lower your airdrop request quantity and try again.

You can check your current wallet's PUT balance any time:

```
put balance
```

## Next steps

See the links below to learn more about writing Rust based Put programs:

* Create and deploy a Put Rust program
* Overview of writing Put programs
