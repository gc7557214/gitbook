# Rust program



## Rust Program Quickstart

Rust is the most common programming language to write Put programs with.&#x20;

This quickstart guide will demonstrate how to quickly setup, build, and deploy your first Rust based Put program to the blockchain.



NOTE: This guide uses the Put CLI and assumes you have setup your local development environment. Checkout our local development quickstart guide here to quickly get setup.

## What you will learn\#

* How to install the Rust language locally
* How to initialize a new Put Rust program
* How to code a basic Put program in Rust
* How to build and deploy your Rust program

## Install Rust and Cargo

To be able to compile Rust based Put programs, install the Rust language and Cargo (the Rust package manager) using Rustup:

```
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

## Run your localhost validator

The Put CLI comes with the test validator built in.&#x20;

This command line tool will allow you to run a full blockchain cluster on your machine.

```
put-test-validator
```

PRO TIP:&#x20;

Run the Put test validator in a new/separate terminal window that will remain open.&#x20;

This command line program must remain running for your localhost validator to remain online and ready for action.

Configure your Put CLI to use your localhost validator for all your future terminal commands and Put program deployment:

```
put config set --url localhost
```

## Create a new Rust library with Cargo

Put programs written in Rust are libraries which are compiled to BPF bytecode and saved in the .so format.

Initialize a new Rust library named hello\_world via the Cargo command line:

```
cargo init hello_world --lib
cd hello_world
```

Add the put-program crate to your new Rust library:

```
cargo add put-program
```

Open your Cargo.toml file and add these required Rust library configuration settings, updating your project name as appropriate:

```
[lib]
name = "hello_world"
crate-type = ["cdylib", "lib"]
```

## Create your first Put program

The code for your Rust based Put program will live in your src/lib.rs file.&#x20;

Inside src/lib.rs you will be able to import your Rust crates and define your logic.&#x20;

Open your src/lib.rs file in your favorite editor.

At the top of lib.rs, import the put-program crate and bring our needed items into the local namespace:

```
use put_program::{
    account_info::AccountInfo,
    entrypoint,
    entrypoint::ProgramResult,
    pubkey::Pubkey,
    msg,
};
```

Every Put program must define an entrypoint that tells the Put runtime where to start executing your on chain code.&#x20;

Your program's entrypoint should provide a public function named process\_instruction:

```
// declare and export the program's entrypoint
entrypoint!(process_instruction);

// program entrypoint's implementation
pub fn process_instruction(
    program_id: &Pubkey,
    accounts: &[AccountInfo],
    instruction_data: &[u8]
) -> ProgramResult {
    // log a message to the blockchain
    msg!("Hello, world!");

    // gracefully exit the program
    Ok(())
}
```

Every on chain program should return the Ok result enum with a value of ().&#x20;

This tells the Put runtime that your program executed successfully without errors.

This program above will simply log a message of "Hello, world!" to the blockchain cluster, then gracefully exit with Ok(()).

Build your Rust program#

Inside a terminal window, you can build your Put Rust program by running in the root of your project (i.e. the directory with your Cargo.toml file):

```
cargo build-bpf
```

NOTE: After each time you build your Put program, the above command will output the build path of your compiled program's .so file and the default keyfile that will be used for the program's address.

## Deploy your Put program

Using the Put CLI, you can deploy your program to your currently selected cluster:

```
put program deploy ./target/deploy/hello_world.so
```

Once your Put program has been deployed (and the transaction finalized), the above command will output your program's public address (aka its "program id").

```
# example output
Program Id: EFH95fWg49vkFNbAdw9vy75tM7sWZ2hQbTTUmuACGip3
```

Congratulations!#

You have successfully setup, built, and deployed a Put program using the Rust language.

PS: Check your Put wallet's balance again after you deployed. See how much PUT it cost to deploy your simple program?

## Next steps

See the links below to learn more about writing Rust based Put programs:

```
Overview of writing Put programs
Learn more about developing Put programs with Rust
Debugging on chain programs
```
