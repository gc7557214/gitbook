# Program Examples

Program Examples&#x20;

## Helloworld

Hello World is a project that demonstrates how to use the Put Javascript API and both Rust and C programs to build, deploy, and interact with programs on the Put blockchain.

The project comprises of:

* An on-chain hello world program
* A client that can send a "hello" to an account and get back the number of times "hello" has been sent

### Build and Run\#

fetch the latest version of the example code:&#x20;

```
$ git clone https://github.com/put-labs/example-helloworld.git 
$ cd example-helloworld
```
