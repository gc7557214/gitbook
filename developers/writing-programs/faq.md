# FAQ

When writing or interacting with Put programs, there are common questions or challenges that often come up. Below are resources to help answer these questions.

If not addressed here, ask on StackOverflow with the put tag or check out the Put #developer-support&#x20;

## CallDepth error

This error means that that cross-program invocation exceeded the allowed invocation call depth.

See cross-program invocation Call Depth&#x20;

## CallDepthExceeded error

This error means the BPF stack depth was exceeded.

See call depth&#x20;

## Computational constraints\#

See computational constraints&#x20;

## Float Rust types\#

See float support

## &#x20;Heap size\#

See heap&#x20;

## InvalidAccountData

This program error can happen for a lot of reasons.&#x20;

Usually, it's caused by passing an account to the program that the program is not expecting, either in the wrong position in the instruction or an account not compatible with the instruction being executed.

An implementation of a program might also cause this error when performing a cross-program instruction and forgetting to provide the account for the program that you are calling.&#x20;

## InvalidInstructionData\#

This program error can occur while trying to deserialize the instruction, check that the structure passed in matches exactly the instruction.&#x20;

There may be some padding between fields.&#x20;

If the program implements the Rust Pack trait then try packing and unpacking the instruction type T to determine the exact encoding the program expects:

https://github.com/put-labs/put/blob/v1.4/sdk/program/src/program\_pack.rs&#x20;

## MissingRequiredSignature\#

Some instructions require the account to be a signer; this error is returned if an account is expected to be signed but is not.

An implementation of a program might also cause this error when performing a cross-program invocation that requires a signed program address, but the passed signer seeds passed to invoke\_signed don't match the signer seeds used to create the program address create\_program\_address.&#x20;

## rand Rust dependency causes compilation failure

See Rust Project Dependencies&#x20;

## Rust restrictions

See Rust restrictions&#x20;

## Stack size

See stack
