# Deploying

<figure><img src="../../.gitbook/assets/icon.svg" alt=""><figcaption></figcaption></figure>

Deploying Programs&#x20;



As shown in the diagram above, a program author creates a program, compiles it to an ELF shared object containing BPF bytecode, and uploads it to the Put cluster with a special deploy transaction.&#x20;

The cluster makes it available to clients via a program ID.&#x20;

The program ID is an address specified when deploying and is used to reference the program in subsequent transactions.

Upon a successful deployment the account that holds the program is marked executable.&#x20;

If the program is marked "final", its account data become permanently immutable.&#x20;

If any changes are required to the finalized program (features, patches, etc...) the new program must be deployed to a new program ID.

If a program is upgradeable, the account that holds the program is marked executable, but it is possible to redeploy a new shared object to the same program ID, provided that the program's upgrade authority signs the transaction.

The Put command line interface supports deploying programs, for more information see the deploy command line usage documentation.
