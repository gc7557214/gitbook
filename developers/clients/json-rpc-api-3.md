# JSON RPC API -3

## JSON RPC API Deprecated Methods

### getConfirmedBlock

**DEPRECATED: Please use getBlock instead** This method is expected to be removed in put-core v2.0

Returns identity and transaction information about a confirmed block in the ledger

**Parameters:**

* `<u64>` - slot, as u64 integer
* (optional) `<object>` - Configuration object containing the following optional fields:
  * (optional) `encoding: <string>` - encoding for each returned Transaction, either "json", "jsonParsed", "base58" (_slow_), "base64". If parameter not provided, the default encoding is "json". "jsonParsed" encoding attempts to use program-specific instruction parsers to return more human-readable and explicit data in the `transaction.message.instructions` list. If "jsonParsed" is requested but a parser cannot be found, the instruction falls back to regular JSON encoding (`accounts`, `data`, and `programIdIndex` fields).
  * (optional) `transactionDetails: <string>` - level of transaction detail to return, either "full", "signatures", or "none". If parameter not provided, the default detail level is "full".
  * (optional) `rewards: bool` - whether to populate the `rewards` array. If parameter not provided, the default includes rewards.
  * (optional) `commitment: <string>` - Commitment; "processed" is not supported. If parameter not provided, the default is "finalized".

**Results:**

The result field will be an object with the following fields:

* `<null>` - if specified block is not confirmed
* `<object>` - if block is confirmed, an object with the following fields:
  * `blockhash: <string>` - the blockhash of this block, as base-58 encoded string
  * `previousBlockhash: <string>` - the blockhash of this block's parent, as base-58 encoded string; if the parent block is not available due to ledger cleanup, this field will return "11111111111111111111111111111111"
  * `parentSlot: <u64>` - the slot index of this block's parent
  * `transactions: <array>` - present if "full" transaction details are requested; an array of JSON objects containing:
    * `transaction: <object|[string,encoding]>` - Transaction object, either in JSON format or encoded binary data, depending on encoding parameter
    * `meta: <object>` - transaction status metadata object, containing `null` or:
      * `err: <object|null>` - Error if transaction failed, null if transaction succeeded. [TransactionError definitions](https://github.com/put-labs/put/blob/c0c60386544ec9a9ec7119229f37386d9f070523/sdk/src/transaction/error.rs#L13)
      * `fee: <u64>` - fee this transaction was charged, as u64 integer
      * `preBalances: <array>` - array of u64 account balances from before the transaction was processed
      * `postBalances: <array>` - array of u64 account balances after the transaction was processed
      * `innerInstructions: <array|null>` - List of inner instructions or `null` if inner instruction recording was not enabled during this transaction
      * `preTokenBalances: <array|undefined>` - List of token balances from before the transaction was processed or omitted if token balance recording was not yet enabled during this transaction
      * `postTokenBalances: <array|undefined>` - List of token balances from after the transaction was processed or omitted if token balance recording was not yet enabled during this transaction
      * `logMessages: <array|null>` - array of string log messages or `null` if log message recording was not enabled during this transaction
      * DEPRECATED: `status: <object>` - Transaction status
        * `"Ok": <null>` - Transaction was successful
        * `"Err": <ERR>` - Transaction failed with TransactionError
  * `signatures: <array>` - present if "signatures" are requested for transaction details; an array of signatures strings, corresponding to the transaction order in the block
  * `rewards: <array>` - present if rewards are requested; an array of JSON objects containing:
    * `pubkey: <string>` - The public key, as base-58 encoded string, of the account that received the reward
    * `lamports: <i64>`- number of reward lamports credited or debited by the account, as a i64
    * `postBalance: <u64>` - account balance in lamports after the reward was applied
    * `rewardType: <string|undefined>` - type of reward: "fee", "rent", "voting", "staking"
    * `commission: <u8|undefined>` - vote account commission when the reward was credited, only present for voting and staking rewards
  * `blockTime: <i64|null>` - estimated production time, as Unix timestamp (seconds since the Unix epoch). null if not available

**Example:**

Request:

```bash
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {"jsonrpc": "2.0","id":1,"method":"getConfirmedBlock","params":[430, {"encoding": "json","transactionDetails":"full","rewards":false}]}
'
```

Result:

```json
{
  "jsonrpc": "2.0",
  "result": {
    "blockTime": null,
    "blockhash": "3Eq21vXNB5s86c62bVuUfTeaMif1N2kUqRPBmGRJhyTA",
    "parentSlot": 429,
    "previousBlockhash": "mfcyqEXB3DnHXki6KjjmZck6YjmZLvpAByy2fj4nh6B",
    "transactions": [
      {
        "meta": {
          "err": null,
          "fee": 5000,
          "innerInstructions": [],
          "logMessages": [],
          "postBalances": [499998932500, 26858640, 1, 1, 1],
          "postTokenBalances": [],
          "preBalances": [499998937500, 26858640, 1, 1, 1],
          "preTokenBalances": [],
          "status": {
            "Ok": null
          }
        },
        "transaction": {
          "message": {
            "accountKeys": [
              "3UVYmECPPMZSCqWKfENfuoTv51fTDTWicX9xmBD2euKe",
              "AjozzgE83A3x1sHNUR64hfH7zaEBWeMaFuAN9kQgujrc",
              "SysvarS1otHashes111111111111111111111111111",
              "SysvarC1ock11111111111111111111111111111111",
              "Vote111111111111111111111111111111111111111"
            ],
            "header": {
              "numReadonlySignedAccounts": 0,
              "numReadonlyUnsignedAccounts": 3,
              "numRequiredSignatures": 1
            },
            "instructions": [
              {
                "accounts": [1, 2, 3, 0],
                "data": "37u9WtQpcm6ULa3WRQHmj49EPs4if7o9f1jSRVZpm2dvihR9C8jY4NqEwXUbLwx15HBSNcP1",
                "programIdIndex": 4
              }
            ],
            "recentBlockhash": "mfcyqEXB3DnHXki6KjjmZck6YjmZLvpAByy2fj4nh6B"
          },
          "signatures": [
            "2nBhEBYYvfaAe16UMNqRHre4YNSskvuYgx3M6E4JP1oDYvZEJHvoPzyUidNgNX5r9sTyN1J9UxtbCXy2rqYcuyuv"
          ]
        }
      }
    ]
  },
  "id": 1
}
```

**Example:**

Request:

```bash
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {"jsonrpc": "2.0","id":1,"method":"getConfirmedBlock","params":[430, "base64"]}
'
```

Result:

```json
{
  "jsonrpc": "2.0",
  "result": {
    "blockTime": null,
    "blockhash": "3Eq21vXNB5s86c62bVuUfTeaMif1N2kUqRPBmGRJhyTA",
    "parentSlot": 429,
    "previousBlockhash": "mfcyqEXB3DnHXki6KjjmZck6YjmZLvpAByy2fj4nh6B",
    "rewards": [],
    "transactions": [
      {
        "meta": {
          "err": null,
          "fee": 5000,
          "innerInstructions": [],
          "logMessages": [],
          "postBalances": [499998932500, 26858640, 1, 1, 1],
          "postTokenBalances": [],
          "preBalances": [499998937500, 26858640, 1, 1, 1],
          "preTokenBalances": [],
          "status": {
            "Ok": null
          }
        },
        "transaction": [
          "AVj7dxHlQ9IrvdYVIjuiRFs1jLaDMHixgrv+qtHBwz51L4/ImLZhszwiyEJDIp7xeBSpm/TX5B7mYzxa+fPOMw0BAAMFJMJVqLw+hJYheizSoYlLm53KzgT82cDVmazarqQKG2GQsLgiqktA+a+FDR4/7xnDX7rsusMwryYVUdixfz1B1Qan1RcZLwqvxvJl4/t3zHragsUp0L47E24tAFUgAAAABqfVFxjHdMkoVmOYaR1etoteuKObS21cc1VbIQAAAAAHYUgdNXR0u3xNdiTr072z2DVec9EQQ/wNo1OAAAAAAAtxOUhPBp2WSjUNJEgfvy70BbxI00fZyEPvFHNfxrtEAQQEAQIDADUCAAAAAQAAAAAAAACtAQAAAAAAAAdUE18R96XTJCe+YfRfUp6WP+YKCy/72ucOL8AoBFSpAA==",
          "base64"
        ]
      }
    ]
  },
  "id": 1
}
```

For more details on returned data: Transaction Structure Inner Instructions Structure Token Balances Structure

### getConfirmedBlocks

**DEPRECATED: Please use getBlocks instead** This method is expected to be removed in put-core v2.0

Returns a list of confirmed blocks between two slots

**Parameters:**

* `<u64>` - start\_slot, as u64 integer
* (optional) `<u64>` - end\_slot, as u64 integer
* (optional) `<object>` - Configuration object containing the following field:
  * (optional) `commitment: <string>` - Commitment; "processed" is not supported. If parameter not provided, the default is "finalized".

**Results:**

The result field will be an array of u64 integers listing confirmed blocks between `start_slot` and either `end_slot`, if provided, or latest confirmed block, inclusive. Max range allowed is 500,000 slots.

**Example:**

Request:

```bash
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {"jsonrpc": "2.0","id":1,"method":"getConfirmedBlocks","params":[5, 10]}
'
```

Result:

```json
{ "jsonrpc": "2.0", "result": [5, 6, 7, 8, 9, 10], "id": 1 }
```

### getConfirmedBlocksWithLimit

**DEPRECATED: Please use getBlocksWithLimit instead** This method is expected to be removed in put-core v2.0

Returns a list of confirmed blocks starting at the given slot

**Parameters:**

* `<u64>` - start\_slot, as u64 integer
* `<u64>` - limit, as u64 integer
* (optional) `<object>` - Configuration object containing the following field:
  * (optional) `commitment: <string>` - Commitment; "processed" is not supported. If parameter not provided, the default is "finalized".

**Results:**

The result field will be an array of u64 integers listing confirmed blocks starting at `start_slot` for up to `limit` blocks, inclusive.

**Example:**

Request:

```bash
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {"jsonrpc": "2.0","id":1,"method":"getConfirmedBlocksWithLimit","params":[5, 3]}
'
```

Result:

```json
{ "jsonrpc": "2.0", "result": [5, 6, 7], "id": 1 }
```

### getConfirmedSignaturesForAddress2

**DEPRECATED: Please use getSignaturesForAddress instead** This method is expected to be removed in put-core v2.0

Returns signatures for confirmed transactions that include the given address in their `accountKeys` list. Returns signatures backwards in time from the provided signature or most recent confirmed block

**Parameters:**

* `<string>` - account address as base-58 encoded string
* (optional) `<object>` - Configuration object containing the following fields:
  * (optional) `limit: <number>` - maximum transaction signatures to return (between 1 and 1,000, default: 1,000).
  * (optional) `before: <string>` - start searching backwards from this transaction signature. If not provided the search starts from the top of the highest max confirmed block.
  * (optional) `until: <string>` - search until this transaction signature, if found before limit reached.
  * (optional) `commitment: <string>` - Commitment; "processed" is not supported. If parameter not provided, the default is "finalized".

**Results:**

The result field will be an array of transaction signature information, ordered from newest to oldest transaction:

* `<object>`
  * `signature: <string>` - transaction signature as base-58 encoded string
  * `slot: <u64>` - The slot that contains the block with the transaction
  * `err: <object|null>` - Error if transaction failed, null if transaction succeeded. [TransactionError definitions](https://github.com/put-labs/put/blob/c0c60386544ec9a9ec7119229f37386d9f070523/sdk/src/transaction/error.rs#L13)
  * `memo: <string|null>` - Memo associated with the transaction, null if no memo is present
  * `blockTime: <i64|null>` - estimated production time, as Unix timestamp (seconds since the Unix epoch) of when transaction was processed. null if not available.

**Example:**

Request:

```bash
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {
    "jsonrpc": "2.0",
    "id": 1,
    "method": "getConfirmedSignaturesForAddress2",
    "params": [
      "Vote111111111111111111111111111111111111111",
      {
        "limit": 1
      }
    ]
  }
'
```

Result:

```json
{
  "jsonrpc": "2.0",
  "result": [
    {
      "err": null,
      "memo": null,
      "signature": "5h6xBEauJ3PK6SWCZ1PGjBvj8vDdWG3KpwATGy1ARAXFSDwt8GFXM7W5Ncn16wmqokgpiKRLuS83KUxyZyv2sUYv",
      "slot": 114,
      "blockTime": null
    }
  ],
  "id": 1
}
```

### getConfirmedTransaction

**DEPRECATED: Please use getTransaction instead** This method is expected to be removed in put-core v2.0

Returns transaction details for a confirmed transaction

**Parameters:**

* `<string>` - transaction signature as base-58 encoded string
* (optional) `<object>` - Configuration object containing the following optional fields:
  * (optional) `encoding: <string>` - encoding for each returned Transaction, either "json", "jsonParsed", "base58" (_slow_), "base64". If parameter not provided, the default encoding is "json". "jsonParsed" encoding attempts to use program-specific instruction parsers to return more human-readable and explicit data in the `transaction.message.instructions` list. If "jsonParsed" is requested but a parser cannot be found, the instruction falls back to regular JSON encoding (`accounts`, `data`, and `programIdIndex` fields).
  * (optional) `commitment: <string>` - Commitment; "processed" is not supported. If parameter not provided, the default is "finalized".

**Results:**

* `<null>` - if transaction is not found or not confirmed
* `<object>` - if transaction is confirmed, an object with the following fields:
  * `slot: <u64>` - the slot this transaction was processed in
  * `transaction: <object|[string,encoding]>` - Transaction object, either in JSON format or encoded binary data, depending on encoding parameter
  * `blockTime: <i64|null>` - estimated production time, as Unix timestamp (seconds since the Unix epoch) of when the transaction was processed. null if not available
  * `meta: <object|null>` - transaction status metadata object:
    * `err: <object|null>` - Error if transaction failed, null if transaction succeeded. [TransactionError definitions](https://docs.rs/put-sdk/VERSION\_FOR\_DOCS\_RS/put\_sdk/transaction/enum.TransactionError.html)
    * `fee: <u64>` - fee this transaction was charged, as u64 integer
    * `preBalances: <array>` - array of u64 account balances from before the transaction was processed
    * `postBalances: <array>` - array of u64 account balances after the transaction was processed
    * `innerInstructions: <array|null>` - List of inner instructions or `null` if inner instruction recording was not enabled during this transaction
    * `preTokenBalances: <array|undefined>` - List of token balances from before the transaction was processed or omitted if token balance recording was not yet enabled during this transaction
    * `postTokenBalances: <array|undefined>` - List of token balances from after the transaction was processed or omitted if token balance recording was not yet enabled during this transaction
    * `logMessages: <array|null>` - array of string log messages or `null` if log message recording was not enabled during this transaction
    * DEPRECATED: `status: <object>` - Transaction status
      * `"Ok": <null>` - Transaction was successful
      * `"Err": <ERR>` - Transaction failed with TransactionError

**Example:**

Request:

```bash
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {
    "jsonrpc": "2.0",
    "id": 1,
    "method": "getConfirmedTransaction",
    "params": [
      "2nBhEBYYvfaAe16UMNqRHre4YNSskvuYgx3M6E4JP1oDYvZEJHvoPzyUidNgNX5r9sTyN1J9UxtbCXy2rqYcuyuv",
      "json"
    ]
  }
'
```

Result:

```json
{
  "jsonrpc": "2.0",
  "result": {
    "meta": {
      "err": null,
      "fee": 5000,
      "innerInstructions": [],
      "postBalances": [499998932500, 26858640, 1, 1, 1],
      "postTokenBalances": [],
      "preBalances": [499998937500, 26858640, 1, 1, 1],
      "preTokenBalances": [],
      "status": {
        "Ok": null
      }
    },
    "slot": 430,
    "transaction": {
      "message": {
        "accountKeys": [
          "3UVYmECPPMZSCqWKfENfuoTv51fTDTWicX9xmBD2euKe",
          "AjozzgE83A3x1sHNUR64hfH7zaEBWeMaFuAN9kQgujrc",
          "SysvarS1otHashes111111111111111111111111111",
          "SysvarC1ock11111111111111111111111111111111",
          "Vote111111111111111111111111111111111111111"
        ],
        "header": {
          "numReadonlySignedAccounts": 0,
          "numReadonlyUnsignedAccounts": 3,
          "numRequiredSignatures": 1
        },
        "instructions": [
          {
            "accounts": [1, 2, 3, 0],
            "data": "37u9WtQpcm6ULa3WRQHmj49EPs4if7o9f1jSRVZpm2dvihR9C8jY4NqEwXUbLwx15HBSNcP1",
            "programIdIndex": 4
          }
        ],
        "recentBlockhash": "mfcyqEXB3DnHXki6KjjmZck6YjmZLvpAByy2fj4nh6B"
      },
      "signatures": [
        "2nBhEBYYvfaAe16UMNqRHre4YNSskvuYgx3M6E4JP1oDYvZEJHvoPzyUidNgNX5r9sTyN1J9UxtbCXy2rqYcuyuv"
      ]
    }
  },
  "blockTime": null,
  "id": 1
}
```

**Example:**

Request:

```bash
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {
    "jsonrpc": "2.0",
    "id": 1,
    "method": "getConfirmedTransaction",
    "params": [
      "2nBhEBYYvfaAe16UMNqRHre4YNSskvuYgx3M6E4JP1oDYvZEJHvoPzyUidNgNX5r9sTyN1J9UxtbCXy2rqYcuyuv",
      "base64"
    ]
  }
'
```

Result:

```json
{
  "jsonrpc": "2.0",
  "result": {
    "meta": {
      "err": null,
      "fee": 5000,
      "innerInstructions": [],
      "postBalances": [499998932500, 26858640, 1, 1, 1],
      "postTokenBalances": [],
      "preBalances": [499998937500, 26858640, 1, 1, 1],
      "preTokenBalances": [],
      "status": {
        "Ok": null
      }
    },
    "slot": 430,
    "transaction": [
      "AVj7dxHlQ9IrvdYVIjuiRFs1jLaDMHixgrv+qtHBwz51L4/ImLZhszwiyEJDIp7xeBSpm/TX5B7mYzxa+fPOMw0BAAMFJMJVqLw+hJYheizSoYlLm53KzgT82cDVmazarqQKG2GQsLgiqktA+a+FDR4/7xnDX7rsusMwryYVUdixfz1B1Qan1RcZLwqvxvJl4/t3zHragsUp0L47E24tAFUgAAAABqfVFxjHdMkoVmOYaR1etoteuKObS21cc1VbIQAAAAAHYUgdNXR0u3xNdiTr072z2DVec9EQQ/wNo1OAAAAAAAtxOUhPBp2WSjUNJEgfvy70BbxI00fZyEPvFHNfxrtEAQQEAQIDADUCAAAAAQAAAAAAAACtAQAAAAAAAAdUE18R96XTJCe+YfRfUp6WP+YKCy/72ucOL8AoBFSpAA==",
      "base64"
    ]
  },
  "id": 1
}
```

### getFeeCalculatorForBlockhash

**DEPRECATED: Please use isBlockhashValid or getFeeForMessage instead** This method is expected to be removed in put-core v2.0

Returns the fee calculator associated with the query blockhash, or `null` if the blockhash has expired

**Parameters:**

* `<string>` - query blockhash as a Base58 encoded string
* (optional) `<object>` - Configuration object containing the following fields:
  * (optional) `commitment: <string>` - Commitment
  * (optional) `minContextSlot: <number>` - set the minimum slot that the request can be evaluated at.

**Results:**

The result will be an RpcResponse JSON object with `value` equal to:

* `<null>` - if the query blockhash has expired
* `<object>` - otherwise, a JSON object containing:
  * `feeCalculator: <object>`, `FeeCalculator` object describing the cluster fee rate at the queried blockhash

**Example:**

Request:

```bash
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {
    "jsonrpc": "2.0",
    "id": 1,
    "method": "getFeeCalculatorForBlockhash",
    "params": [
      "GJxqhuxcgfn5Tcj6y3f8X4FeCDd2RQ6SnEMo1AAxrPRZ"
    ]
  }
'
```

Result:

```json
{
  "jsonrpc": "2.0",
  "result": {
    "context": {
      "slot": 221
    },
    "value": {
      "feeCalculator": {
        "lamportsPerSignature": 5000
      }
    }
  },
  "id": 1
}
```

### getFeeRateGovernor

Returns the fee rate governor information from the root bank

**Parameters:**

None

**Results:**

The `result` field will be an `object` with the following fields:

* `burnPercent: <u8>`, Percentage of fees collected to be destroyed
* `maxLamportsPerSignature: <u64>`, Largest value `lamportsPerSignature` can attain for the next slot
* `minLamportsPerSignature: <u64>`, Smallest value `lamportsPerSignature` can attain for the next slot
* `targetLamportsPerSignature: <u64>`, Desired fee rate for the cluster
* `targetSignaturesPerSlot: <u64>`, Desired signature rate for the cluster

**Example:**

Request:

```bash
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {"jsonrpc":"2.0","id":1, "method":"getFeeRateGovernor"}
'
```

Result:

```json
{
  "jsonrpc": "2.0",
  "result": {
    "context": {
      "slot": 54
    },
    "value": {
      "feeRateGovernor": {
        "burnPercent": 50,
        "maxLamportsPerSignature": 100000,
        "minLamportsPerSignature": 5000,
        "targetLamportsPerSignature": 10000,
        "targetSignaturesPerSlot": 20000
      }
    }
  },
  "id": 1
}
```

#### getFees

**DEPRECATED: Please use getFeeForMessage instead** This method is expected to be removed in put-core v2.0

Returns a recent block hash from the ledger, a fee schedule that can be used to compute the cost of submitting a transaction using it, and the last slot in which the blockhash will be valid.

**Parameters:**

* (optional) `<object>` - Configuration object containing the following field:
  * (optional) `commitment: <string>` - Commitment

**Results:**

The result will be an RpcResponse JSON object with `value` set to a JSON object with the following fields:

* `blockhash: <string>` - a Hash as base-58 encoded string
* `feeCalculator: <object>` - FeeCalculator object, the fee schedule for this block hash
* `lastValidSlot: <u64>` - DEPRECATED - this value is inaccurate and should not be relied upon
* `lastValidBlockHeight: <u64>` - last block height at which the blockhash will be valid

**Example:**

Request:

```bash
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {"jsonrpc":"2.0","id":1, "method":"getFees"}
'
```

Result:

```json
{
  "jsonrpc": "2.0",
  "result": {
    "context": {
      "slot": 1
    },
    "value": {
      "blockhash": "CSymwgTNX1j3E4qhKfJAUE41nBWEwXufoYryPbkde5RR",
      "feeCalculator": {
        "lamportsPerSignature": 5000
      },
      "lastValidSlot": 297,
      "lastValidBlockHeight": 296
    }
  },
  "id": 1
}
```

### getRecentBlockhash

**DEPRECATED: Please use getLatestBlockhash instead** This method is expected to be removed in put-core v2.0

Returns a recent block hash from the ledger, and a fee schedule that can be used to compute the cost of submitting a transaction using it.

**Parameters:**

* (optional) `<object>` - Configuration object containing the following field:
  * (optional) `commitment: <string>` - Commitment

**Results:**

An RpcResponse containing a JSON object consisting of a string blockhash and FeeCalculator JSON object.

* `RpcResponse<object>` - RpcResponse JSON object with `value` field set to a JSON object including:
* `blockhash: <string>` - a Hash as base-58 encoded string
* `feeCalculator: <object>` - FeeCalculator object, the fee schedule for this block hash

**Example:**

Request:

```bash
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {"jsonrpc":"2.0","id":1, "method":"getRecentBlockhash"}
'
```

Result:

```json
{
  "jsonrpc": "2.0",
  "result": {
    "context": {
      "slot": 1
    },
    "value": {
      "blockhash": "CSymwgTNX1j3E4qhKfJAUE41nBWEwXufoYryPbkde5RR",
      "feeCalculator": {
        "lamportsPerSignature": 5000
      }
    }
  },
  "id": 1
}
```

### getSnapshotSlot

**DEPRECATED: Please use getHighestSnapshotSlot instead** This method is expected to be removed in put-core v2.0

Returns the highest slot that the node has a snapshot for

**Parameters:**

None

**Results:**

* `<u64>` - Snapshot slot

**Example:**

Request:

```bash
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {"jsonrpc":"2.0","id":1, "method":"getSnapshotSlot"}
'
```

Result:

```json
{ "jsonrpc": "2.0", "result": 100, "id": 1 }
```

Result when the node has no snapshot:

```json
{
  "jsonrpc": "2.0",
  "error": { "code": -32008, "message": "No snapshot" },
  "id": 1
}
```
