# JSON RPC API -1

JSON RPC API

PUT nodes accept HTTP requests using the JSON-RPC 2.0 specification.

To interact with a PUT node inside a JavaScript application, use the put-web3.js library, which gives a convenient interface for the RPC methods.

## RPC HTTP Endpoint

Default port: 8899 e.g. http://localhost:8899, http://192.168.1.88:8899 RPC PubSub WebSocket Endpoint#

Default port: 8900 e.g. ws://localhost:8900, http://192.168.1.88:8900

## Methods

```
getAccountInfo
getBalance
getBlock
getBlockHeight
getBlockProduction
getBlockCommitment
getBlocks
getBlocksWithLimit
getBlockTime
getClusterNodes
getEpochInfo
getEpochSchedule
getFeeForMessage
getFirstAvailableBlock
getGenesisHash
getHealth
getHighestSnapshotSlot
getIdentity
getInflationGovernor
getInflationRate
getInflationReward
getLargestAccounts
getLatestBlockhash
getLeaderSchedule
getMaxRetransmitSlot
getMaxShredInsertSlot
getMinimumBalanceForRentExemption
getMultipleAccounts
getProgramAccounts
getRecentPerformanceSamples
getSignaturesForAddress
getSignatureStatuses
getSlot
getSlotLeader
getSlotLeaders
getStakeActivation
getStakeMinimumDelegation
getSupply
getTokenAccountBalance
getTokenAccountsByDelegate
getTokenAccountsByOwner
getTokenLargestAccounts
getTokenSupply
getTransaction
getTransactionCount
getVersion
getVoteAccounts
isBlockhashValid
minimumLedgerSlot
requestAirdrop
sendTransaction
simulateTransaction
Subscription Websocket
    accountSubscribe
    accountUnsubscribe
    logsSubscribe
    logsUnsubscribe
    programSubscribe
    programUnsubscribe
    signatureSubscribe
    signatureUnsubscribe
    slotSubscribe
    slotUnsubscribe
```

### Unstable Methods

Unstable methods may see breaking changes in patch releases and may not be supported in perpetuity.

```
blockSubscribe
blockUnsubscribe
slotsUpdatesSubscribe
slotsUpdatesUnsubscribe
voteSubscribe
voteUnsubscribe
```

### Deprecated Methods

```
getConfirmedBlock
getConfirmedBlocks
getConfirmedBlocksWithLimit
getConfirmedSignaturesForAddress2
getConfirmedTransaction
getFeeCalculatorForBlockhash
getFeeRateGovernor
getFees
getRecentBlockhash
getSnapshotSlot
```

## Request Formatting

To make a JSON-RPC request, send an HTTP POST request with a Content-Type: application/json header. The JSON request data should contain 4 fields:

```
jsonrpc: <string>, set to "2.0"
id: <number>, a unique client-generated identifying integer
method: <string>, a string containing the method to be invoked
params: <array>, a JSON array of ordered parameter values
```

Example using curl:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {
    "jsonrpc": "2.0",
    "id": 1,
    "method": "getBalance",
    "params": [
      "83astBRguLMdt2h5U1Tpdq5tjFoJ6noeGwaY3mDLVcri"
    ]
  }
'
```

The response output will be a JSON object with the following fields:

```
jsonrpc: <string>, matching the request specification
id: <number>, matching the request identifier
result: <array|number|object|string>, requested data or success confirmation
```

Requests can be sent in batches by sending an array of JSON-RPC request objects as the data for a single POST.

## Definitions

```
Hash: A SHA-256 hash of a chunk of data.
Pubkey: The public key of a Ed25519 key-pair.
Transaction: A list of Put instructions signed by a client keypair to authorize those actions.
Signature: An Ed25519 signature of transaction's payload data including instructions. This can be used to identify transactions.
```

## Configuring State Commitment

For preflight checks and transaction processing, Put nodes choose which bank state to query based on a commitment requirement set by the client. The commitment describes how finalized a block is at that point in time. When querying the ledger state, it's recommended to use lower levels of commitment to report progress and higher levels to ensure the state will not be rolled back.

In descending order of commitment (most finalized to least finalized), clients may specify:

```
"finalized" - the node will query the most recent block confirmed by supermajority of the cluster as having reached maximum lockout, meaning the cluster has recognized this block as finalized
"confirmed" - the node will query the most recent block that has been voted on by supermajority of the cluster.
    It incorporates votes from gossip and replay.
    It does not count votes on descendants of a block, only direct votes on that block.
    This confirmation level also upholds "optimistic confirmation" guarantees in release 1.3 and onwards.
"processed" - the node will query its most recent block. Note that the block may still be skipped by the cluster.
```

For processing many dependent transactions in series, it's recommended to use "confirmed" commitment, which balances speed with rollback safety. For total safety, it's recommended to use"finalized" commitment.

Example#

The commitment parameter should be included as the last element in the params array:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {
    "jsonrpc": "2.0",
    "id": 1,
    "method": "getBalance",
    "params": [
      "83astBRguLMdt2h5U1Tpdq5tjFoJ6noeGwaY3mDLVcri",
      {
        "commitment": "finalized"
      }
    ]
  }
'
```

Default:#

If commitment configuration is not provided, the node will default to "finalized" commitment

Only methods that query bank state accept the commitment parameter. They are indicated in the API Reference below.

RpcResponse Structure#

Many methods that take a commitment parameter return an RpcResponse JSON object comprised of two parts:

```
context : An RpcResponseContext JSON structure including a slot field at which the operation was evaluated.
value : The value returned by the operation itself.
```

Parsed Responses#

Some methods support an encoding parameter, and can return account or instruction data in parsed JSON format if "encoding":"jsonParsed" is requested and the node has a parser for the owning program. Put nodes currently support JSON parsing for the following native and SPL programs:

```
Program	Account State	Instructions
Address Lookup	v1.14.4	v1.14.4
BPF Loader	n/a	stable
BPF Upgradeable Loader	stable	stable
Config	stable	
SPL Associated Token Account	n/a	stable
SPL Memo	n/a	stable
SPL Token	stable	stable
SPL Token 2022	stable	stable
Stake	stable	stable
Vote	stable	stable
```

The list of account parsers can be found here, and instruction parsers here.

## Health Check

Although not a JSON RPC API, a GET /health at the RPC HTTP Endpoint provides a health-check mechanism for use by load balancers or other network infrastructure. This request will always return a HTTP 200 OK response with a body of "ok", "behind" or "unknown" based on the following conditions:

```
If one or more --known-validator arguments are provided to put-validator, "ok" is returned when the node has within HEALTH_CHECK_SLOT_DISTANCE slots of the highest known validator, otherwise "behind". "unknown" is returned when no slot information from known validators is not yet available.

"ok" is always returned if no known validators are provided.
```

## JSON RPC API Reference\#

### getAccountInfo

Returns all information associated with the account of provided Pubkey

Parameters:#

```
<string> - Pubkey of account to query, as base-58 encoded string
(optional) <object> - Configuration object containing the following fields:
    (optional) commitment: <string> - Commitment
    (optional) encoding: <string> - encoding for Account data, either "base58" (slow), "base64", "base64+zstd", or "jsonParsed". "base58" is limited to Account data of less than 129 bytes. "base64" will return base64 encoded data for Account data of any size. "base64+zstd" compresses the Account data using Zstandard and base64-encodes the result. "jsonParsed" encoding attempts to use program-specific state parsers to return more human-readable and explicit account state data. If "jsonParsed" is requested but a parser cannot be found, the field falls back to "base64" encoding, detectable when the data field is type <string>.
    (optional) dataSlice: <object> - limit the returned account data using the provided offset: <usize> and length: <usize> fields; only available for "base58", "base64" or "base64+zstd" encodings.
    (optional) minContextSlot: <number> - set the minimum slot that the request can be evaluated at.
```

Results:#

The result will be an RpcResponse JSON object with value equal to:

```
<null> - if the requested account doesn't exist
<object> - otherwise, a JSON object containing:
    lamports: <u64>, number of lamports assigned to this account, as a u64
    owner: <string>, base-58 encoded Pubkey of the program this account has been assigned to
    data: <[string, encoding]|object>, data associated with the account, either as encoded binary data or JSON format {<program>: <state>}, depending on encoding parameter
    executable: <bool>, boolean indicating if the account contains a program (and is strictly read-only)
    rentEpoch: <u64>, the epoch at which this account will next owe rent, as u64
```

Example:#

Request:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {
    "jsonrpc": "2.0",
    "id": 1,
    "method": "getAccountInfo",
    "params": [
      "vines1vzrYbzLMRdu58ou5XTby4qAqVRLmqo36NKPTg",
      {
        "encoding": "base58"
      }
    ]
  }
'
```

Response:

```
{
  "jsonrpc": "2.0",
  "result": {
    "context": {
      "slot": 1
    },
    "value": {
      "data": [
        "11116bv5nS2h3y12kD1yUKeMZvGcKLSjQgX6BeV7u1FrjeJcKfsHRTPuR3oZ1EioKtYGiYxpxMG5vpbZLsbcBYBEmZZcMKaSoGx9JZeAuWf",
        "base58"
      ],
      "executable": false,
      "lamports": 1000000000,
      "owner": "11111111111111111111111111111111",
      "rentEpoch": 2
    }
  },
  "id": 1
}
```

Example:#

Request:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {
    "jsonrpc": "2.0",
    "id": 1,
    "method": "getAccountInfo",
    "params": [
      "4fYNw3dojWmQ4dXtSGE9epjRGy9pFSx62YypT7avPYvA",
      {
        "encoding": "jsonParsed"
      }
    ]
  }
'
```

Response:

```
{
  "jsonrpc": "2.0",
  "result": {
    "context": {
      "slot": 1
    },
    "value": {
      "data": {
        "nonce": {
          "initialized": {
            "authority": "Bbqg1M4YVVfbhEzwA9SpC9FhsaG83YMTYoR4a8oTDLX",
            "blockhash": "3xLP3jK6dVJwpeGeTDYTwdDK3TKchUf1gYYGHa4sF3XJ",
            "feeCalculator": {
              "lamportsPerSignature": 5000
            }
          }
        }
      },
      "executable": false,
      "lamports": 1000000000,
      "owner": "11111111111111111111111111111111",
      "rentEpoch": 2
    }
  },
  "id": 1
}
```

### getBalance

Returns the balance of the account of provided Pubkey Parameters:#

```
<string> - Pubkey of account to query, as base-58 encoded string
(optional) <object> - Configuration object containing the following fields:
    (optional) commitment: <string> - Commitment
    (optional) minContextSlot: <number> - set the minimum slot that the request can be evaluated at.
```

Results:#

```
RpcResponse<u64> - RpcResponse JSON object with value field set to the balance
```

Example:#

Request:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {"jsonrpc":"2.0", "id":1, "method":"getBalance", "params":["83astBRguLMdt2h5U1Tpdq5tjFoJ6noeGwaY3mDLVcri"]}
'
```

Result:

```
{
  "jsonrpc": "2.0",
  "result": { "context": { "slot": 1 }, "value": 0 },
  "id": 1
}
```

### getBlock

Returns identity and transaction information about a confirmed block in the ledger Parameters:#

```
<u64> - slot, as u64 integer
(optional) <object> - Configuration object containing the following optional fields:
    (optional) encoding: <string> - encoding for each returned Transaction, either "json", "jsonParsed", "base58" (slow), "base64". If parameter not provided, the default encoding is "json". "jsonParsed" encoding attempts to use program-specific instruction parsers to return more human-readable and explicit data in the transaction.message.instructions list. If "jsonParsed" is requested but a parser cannot be found, the instruction falls back to regular JSON encoding (accounts, data, and programIdIndex fields).
    (optional) transactionDetails: <string> - level of transaction detail to return, either "full", "accounts", "signatures", or "none". If parameter not provided, the default detail level is "full". If "accounts" are requested, transaction details only include signatures and an annotated list of accounts in each transaction. Transaction metadata is limited to only: fee, err, pre_balances, post_balances, pre_token_balances, and post_token_balances.
    (optional) rewards: bool - whether to populate the rewards array. If parameter not provided, the default includes rewards.
    (optional) commitment: <string> - Commitment; "processed" is not supported. If parameter not provided, the default is "finalized".
    (optional) maxSupportedTransactionVersion: <number> - set the max transaction version to return in responses. If the requested block contains a transaction with a higher version, an error will be returned. If this parameter is omitted, only legacy transactions will be returned, and a block containing any versioned transaction will prompt the error.
```

Results:#

The result field will be an object with the following fields:

```
<null> - if specified block is not confirmed
<object> - if block is confirmed, an object with the following fields:
    blockhash: <string> - the blockhash of this block, as base-58 encoded string
    previousBlockhash: <string> - the blockhash of this block's parent, as base-58 encoded string; if the parent block is not available due to ledger cleanup, this field will return "11111111111111111111111111111111"
    parentSlot: <u64> - the slot index of this block's parent
    transactions: <array> - present if "full" transaction details are requested; an array of JSON objects containing:
        transaction: <object|[string,encoding]> - Transaction object, either in JSON format or encoded binary data, depending on encoding parameter
        meta: <object> - transaction status metadata object, containing null or:
            err: <object|null> - Error if transaction failed, null if transaction succeeded. TransactionError definitions
            fee: <u64> - fee this transaction was charged, as u64 integer
            preBalances: <array> - array of u64 account balances from before the transaction was processed
            postBalances: <array> - array of u64 account balances after the transaction was processed
            innerInstructions: <array|null> - List of inner instructions or null if inner instruction recording was not enabled during this transaction
            preTokenBalances: <array|undefined> - List of token balances from before the transaction was processed or omitted if token balance recording was not yet enabled during this transaction
            postTokenBalances: <array|undefined> - List of token balances from after the transaction was processed or omitted if token balance recording was not yet enabled during this transaction
            logMessages: <array|null> - array of string log messages or null if log message recording was not enabled during this transaction
            rewards: <array|null> - transaction-level rewards, populated if rewards are requested; an array of JSON objects containing:
                pubkey: <string> - The public key, as base-58 encoded string, of the account that received the reward
                lamports: <i64>- number of reward lamports credited or debited by the account, as a i64
                postBalance: <u64> - account balance in lamports after the reward was applied
                rewardType: <string|undefined> - type of reward: "fee", "rent", "voting", "staking"
                commission: <u8|undefined> - vote account commission when the reward was credited, only present for voting and staking rewards
            DEPRECATED: status: <object> - Transaction status
                "Ok": <null> - Transaction was successful
                "Err": <ERR> - Transaction failed with TransactionError
            loadedAddresses: <object|undefined> - Transaction addresses loaded from address lookup tables. Undefined if maxSupportedTransactionVersion is not set in request params.
                writable: <array[string]> - Ordered list of base-58 encoded addresses for writable loaded accounts
                readonly: <array[string]> - Ordered list of base-58 encoded addresses for readonly loaded accounts
            returnData: <object|undefined> - the most-recent return data generated by an instruction in the transaction, with the following fields:
                programId: <string>, the program that generated the return data, as base-58 encoded Pubkey
                data: <[string, encoding]>, the return data itself, as base-64 encoded binary data
            computeUnitsConsumed: <u64|undefined>, number of compute units consumed by the transaction
        version: <"legacy"|number|undefined> - Transaction version. Undefined if maxSupportedTransactionVersion is not set in request params.
    signatures: <array> - present if "signatures" are requested for transaction details; an array of signatures strings, corresponding to the transaction order in the block
    rewards: <array|undefined> - block-level rewards, present if rewards are requested; an array of JSON objects containing:
        pubkey: <string> - The public key, as base-58 encoded string, of the account that received the reward
        lamports: <i64>- number of reward lamports credited or debited by the account, as a i64
        postBalance: <u64> - account balance in lamports after the reward was applied
        rewardType: <string|undefined> - type of reward: "fee", "rent", "voting", "staking"
        commission: <u8|undefined> - vote account commission when the reward was credited, only present for voting and staking rewards
    blockTime: <i64|null> - estimated production time, as Unix timestamp (seconds since the Unix epoch). null if not available
    blockHeight: <u64|null> - the number of blocks beneath this block
```

Example:#

Request:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {"jsonrpc": "2.0","id":1,"method":"getBlock","params":[430, {"encoding": "json","maxSupportedTransactionVersion":0,"transactionDetails":"full","rewards":false}]}
'
```

Result:

```
{
  "jsonrpc": "2.0",
  "result": {
    "blockHeight": 428,
    "blockTime": null,
    "blockhash": "3Eq21vXNB5s86c62bVuUfTeaMif1N2kUqRPBmGRJhyTA",
    "parentSlot": 429,
    "previousBlockhash": "mfcyqEXB3DnHXki6KjjmZck6YjmZLvpAByy2fj4nh6B",
    "transactions": [
      {
        "meta": {
          "err": null,
          "fee": 5000,
          "innerInstructions": [],
          "logMessages": [],
          "postBalances": [499998932500, 26858640, 1, 1, 1],
          "postTokenBalances": [],
          "preBalances": [499998937500, 26858640, 1, 1, 1],
          "preTokenBalances": [],
          "rewards": null,
          "status": {
            "Ok": null
          }
        },
    "transaction": {
      "message": {
        "accountKeys": [
          "3UVYmECPPMZSCqWKfENfuoTv51fTDTWicX9xmBD2euKe",
          "AjozzgE83A3x1sHNUR64hfH7zaEBWeMaFuAN9kQgujrc",
          "SysvarS1otHashes111111111111111111111111111",
          "SysvarC1ock11111111111111111111111111111111",
          "Vote111111111111111111111111111111111111111"
        ],
        "header": {
              "numReadonlySignedAccounts": 0,
              "numReadonlyUnsignedAccounts": 3,
              "numRequiredSignatures": 1
            },
            "instructions": [
              {
                "accounts": [1, 2, 3, 0],
                "data": "37u9WtQpcm6ULa3WRQHmj49EPs4if7o9f1jSRVZpm2dvihR9C8jY4NqEwXUbLwx15HBSNcP1",
                "programIdIndex": 4
              }
            ],
            "recentBlockhash": "mfcyqEXB3DnHXki6KjjmZck6YjmZLvpAByy2fj4nh6B"
          },
          "signatures": [
            "2nBhEBYYvfaAe16UMNqRHre4YNSskvuYgx3M6E4JP1oDYvZEJHvoPzyUidNgNX5r9sTyN1J9UxtbCXy2rqYcuyuv"
          ]
        }
      }
    ]
  },
  "id": 1
}
```

Example:#

Request:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {"jsonrpc": "2.0","id":1,"method":"getBlock","params":[430, "base64"]}
'
```

Result:

```
{
  "jsonrpc": "2.0",
  "result": {
    "blockHeight": 428,
    "blockTime": null,
    "blockhash": "3Eq21vXNB5s86c62bVuUfTeaMif1N2kUqRPBmGRJhyTA",
    "parentSlot": 429,
    "previousBlockhash": "mfcyqEXB3DnHXki6KjjmZck6YjmZLvpAByy2fj4nh6B",
    "rewards": [],
    "transactions": [
      {
        "meta": {
          "err": null,
          "fee": 5000,
          "innerInstructions": null,
          "logMessages": null,
          "postBalances": [499998932500, 26858640, 1, 1, 1],
          "postTokenBalances": [],
          "preBalances": [499998937500, 26858640, 1, 1, 1],
          "preTokenBalances": [],
          "rewards": [],
          "status": {
            "Ok": null
          }
        },
        "transaction": [
          "AVj7dxHlQ9IrvdYVIjuiRFs1jLaDMHixgrv+qtHBwz51L4/ImLZhszwiyEJDIp7xeBSpm/TX5B7mYzxa+fPOMw0BAAMFJMJVqLw+hJYheizSoYlLm53KzgT82cDVmazarqQKG2GQsLgiqktA+a+FDR4/7xnDX7rsusMwryYVUdixfz1B1Qan1RcZLwqvxvJl4/t3zHragsUp0L47E24tAFUgAAAABqfVFxjHdMkoVmOYaR1etoteuKObS21cc1VbIQAAAAAHYUgdNXR0u3xNdiTr072z2DVec9EQQ/wNo1OAAAAAAAtxOUhPBp2WSjUNJEgfvy70BbxI00fZyEPvFHNfxrtEAQQEAQIDADUCAAAAAQAAAAAAAACtAQAAAAAAAAdUE18R96XTJCe+YfRfUp6WP+YKCy/72ucOL8AoBFSpAA==",
          "base64"
        ]
      }
    ]
  },
  "id": 1
}
```

Transaction Structure#

Transactions are quite different from those on other blockchains. Be sure to review Anatomy of a Transaction to learn about transactions on Put.

The JSON structure of a transaction is defined as follows:

```
signatures: <array[string]> - A list of base-58 encoded signatures applied to the transaction. The list is always of length message.header.numRequiredSignatures and not empty. The signature at index i corresponds to the public key at index i in message.accountKeys. The first one is used as the transaction id.
message: <object> - Defines the content of the transaction.
    accountKeys: <array[string]> - List of base-58 encoded public keys used by the transaction, including by the instructions and for signatures. The first message.header.numRequiredSignatures public keys must sign the transaction.
    header: <object> - Details the account types and signatures required by the transaction.
        numRequiredSignatures: <number> - The total number of signatures required to make the transaction valid. The signatures must match the first numRequiredSignatures of message.accountKeys.
        numReadonlySignedAccounts: <number> - The last numReadonlySignedAccounts of the signed keys are read-only accounts. Programs may process multiple transactions that load read-only accounts within a single PoH entry, but are not permitted to credit or debit lamports or modify account data. Transactions targeting the same read-write account are evaluated sequentially.
        numReadonlyUnsignedAccounts: <number> - The last numReadonlyUnsignedAccounts of the unsigned keys are read-only accounts.
    recentBlockhash: <string> - A base-58 encoded hash of a recent block in the ledger used to prevent transaction duplication and to give transactions lifetimes.
    instructions: <array[object]> - List of program instructions that will be executed in sequence and committed in one atomic transaction if all succeed.
        programIdIndex: <number> - Index into the message.accountKeys array indicating the program account that executes this instruction.
        accounts: <array[number]> - List of ordered indices into the message.accountKeys array indicating which accounts to pass to the program.
        data: <string> - The program input data encoded in a base-58 string.
    addressTableLookups: <array[object]|undefined> - List of address table lookups used by a transaction to dynamically load addresses from on-chain address lookup tables. Undefined if maxSupportedTransactionVersion is not set.
        accountKey: <string> - base-58 encoded public key for an address lookup table account.
        writableIndexes: <array[number]> - List of indices used to load addresses of writable accounts from a lookup table.
        readonlyIndexes: <array[number]> - List of indices used to load addresses of readonly accounts from a lookup table.
```

Inner Instructions Structure#

The Put runtime records the cross-program instructions that are invoked during transaction processing and makes these available for greater transparency of what was executed on-chain per transaction instruction. Invoked instructions are grouped by the originating transaction instruction and are listed in order of processing.

The JSON structure of inner instructions is defined as a list of objects in the following structure:

```
index: number - Index of the transaction instruction from which the inner instruction(s) originated
instructions: <array[object]> - Ordered list of inner program instructions that were invoked during a single transaction instruction.
    programIdIndex: <number> - Index into the message.accountKeys array indicating the program account that executes this instruction.
    accounts: <array[number]> - List of ordered indices into the message.accountKeys array indicating which accounts to pass to the program.
    data: <string> - The program input data encoded in a base-58 string.
```

Token Balances Structure#

The JSON structure of token balances is defined as a list of objects in the following structure:

```
accountIndex: <number> - Index of the account in which the token balance is provided for.
mint: <string> - Pubkey of the token's mint.
owner: <string|undefined> - Pubkey of token balance's owner.
programId: <string|undefined> - Pubkey of the Token program that owns the account.
uiTokenAmount: <object> -
    amount: <string> - Raw amount of tokens as a string, ignoring decimals.
    decimals: <number> - Number of decimals configured for token's mint.
    uiAmount: <number|null> - Token amount as a float, accounting for decimals. DEPRECATED
    uiAmountString: <string> - Token amount as a string, accounting for decimals.
```

### getBlockHeight

Returns the current block height of the node Parameters:#

```
(optional) <object> - Configuration object containing the following fields:
    (optional) commitment: <string> - Commitment
    (optional) minContextSlot: <number> - set the minimum slot that the request can be evaluated at.
```

Results:#

```
<u64> - Current block height
```

Example:#

Request:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {"jsonrpc":"2.0","id":1, "method":"getBlockHeight"}
'
```

Result:

```
{ "jsonrpc": "2.0", "result": 1233, "id": 1 }
```

### getBlockProduction

Returns recent block production information from the current or previous epoch.

Parameters:#

```
(optional) <object> - Configuration object containing the following optional fields:
    (optional) commitment: <string> - Commitment
    (optional) range: <object> - Slot range to return block production for. If parameter not provided, defaults to current epoch.
        firstSlot: <u64> - first slot to return block production information for (inclusive)
        (optional) lastSlot: <u64> - last slot to return block production information for (inclusive). If parameter not provided, defaults to the highest slot
    (optional) identity: <string> - Only return results for this validator identity (base-58 encoded)
```

Results:#

The result will be an RpcResponse JSON object with value equal to:

```
<object>
    byIdentity: <object> - a dictionary of validator identities, as base-58 encoded strings. Value is a two element array containing the number of leader slots and the number of blocks produced.
    range: <object> - Block production slot range
        firstSlot: <u64> - first slot of the block production information (inclusive)
        lastSlot: <u64> - last slot of block production information (inclusive)
```

Example:#

Request:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {"jsonrpc":"2.0","id":1, "method":"getBlockProduction"}
'
```

Result:

```
{
  "jsonrpc": "2.0",
  "result": {
    "context": {
      "slot": 9887
    },
    "value": {
      "byIdentity": {
        "85iYT5RuzRTDgjyRa3cP8SYhM2j21fj7NhfJ3peu1DPr": [9888, 9886]
      },
      "range": {
        "firstSlot": 0,
        "lastSlot": 9887
      }
    }
  },
  "id": 1
}
```

Example:#

Request:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {
    "jsonrpc": "2.0",
    "id": 1,
    "method": "getBlockProduction",
    "params": [
      {
        "identity": "85iYT5RuzRTDgjyRa3cP8SYhM2j21fj7NhfJ3peu1DPr",
        "range": {
          "firstSlot": 40,
          "lastSlot": 50
        }
      }
    ]
  }
'
```

Result:

```
{
  "jsonrpc": "2.0",
  "result": {
    "context": {
      "slot": 10102
    },
    "value": {
      "byIdentity": {
        "85iYT5RuzRTDgjyRa3cP8SYhM2j21fj7NhfJ3peu1DPr": [11, 11]
      },
      "range": {
        "firstSlot": 50,
        "lastSlot": 40
      }
    }
  },
  "id": 1
}
```

### getBlockCommitment

Returns commitment for particular block Parameters:#

```
<u64> - block, identified by Slot
```

Results:#

The result field will be a JSON object containing:

```
commitment - commitment, comprising either:
    <null> - Unknown block
    <array> - commitment, array of u64 integers logging the amount of cluster stake in lamports that has voted on the block at each depth from 0 to MAX_LOCKOUT_HISTORY + 1
totalStake - total active stake, in lamports, of the current epoch
```

Example:#

Request:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {"jsonrpc":"2.0","id":1, "method":"getBlockCommitment","params":[5]}
'
```

Result:

```
{
  "jsonrpc": "2.0",
  "result": {
    "commitment": [
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 10, 32
    ],
    "totalStake": 42
  },
  "id": 1
}
```

### getBlocks

Returns a list of confirmed blocks between two slots

Parameters:#

```
<u64> - start_slot, as u64 integer
(optional) <u64> - end_slot, as u64 integer (must be no more than 500,000 blocks higher than the start_slot)
(optional) <object> - Configuration object containing the following field:
    (optional) commitment: <string> - Commitment; "processed" is not supported. If parameter not provided, the default is "finalized".
```

Results:#

The result field will be an array of u64 integers listing confirmed blocks between start\_slot and either end\_slot, if provided, or latest confirmed block, inclusive. Max range allowed is 500,000 slots.

Example:#

Request:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {"jsonrpc": "2.0","id":1,"method":"getBlocks","params":[5, 10]}
'
```

Result: { "jsonrpc": "2.0", "result": \[5, 6, 7, 8, 9, 10], "id": 1 }

getBlocksWithLimit#

Returns a list of confirmed blocks starting at the given slot

Parameters:#

```
<u64> - start_slot, as u64 integer
<u64> - limit, as u64 integer (must be no more than 500,000 blocks higher than the start_slot)
(optional) <object> - Configuration object containing the following field:
    (optional) commitment: <string> - Commitment; "processed" is not supported. If parameter not provided, the default is "finalized".
```

Results:#

The result field will be an array of u64 integers listing confirmed blocks starting at start\_slot for up to limit blocks, inclusive.

Example:#

Request:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {"jsonrpc": "2.0","id":1,"method":"getBlocksWithLimit","params":[5, 3]}
'
```

Result:

```
{ "jsonrpc": "2.0", "result": [5, 6, 7], "id": 1 }
```

### getBlockTime

Returns the estimated production time of a block.

Each validator reports their UTC time to the ledger on a regular interval by intermittently adding a timestamp to a Vote for a particular block. A requested block's time is calculated from the stake-weighted mean of the Vote timestamps in a set of recent blocks recorded on the ledger.

Parameters:#

```
<u64> - block, identified by Slot
```

Results:#

```
<i64> - estimated production time, as Unix timestamp (seconds since the Unix epoch)
<null> - timestamp is not available for this block
```

Example:#

Request:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {"jsonrpc":"2.0","id":1, "method":"getBlockTime","params":[5]}
'
```

Result:

```
{ "jsonrpc": "2.0", "result": 1574721591, "id": 1 }
```

### getClusterNodes

Returns information about all the nodes participating in the cluster

Parameters:#

None

Results:#

The result field will be an array of JSON objects, each with the following sub fields:

```
pubkey: <string> - Node public key, as base-58 encoded string
gossip: <string|null> - Gossip network address for the node
tpu: <string|null> - TPU network address for the node
rpc: <string|null> - JSON RPC network address for the node, or null if the JSON RPC service is not enabled
version: <string|null> - The software version of the node, or null if the version information is not available
featureSet: <u32|null > - The unique identifier of the node's feature set
shredVersion: <u16|null> - The shred version the node has been configured to use
```

Example:#

Request:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {"jsonrpc":"2.0", "id":1, "method":"getClusterNodes"}
'
```

Result:

```
{
  "jsonrpc": "2.0",
  "result": [
    {
      "gossip": "10.239.6.48:8001",
      "pubkey": "9QzsJf7LPLj8GkXbYT3LFDKqsj2hHG7TA3xinJHu8epQ",
      "rpc": "10.239.6.48:8899",
      "tpu": "10.239.6.48:8856",
      "version": "1.0.0 c375ce1f"
    }
  ],
  "id": 1
}
```

### getEpochInfo

Returns information about the current epoch

Parameters:#

```
(optional) <object> - Configuration object containing the following fields:
    (optional) commitment: <string> - Commitment
    (optional) minContextSlot: <number> - set the minimum slot that the request can be evaluated at.
```

Results:#

The result field will be an object with the following fields:

```
absoluteSlot: <u64>, the current slot
blockHeight: <u64>, the current block height
epoch: <u64>, the current epoch
slotIndex: <u64>, the current slot relative to the start of the current epoch
slotsInEpoch: <u64>, the number of slots in this epoch
transactionCount: <u64|null>, total number of transactions processed without error since genesis
```

Example:#

Request:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {"jsonrpc":"2.0","id":1, "method":"getEpochInfo"}
'
```

Result:

```
{
  "jsonrpc": "2.0",
  "result": {
    "absoluteSlot": 166598,
    "blockHeight": 166500,
    "epoch": 27,
    "slotIndex": 2790,
    "slotsInEpoch": 8192,
    "transactionCount": 22661093
  },
  "id": 1
}
```

### getEpochInfo

Returns information about the current epoch

Parameters:#

```
(optional) <object> - Configuration object containing the following fields:
    (optional) commitment: <string> - Commitment
    (optional) minContextSlot: <number> - set the minimum slot that the request can be evaluated at.
```

Results:#

The result field will be an object with the following fields:

```
absoluteSlot: <u64>, the current slot
blockHeight: <u64>, the current block height
epoch: <u64>, the current epoch
slotIndex: <u64>, the current slot relative to the start of the current epoch
slotsInEpoch: <u64>, the number of slots in this epoch
transactionCount: <u64|null>, total number of transactions processed without error since genesis
```

Example:#

Request:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {"jsonrpc":"2.0","id":1, "method":"getEpochInfo"}
'
```

Result:

```
{
  "jsonrpc": "2.0",
  "result": {
    "absoluteSlot": 166598,
    "blockHeight": 166500,
    "epoch": 27,
    "slotIndex": 2790,
    "slotsInEpoch": 8192,
    "transactionCount": 22661093
  },
  "id": 1
}
```

### getEpochSchedule

Returns epoch schedule information from this cluster's genesis config

Parameters:#

None

Results:#

The result field will be an object with the following fields:

```
slotsPerEpoch: <u64>, the maximum number of slots in each epoch
leaderScheduleSlotOffset: <u64>, the number of slots before beginning of an epoch to calculate a leader schedule for that epoch
warmup: <bool>, whether epochs start short and grow
firstNormalEpoch: <u64>, first normal-length epoch, log2(slotsPerEpoch) - log2(MINIMUM_SLOTS_PER_EPOCH)
firstNormalSlot: <u64>, MINIMUM_SLOTS_PER_EPOCH * (2.pow(firstNormalEpoch) - 1)
```

Example:#

Request:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {"jsonrpc":"2.0","id":1, "method":"getEpochSchedule"}
'
```

Result:

```
{
  "jsonrpc": "2.0",
  "result": {
    "firstNormalEpoch": 8,
    "firstNormalSlot": 8160,
    "leaderScheduleSlotOffset": 8192,
    "slotsPerEpoch": 8192,
    "warmup": true
  },
  "id": 1
}
```

### getFeeForMessage

NEW: This method is only available in put-core v1.9 or newer. Please use getFees for put-core v1.8

Get the fee the network will charge for a particular Message

Parameters:#

```
message: <string> - Base-64 encoded Message
(optional) <object> - Configuration object containing the following optional fields:
    (optional) commitment: <string> - Commitment (used for retrieving blockhash)
    (optional) minContextSlot: <number> - set the minimum slot that the request can be evaluated at.
```

Results:#

```
<u64|null> - Fee corresponding to the message at the specified blockhash
```

Example:#

Request:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
{
  "id":1,
  "jsonrpc":"2.0",
  "method":"getFeeForMessage",
  "params":[
    "AQABAgIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEBAQAA",
    {
      "commitment":"processed"
    }
  ]
}
'
```

Result:

```
{
  "jsonrpc": "2.0",
  "result": { "context": { "slot": 5068 }, "value": 5000 },
  "id": 1
}
```

### getFirstAvailableBlock

Returns the slot of the lowest confirmed block that has not been purged from the ledger

Parameters:#

None

Results:#

```
<u64> - Slot
```

Example:#

Request:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {"jsonrpc":"2.0","id":1, "method":"getFirstAvailableBlock"}
'
```

Result:

```
{ "jsonrpc": "2.0", "result": 250000, "id": 1 }
```

### getGenesisHash

Returns the genesis hash

Parameters:#

None

Results:#

```
<string> - a Hash as base-58 encoded string
```

Example:#

Request:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {"jsonrpc":"2.0","id":1, "method":"getGenesisHash"}
'
```

Result:

```
{
  "jsonrpc": "2.0",
  "result": "GH7ome3EiwEr7tu9JuTh2dpYWBJK3z69Xm1ZE3MEE6JC",
  "id": 1
}
```

### getHealth

Returns the current health of the node.

If one or more --known-validator arguments are provided to put-validator, "ok" is returned when the node has within HEALTH\_CHECK\_SLOT\_DISTANCE slots of the highest known validator, otherwise an error is returned. "ok" is always returned if no known validators are provided.

Parameters:#

None

Results:#

If the node is healthy: "ok" If the node is unhealthy, a JSON RPC error response is returned. The specifics of the error response are UNSTABLE and may change in the future

Example:#

Request:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {"jsonrpc":"2.0","id":1, "method":"getHealth"}
'
```

Healthy Result:

```
{ "jsonrpc": "2.0", "result": "ok", "id": 1 }
```

Unhealthy Result (generic):

```
{
  "jsonrpc": "2.0",
  "error": {
    "code": -32005,
    "message": "Node is unhealthy",
    "data": {}
  },
  "id": 1
}
```

Unhealthy Result (if additional information is available)

```
{
  "jsonrpc": "2.0",
  "error": {
    "code": -32005,
    "message": "Node is behind by 42 slots",
    "data": {
      "numSlotsBehind": 42
    }
  },
  "id": 1
}
```

### getHighestSnapshotSlot

NEW: This method is only available in put-core v1.9 or newer. Please use getSnapshotSlot for put-core v1.8

Returns the highest slot information that the node has snapshots for.

This will find the highest full snapshot slot, and the highest incremental snapshot slot based on the full snapshot slot, if there is one.

Parameters:#

None

Results:#

```
<object>
    full: <u64> - Highest full snapshot slot
    incremental: <u64|undefined> - Highest incremental snapshot slot based on full
```

Example:#

Request:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {"jsonrpc":"2.0","id":1,"method":"getHighestSnapshotSlot"}
'
```

Result:

```
{ "jsonrpc": "2.0", "result": { "full": 100, "incremental": 110 }, "id": 1 }
```

Result when the node has no snapshot:

```
{
  "jsonrpc": "2.0",
  "error": { "code": -32008, "message": "No snapshot" },
  "id": 1
}
```

### getIdentity

Returns the identity pubkey for the current node

Parameters:#

None

Results:#

The result field will be a JSON object with the following fields:

```
identity, the identity pubkey of the current node (as a base-58 encoded string)
```

Example:#

Request:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {"jsonrpc":"2.0","id":1, "method":"getIdentity"}
'
```

Result:

```
{
  "jsonrpc": "2.0",
  "result": { "identity": "2r1F4iWqVcb8M1DbAjQuFpebkQHY9hcVU4WuW2DJBppN" },
  "id": 1
}
```

### getInflationGovernor

Returns the current inflation governor

Parameters:#

```
(optional) <object> - Configuration object containing the following field:
    (optional) commitment: <string> - Commitment
```

Results:#

The result field will be a JSON object with the following fields:

```
initial: <f64>, the initial inflation percentage from time 0
terminal: <f64>, terminal inflation percentage
taper: <f64>, rate per year at which inflation is lowered. Rate reduction is derived using the target slot time in genesis config
foundation: <f64>, percentage of total inflation allocated to the foundation
foundationTerm: <f64>, duration of foundation pool inflation in years
```

Example:#

Request:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {"jsonrpc":"2.0","id":1, "method":"getInflationGovernor"}
'
```

Result:

```
{
  "jsonrpc": "2.0",
  "result": {
    "foundation": 0.05,
    "foundationTerm": 7,
    "initial": 0.15,
    "taper": 0.15,
    "terminal": 0.015
  },
  "id": 1
}
```

### getInflationRate

Returns the specific inflation values for the current epoch

Parameters:#

None

Results:#

The result field will be a JSON object with the following fields:

```
total: <f64>, total inflation
validator: <f64>, inflation allocated to validators
foundation: <f64>, inflation allocated to the foundation
epoch: <u64>, epoch for which these values are valid
```

Example:#

Request:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {"jsonrpc":"2.0","id":1, "method":"getInflationRate"}
'
```

Result:

```
{
  "jsonrpc": "2.0",
  "result": {
    "epoch": 100,
    "foundation": 0.001,
    "total": 0.149,
    "validator": 0.148
  },
  "id": 1
}
```

### getInflationReward

Returns the inflation / staking reward for a list of addresses for an epoch

Parameters:#

```
<array> - An array of addresses to query, as base-58 encoded strings
(optional) <object> - Configuration object containing the following fields:
    (optional) commitment: <string> - Commitment
    (optional) epoch: <u64> - An epoch for which the reward occurs. If omitted, the previous epoch will be used
    (optional) minContextSlot: <number> - set the minimum slot that the request can be evaluated at.
```

Results#

The result field will be a JSON array with the following fields:

```
epoch: <u64>, epoch for which reward occured
effectiveSlot: <u64>, the slot in which the rewards are effective
amount: <u64>, reward amount in lamports
postBalance: <u64>, post balance of the account in lamports
commission: <u8|undefined> - vote account commission when the reward was credited
```

Example#

Request:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {
    "jsonrpc": "2.0",
    "id": 1,
    "method": "getInflationReward",
    "params": [
       ["6dmNQ5jwLeLk5REvio1JcMshcbvkYMwy26sJ8pbkvStu", "BGsqMegLpV6n6Ve146sSX2dTjUMj3M92HnU8BbNRMhF2"], {"epoch": 2}
    ]
  }
'
```

Response:

```
{
  "jsonrpc": "2.0",
  "result": [
    {
      "amount": 2500,
      "effectiveSlot": 224,
      "epoch": 2,
      "postBalance": 499999442500
    },
    null
  ],
  "id": 1
}
```

### getLargestAccounts

Returns the 20 largest accounts, by lamport balance (results may be cached up to two hours)

Parameters:#

```
(optional) <object> - Configuration object containing the following optional fields:
    (optional) commitment: <string> - Commitment
    (optional) filter: <string> - filter results by account type; currently supported: circulating|nonCirculating
```

Results:#

The result will be an RpcResponse JSON object with value equal to an array of:

```
<object> - otherwise, a JSON object containing:
    address: <string>, base-58 encoded address of the account
    lamports: <u64>, number of lamports in the account, as a u64
```

Example:#

Request:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {"jsonrpc":"2.0","id":1, "method":"getLargestAccounts"}
'
```

Result:

```
{
  "jsonrpc": "2.0",
  "result": {
    "context": {
      "slot": 54
    },
    "value": [
      {
	"lamports": 999974,
	"address": "99P8ZgtJYe1buSK8JXkvpLh8xPsCFuLYhz9hQFNw93WJ"
      },
      {
	"lamports": 42,
	"address": "uPwWLo16MVehpyWqsLkK3Ka8nLowWvAHbBChqv2FZeL"
      },
      {
	"lamports": 42,
	"address": "aYJCgU7REfu3XF8b3QhkqgqQvLizx8zxuLBHA25PzDS"
      },
      {
	"lamports": 42,
	"address": "CTvHVtQ4gd4gUcw3bdVgZJJqApXE9nCbbbP4VTS5wE1D"
      },
      {
	"lamports": 20,
	"address": "4fq3xJ6kfrh9RkJQsmVd5gNMvJbuSHfErywvEjNQDPxu"
      },
      {
	"lamports": 4,
	"address": "AXJADheGVp9cruP8WYu46oNkRbeASngN5fPCMVGQqNHa"
      },
      {
	"lamports": 2,
	"address": "8NT8yS6LiwNprgW4yM1jPPow7CwRUotddBVkrkWgYp24"
      },
      {
	"lamports": 1,
	"address": "SysvarEpochSchedu1e111111111111111111111111"
      },
      {
	"lamports": 1,
	"address": "11111111111111111111111111111111"
      },
      {
	"lamports": 1,
	"address": "Stake11111111111111111111111111111111111111"
      },
      {
	"lamports": 1,
	"address": "SysvarC1ock11111111111111111111111111111111"
      },
      {
	"lamports": 1,
	"address": "StakeConfig11111111111111111111111111111111"
      },
      {
	"lamports": 1,
	"address": "SysvarRent111111111111111111111111111111111"
      },
      {
	"lamports": 1,
	"address": "Config1111111111111111111111111111111111111"
      },
      {
	"lamports": 1,
	"address": "SysvarStakeHistory1111111111111111111111111"
      },
      {
	"lamports": 1,
	"address": "SysvarRecentB1ockHashes11111111111111111111"
      },
      {
	"lamports": 1,
	"address": "SysvarFees111111111111111111111111111111111"
      },
      {
	"lamports": 1,
	"address": "Vote111111111111111111111111111111111111111"
      }
    ]
  },
  "id": 1
}
```

getLatestBlockhash#

NEW: This method is only available in put-core v1.9 or newer. Please use getRecentBlockhash for put-core v1.8

Returns the latest blockhash Parameters:#

```
(optional) <object> - Configuration object containing the following fields:
    (optional) commitment: <string> - Commitment (used for retrieving blockhash)
    (optional) minContextSlot: <number> - set the minimum slot that the request can be evaluated at.
```

Results:#

```
RpcResponse<object> - RpcResponse JSON object with value field set to a JSON object including:
blockhash: <string> - a Hash as base-58 encoded string
lastValidBlockHeight: <u64> - last block height at which the blockhash will be valid
```

Example:#

Request:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {
    "id":1,
    "jsonrpc":"2.0",
    "method":"getLatestBlockhash",
    "params":[
      {
	"commitment":"processed"
      }
    ]
  }
'
```

Result:

```
{
  "jsonrpc": "2.0",
  "result": {
    "context": {
      "slot": 2792
    },
    "value": {
      "blockhash": "EkSnNWid2cvwEVnVx9aBqawnmiCNiDgp3gUdkDPTKN1N",
      "lastValidBlockHeight": 3090
    }
  },
  "id": 1
}
```

### getLeaderSchedule

Returns the leader schedule for an epoch Parameters:#

```
(optional) <u64> - Fetch the leader schedule for the epoch that corresponds to the provided slot. If unspecified, the leader schedule for the current epoch is fetched
(optional) <object> - Configuration object containing the following field:
    (optional) commitment: <string> - Commitment
    (optional) identity: <string> - Only return results for this validator identity (base-58 encoded)
```

Results:#

```
<null> - if requested epoch is not found
<object> - otherwise, the result field will be a dictionary of validator identities, as base-58 encoded strings, and their corresponding leader slot indices as values (indices are relative to the first slot in the requested epoch)
```

Example:#

Request:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {"jsonrpc":"2.0","id":1, "method":"getLeaderSchedule"}
'
```

Result:

```
{
  "jsonrpc": "2.0",
  "result": {
    "4Qkev8aNZcqFNSRhQzwyLMFSsi94jHqE8WNVTJzTP99F": [
      0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
      21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38,
      39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56,
      57, 58, 59, 60, 61, 62, 63
    ]
  },
  "id": 1
}
```

Example:#

Request:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {
    "jsonrpc": "2.0",
    "id": 1,
    "method": "getLeaderSchedule",
    "params": [
      null,
      {
	"identity": "4Qkev8aNZcqFNSRhQzwyLMFSsi94jHqE8WNVTJzTP99F"
      }
    ]
  }
'
```

Result:

```
{
  "jsonrpc": "2.0",
  "result": {
    "4Qkev8aNZcqFNSRhQzwyLMFSsi94jHqE8WNVTJzTP99F": [
      0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
      21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38,
      39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56,
      57, 58, 59, 60, 61, 62, 63
    ]
  },
  "id": 1
}
```

### getMaxRetransmitSlot

Get the max slot seen from retransmit stage.

Results:#

```
<u64> - Slot
```

Example:#

Request:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
{"jsonrpc":"2.0","id":1, "method":"getMaxRetransmitSlot"}
'
```

Result:

```
{ "jsonrpc": "2.0", "result": 1234, "id": 1 }
```

### getMaxShredInsertSlot

Get the max slot seen from after shred insert. Results:#

```
<u64> - Slot
```

Example:#

Request:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
{"jsonrpc":"2.0","id":1, "method":"getMaxShredInsertSlot"}
'
```

Result:

```
{ "jsonrpc": "2.0", "result": 1234, "id": 1 }
```

### getMinimumBalanceForRentExemption

Returns minimum balance required to make account rent exempt.

Parameters:#

```
<usize> - account data length
(optional) <object> - Configuration object containing the following field:
    (optional) commitment: <string> - Commitment
```

Results:#

```
<u64> - minimum lamports required in account
```

Example:#

Request:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
{"jsonrpc":"2.0", "id":1, "method":"getMinimumBalanceForRentExemption", "params":[50]}
'
```

Result:

```
{ "jsonrpc": "2.0", "result": 500, "id": 1 }
```

### getMultipleAccounts

Returns the account information for a list of Pubkeys.

Parameters:#

```
<array> - An array of Pubkeys to query, as base-58 encoded strings (up to a maximum of 100).
(optional) <object> - Configuration object containing the following fields:
    (optional) commitment: <string> - Commitment
    (optional) encoding: <string> - encoding for Account data, either "base58" (slow), "base64", "base64+zstd", or "jsonParsed". "base58" is limited to Account data of less than 129 bytes. "base64" will return base64 encoded data for Account data of any size. "base64+zstd" compresses the Account data using Zstandard and base64-encodes the result. "jsonParsed" encoding attempts to use program-specific state parsers to return more human-readable and explicit account state data. If "jsonParsed" is requested but a parser cannot be found, the field falls back to "base64" encoding, detectable when the data field is type <string>.
    (optional) dataSlice: <object> - limit the returned account data using the provided offset: <usize> and length: <usize> fields; only available for "base58", "base64" or "base64+zstd" encodings.
    (optional) minContextSlot: <number> - set the minimum slot that the request can be evaluated at.
```

Results:#

The result will be an RpcResponse JSON object with value equal to:

An array of:

```
<null> - if the account at that Pubkey doesn't exist
<object> - otherwise, a JSON object containing:
    lamports: <u64>, number of lamports assigned to this account, as a u64
    owner: <string>, base-58 encoded Pubkey of the program this account has been assigned to
    data: <[string, encoding]|object>, data associated with the account, either as encoded binary data or JSON format {<program>: <state>}, depending on encoding parameter
    executable: <bool>, boolean indicating if the account contains a program (and is strictly read-only)
    rentEpoch: <u64>, the epoch at which this account will next owe rent, as u64
```

Example:#

Request:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {
    "jsonrpc": "2.0",
    "id": 1,
    "method": "getMultipleAccounts",
    "params": [
      [
        "vines1vzrYbzLMRdu58ou5XTby4qAqVRLmqo36NKPTg",
        "4fYNw3dojWmQ4dXtSGE9epjRGy9pFSx62YypT7avPYvA"
      ],
      {
        "dataSlice": {
          "offset": 0,
          "length": 0
        }
      }
    ]
  }
```

'

Result:

```
{
  "jsonrpc": "2.0",
  "result": {
    "context": {
      "slot": 1
    },
    "value": [
      {
        "data": ["AAAAAAEAAAACtzNsyJrW0g==", "base64"],
        "executable": false,
        "lamports": 1000000000,
        "owner": "11111111111111111111111111111111",
        "rentEpoch": 2
      },
      {
        "data": ["", "base64"],
        "executable": false,
        "lamports": 5000000000,
        "owner": "11111111111111111111111111111111",
        "rentEpoch": 2
      }
    ]
  },
  "id": 1
```

}

Example:#

Request:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {
    "jsonrpc": "2.0",
    "id": 1,
    "method": "getMultipleAccounts",
    "params": [
      [
        "vines1vzrYbzLMRdu58ou5XTby4qAqVRLmqo36NKPTg",
        "4fYNw3dojWmQ4dXtSGE9epjRGy9pFSx62YypT7avPYvA"
      ],
      {
        "encoding": "base58"
      }
    ]
  }
```

'

Result:

```
{
  "jsonrpc": "2.0",
  "result": {
    "context": {
      "slot": 1
    },
    "value": [
      {
        "data": [
          "11116bv5nS2h3y12kD1yUKeMZvGcKLSjQgX6BeV7u1FrjeJcKfsHRTPuR3oZ1EioKtYGiYxpxMG5vpbZLsbcBYBEmZZcMKaSoGx9JZeAuWf",
          "base58"
        ],
        "executable": false,
        "lamports": 1000000000,
        "owner": "11111111111111111111111111111111",
        "rentEpoch": 2
      },
      {
        "data": ["", "base58"],
        "executable": false,
        "lamports": 5000000000,
        "owner": "11111111111111111111111111111111",
        "rentEpoch": 2
      }
    ]
  },
  "id": 1
}
```

### getProgramAccounts

Returns all accounts owned by the provided program Pubkey

Parameters:#

```
<string> - Pubkey of program, as base-58 encoded string
(optional) <object> - Configuration object containing the following fields:
    (optional) commitment: <string> - Commitment
    (optional) encoding: <string> - encoding for Account data, either "base58" (slow), "base64", "base64+zstd", or "jsonParsed". "base58" is limited to Account data of less than 129 bytes. "base64" will return base64 encoded data for Account data of any size. "base64+zstd" compresses the Account data using Zstandard and base64-encodes the result. "jsonParsed" encoding attempts to use program-specific state parsers to return more human-readable and explicit account state data. If "jsonParsed" is requested but a parser cannot be found, the field falls back to "base64" encoding, detectable when the data field is type <string>.
    (optional) dataSlice: <object> - limit the returned account data using the provided offset: <usize> and length: <usize> fields; only available for "base58", "base64" or "base64+zstd" encodings.
    (optional) filters: <array> - filter results using up to 4 filter objects; account must meet all filter criteria to be included in results
    (optional) withContext: bool - wrap the result in an RpcResponse JSON object.
    (optional) minContextSlot: <number> - set the minimum slot that the request can be evaluated at.
```

Filters:#

```
memcmp: <object> - compares a provided series of bytes with program account data at a particular offset. Fields:
    offset: <usize> - offset into program account data to start comparison
    bytes: <string> - data to match, as encoded string
    encoding: <string> - encoding for filter bytes data, either "base58" or "base64". Data is limited in size to 128 or fewer decoded bytes. NEW: This field, and base64 support generally, is only available in put-core v1.11.2 or newer. Please omit when querying nodes on earlier versions

dataSize: <u64> - compares the program account data length with the provided data size
```

Results:#

By default the result field will be an array of JSON objects. If withContext flag is set the array will be wrapped in an RpcResponse JSON object.

The array will contain:

```
pubkey: <string> - the account Pubkey as base-58 encoded string
account: <object> - a JSON object, with the following sub fields:
    lamports: <u64>, number of lamports assigned to this account, as a u64
    owner: <string>, base-58 encoded Pubkey of the program this account has been assigned to
    data: <[string,encoding]|object>, data associated with the account, either as encoded binary data or JSON format {<program>: <state>}, depending on encoding parameter
    executable: <bool>, boolean indicating if the account contains a program (and is strictly read-only)
    rentEpoch: <u64>, the epoch at which this account will next owe rent, as u64
```

Example:#

Request:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {"jsonrpc":"2.0", "id":1, "method":"getProgramAccounts", "params":["4Nd1mBQtrMJVYVfKf2PJy9NZUZdTAsp7D4xWLs4gDB4T"]}
'
```

Result:

```
{
  "jsonrpc": "2.0",
  "result": [
    {
      "account": {
        "data": "2R9jLfiAQ9bgdcw6h8s44439",
        "executable": false,
        "lamports": 15298080,
        "owner": "4Nd1mBQtrMJVYVfKf2PJy9NZUZdTAsp7D4xWLs4gDB4T",
        "rentEpoch": 28
      },
      "pubkey": "CxELquR1gPP8wHe33gZ4QxqGB3sZ9RSwsJ2KshVewkFY"
    }
  ],
  "id": 1
}
```

Example:#

Request:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {
    "jsonrpc": "2.0",
    "id": 1,
    "method": "getProgramAccounts",
    "params": [
      "4Nd1mBQtrMJVYVfKf2PJy9NZUZdTAsp7D4xWLs4gDB4T",
      {
        "filters": [
          {
            "dataSize": 17
          },
          {
            "memcmp": {
              "offset": 4,
              "bytes": "3Mc6vR"
            }
          }
        ]
      }
    ]
  }
'
```

Result:

```
{
  "jsonrpc": "2.0",
  "result": [
    {
      "account": {
        "data": "2R9jLfiAQ9bgdcw6h8s44439",
        "executable": false,
        "lamports": 15298080,
        "owner": "4Nd1mBQtrMJVYVfKf2PJy9NZUZdTAsp7D4xWLs4gDB4T",
        "rentEpoch": 28
      },
      "pubkey": "CxELquR1gPP8wHe33gZ4QxqGB3sZ9RSwsJ2KshVewkFY"
    }
  ],
  "id": 1
```

}

### getRecentPerformanceSamples

Returns a list of recent performance samples, in reverse slot order. Performance samples are taken every 60 seconds and include the number of transactions and slots that occur in a given time window.

Parameters:#

```
(optional) limit: <usize> - number of samples to return (maximum 720)
```

Results:#

An array of:

```
RpcPerfSample<object>
    slot: <u64> - Slot in which sample was taken at
    numTransactions: <u64> - Number of transactions in sample
    numSlots: <u64> - Number of slots in sample
    samplePeriodSecs: <u16> - Number of seconds in a sample window
```

Example:#

Request:

```
// Request
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {"jsonrpc":"2.0", "id":1, "method":"getRecentPerformanceSamples", "params": [4]}
'
```

Result:

```
{
  "jsonrpc": "2.0",
  "result": [
    {
      "numSlots": 126,
      "numTransactions": 126,
      "samplePeriodSecs": 60,
      "slot": 348125
    },
    {
      "numSlots": 126,
      "numTransactions": 126,
      "samplePeriodSecs": 60,
      "slot": 347999
    },
    {
      "numSlots": 125,
      "numTransactions": 125,
      "samplePeriodSecs": 60,
      "slot": 347873
    },
    {
      "numSlots": 125,
      "numTransactions": 125,
      "samplePeriodSecs": 60,
      "slot": 347748
    }
  ],
  "id": 1
}
```

### getSignaturesForAddress

Returns signatures for confirmed transactions that include the given address in their accountKeys list. Returns signatures backwards in time from the provided signature or most recent confirmed block

Parameters:#

```
<string> - account address as base-58 encoded string
(optional) <object> - Configuration object containing the following fields:
    (optional) limit: <number> - maximum transaction signatures to return (between 1 and 1,000, default: 1,000).
    (optional) before: <string> - start searching backwards from this transaction signature. If not provided the search starts from the top of the highest max confirmed block.
    (optional) until: <string> - search until this transaction signature, if found before limit reached.
    (optional) commitment: <string> - Commitment
    (optional) minContextSlot: <number> - set the minimum slot that the request can be evaluated at.
```

Results:#

The result field will be an array of transaction signature information, ordered from newest to oldest transaction:

```
<object>
    signature: <string> - transaction signature as base-58 encoded string
    slot: <u64> - The slot that contains the block with the transaction
    err: <object|null> - Error if transaction failed, null if transaction succeeded. TransactionError definitions
    memo: <string|null> - Memo associated with the transaction, null if no memo is present
    blockTime: <i64|null> - estimated production time, as Unix timestamp (seconds since the Unix epoch) of when transaction was processed. null if not available.
    confirmationStatus: <string|null> - The transaction's cluster confirmation status; either processed, confirmed, or finalized. See Commitment for more on optimistic confirmation.
```

Example:#

Request:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {
    "jsonrpc": "2.0",
    "id": 1,
    "method": "getSignaturesForAddress",
    "params": [
      "Vote111111111111111111111111111111111111111",
      {
        "limit": 1
      }
    ]
  }
'
```

Result:

```
{
  "jsonrpc": "2.0",
  "result": [
    {
      "err": null,
      "memo": null,
      "signature": "5h6xBEauJ3PK6SWCZ1PGjBvj8vDdWG3KpwATGy1ARAXFSDwt8GFXM7W5Ncn16wmqokgpiKRLuS83KUxyZyv2sUYv",
      "slot": 114,
      "blockTime": null
    }
  ],
  "id": 1
}
```

### getSignatureStatuses

Returns the statuses of a list of signatures. Unless the searchTransactionHistory configuration parameter is included, this method only searches the recent status cache of signatures, which retains statuses for all active slots plus MAX\_RECENT\_BLOCKHASHES rooted slots.

Parameters:#

```
<array> - An array of transaction signatures to confirm, as base-58 encoded strings (up to a maximum of 256)
(optional) <object> - Configuration object containing the following field:
    searchTransactionHistory: <bool> - if true, a Put node will search its ledger cache for any signatures not found in the recent status cache
```

Results:#

An RpcResponse containing a JSON object consisting of an array of TransactionStatus objects.

```
RpcResponse<object> - RpcResponse JSON object with value field:
```

An array of:

```
<null> - Unknown transaction
<object>
    slot: <u64> - The slot the transaction was processed
    confirmations: <usize|null> - Number of blocks since signature confirmation, null if rooted, as well as finalized by a supermajority of the cluster
    err: <object|null> - Error if transaction failed, null if transaction succeeded. TransactionError definitions
    confirmationStatus: <string|null> - The transaction's cluster confirmation status; either processed, confirmed, or finalized. See Commitment for more on optimistic confirmation.
    DEPRECATED: status: <object> - Transaction status
        "Ok": <null> - Transaction was successful
        "Err": <ERR> - Transaction failed with TransactionError
```

Example:#

Request:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {
    "jsonrpc": "2.0",
    "id": 1,
    "method": "getSignatureStatuses",
    "params": [
      [
        "5VERv8NMvzbJMEkV8xnrLkEaWRtSz9CosKDYjCJjBRnbJLgp8uirBgmQpjKhoR4tjF3ZpRzrFmBV6UjKdiSZkQUW",
        "5j7s6NiJS3JAkvgkoc18WVAsiSaci2pxB2A6ueCJP4tprA2TFg9wSyTLeYouxPBJEMzJinENTkpA52YStRW5Dia7"
      ]
    ]
  }
```

'

Result:

```
{
  "jsonrpc": "2.0",
  "result": {
    "context": {
      "slot": 82
    },
    "value": [
      {
        "slot": 72,
        "confirmations": 10,
        "err": null,
        "status": {
          "Ok": null
        },
        "confirmationStatus": "confirmed"
      },
      null
    ]
  },
  "id": 1
}
```

Example:#

Request:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {
    "jsonrpc": "2.0",
    "id": 1,
    "method": "getSignatureStatuses",
    "params": [
      [
        "5VERv8NMvzbJMEkV8xnrLkEaWRtSz9CosKDYjCJjBRnbJLgp8uirBgmQpjKhoR4tjF3ZpRzrFmBV6UjKdiSZkQUW"
      ],
      {
        "searchTransactionHistory": true
      }
    ]
  }
'
```

Result:

```
{
  "jsonrpc": "2.0",
  "result": {
    "context": {
      "slot": 82
    },
    "value": [
      {
        "slot": 48,
        "confirmations": null,
        "err": null,
        "status": {
          "Ok": null
        },
        "confirmationStatus": "finalized"
      },
      null
    ]
  },
  "id": 1
}
```

### getSlot

Returns the slot that has reached the given or default commitment level Parameters:#

```
(optional) <object> - Configuration object containing the following fields:
    (optional) commitment: <string> - Commitment
    (optional) minContextSlot: <number> - set the minimum slot that the request can be evaluated at.
```

Results:#

```
<u64> - Current slot
```

Example:#

Request:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {"jsonrpc":"2.0","id":1, "method":"getSlot"}
'
```

Result:

```
{ "jsonrpc": "2.0", "result": 1234, "id": 1 }
```

### getSlotLeader

Returns the current slot leader

Parameters:#

```
(optional) <object> - Configuration object containing the following fields:
    (optional) commitment: <string> - Commitment
    (optional) minContextSlot: <number> - set the minimum slot that the request can be evaluated at.
```

Results:#

```
<string> - Node identity Pubkey as base-58 encoded string
```

Example:#

Request:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {"jsonrpc":"2.0","id":1, "method":"getSlotLeader"}
'
```

Result:

```
{
  "jsonrpc": "2.0",
  "result": "ENvAW7JScgYq6o4zKZwewtkzzJgDzuJAFxYasvmEQdpS",
  "id": 1
}
```

### getSlotLeaders

Returns the slot leaders for a given slot range Parameters:#

```
<u64> - Start slot, as u64 integer
<u64> - Limit, as u64 integer (between 1 and 5,000)
```

Results:#

```
<array[string]> - Node identity public keys as base-58 encoded strings
```

Example:#

If the current slot is #99, query the next 10 leaders with the following request:

Request:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {"jsonrpc":"2.0","id":1, "method":"getSlotLeaders", "params":[100, 10]}
'
```

Result:

The first leader returned is the leader for slot #100:

```
{
  "jsonrpc": "2.0",
  "result": [
    "ChorusmmK7i1AxXeiTtQgQZhQNiXYU84ULeaYF1EH15n",
    "ChorusmmK7i1AxXeiTtQgQZhQNiXYU84ULeaYF1EH15n",
    "ChorusmmK7i1AxXeiTtQgQZhQNiXYU84ULeaYF1EH15n",
    "ChorusmmK7i1AxXeiTtQgQZhQNiXYU84ULeaYF1EH15n",
    "Awes4Tr6TX8JDzEhCZY2QVNimT6iD1zWHzf1vNyGvpLM",
    "Awes4Tr6TX8JDzEhCZY2QVNimT6iD1zWHzf1vNyGvpLM",
    "Awes4Tr6TX8JDzEhCZY2QVNimT6iD1zWHzf1vNyGvpLM",
    "Awes4Tr6TX8JDzEhCZY2QVNimT6iD1zWHzf1vNyGvpLM",
    "DWvDTSh3qfn88UoQTEKRV2JnLt5jtJAVoiCo3ivtMwXP",
    "DWvDTSh3qfn88UoQTEKRV2JnLt5jtJAVoiCo3ivtMwXP"
  ],
  "id": 1
}
```

### getStakeActivation

Returns epoch activation information for a stake account

Parameters:#

```
<string> - Pubkey of stake account to query, as base-58 encoded string
(optional) <object> - Configuration object containing the following fields:
    (optional) commitment: <string> - Commitment
    (optional) epoch: <u64> - epoch for which to calculate activation details. If parameter not provided, defaults to current epoch.
    (optional) minContextSlot: <number> - set the minimum slot that the request can be evaluated at.
```

Results:#

The result will be a JSON object with the following fields:

```
state: <string - the stake account's activation state, one of: active, inactive, activating, deactivating
active: <u64> - stake active during the epoch
inactive: <u64> - stake inactive during the epoch
```

Example:#

Request:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {"jsonrpc":"2.0","id":1, "method":"getStakeActivation", "params": ["CYRJWqiSjLitBAcRxPvWpgX3s5TvmN2SuRY3eEYypFvT"]}
'
```

Result:

```
{
  "jsonrpc": "2.0",
  "result": { "active": 197717120, "inactive": 0, "state": "active" },
  "id": 1
}
```

Example:#

Request:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {
    "jsonrpc": "2.0",
    "id": 1,
    "method": "getStakeActivation",
    "params": [
      "CYRJWqiSjLitBAcRxPvWpgX3s5TvmN2SuRY3eEYypFvT",
      {
        "epoch": 4
      }
    ]
  }
'
```

Result:

```
{
  "jsonrpc": "2.0",
  "result": {
    "active": 124429280,
    "inactive": 73287840,
    "state": "activating"
  },
  "id": 1
}
```

### getStakeMinimumDelegation

Returns the stake minimum delegation, in lamports.

Parameters:#

```
(optional) <object> - Configuration object containing the following field:
    (optional) commitment: <string> - Commitment
```

Results:#

The result will be an RpcResponse JSON object with value equal to:

```
<u64> - The stake minimum delegation, in lamports
```

Example:#

Request:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {"jsonrpc":"2.0","id":1,"method":"getStakeMinimumDelegation"}
'
```

Result:

```
{
  "jsonrpc": "2.0",
  "result": {
    "context": {
      "slot": 501
    },
    "value": 1000000000
  },
  "id": 1
}
```

getSupply#

Returns information about the current supply.

Parameters:#

```
(optional) <object> - Configuration object containing the following optional fields:
    (optional) commitment: <string> - Commitment
    (optional) excludeNonCirculatingAccountsList: <bool> - exclude non circulating accounts list from response
```

Results:#

The result will be an RpcResponse JSON object with value equal to a JSON object containing:

```
total: <u64> - Total supply in lamports
circulating: <u64> - Circulating supply in lamports
nonCirculating: <u64> - Non-circulating supply in lamports
nonCirculatingAccounts: <array> - an array of account addresses of non-circulating accounts, as strings. If excludeNonCirculatingAccountsList is enabled, the returned array will be empty.
```

Example:#

Request:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {"jsonrpc":"2.0", "id":1, "method":"getSupply"}
'
```

Result:

```
{
  "jsonrpc": "2.0",
  "result": {
    "context": {
      "slot": 1114
    },
    "value": {
      "circulating": 16000,
      "nonCirculating": 1000000,
      "nonCirculatingAccounts": [
        "FEy8pTbP5fEoqMV1GdTz83byuA8EKByqYat1PKDgVAq5",
        "9huDUZfxoJ7wGMTffUE7vh1xePqef7gyrLJu9NApncqA",
        "3mi1GmwEE3zo2jmfDuzvjSX9ovRXsDUKHvsntpkhuLJ9",
        "BYxEJTDerkaRWBem3XgnVcdhppktBXa2HbkHPKj2Ui4Z"
      ],
      "total": 1016000
    }
  },
  "id": 1
}
```

### getTokenAccountBalance

Returns the token balance of an SPL Token account.

Parameters:#

```
<string> - Pubkey of Token account to query, as base-58 encoded string
(optional) <object> - Configuration object containing the following field:
    (optional) commitment: <string> - Commitment
```

Results:#

The result will be an RpcResponse JSON object with value equal to a JSON object containing:

```
amount: <string> - the raw balance without decimals, a string representation of u64
decimals: <u8> - number of base 10 digits to the right of the decimal place
uiAmount: <number|null> - the balance, using mint-prescribed decimals DEPRECATED
uiAmountString: <string> - the balance as a string, using mint-prescribed decimals
```

For more details on returned data: The Token Balances Structure response from getBlock follows a similar structure. Example:#

Request:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {"jsonrpc":"2.0", "id":1, "method":"getTokenAccountBalance", "params": ["7fUAJdStEuGbc3sM84cKRL6yYaaSstyLSU4ve5oovLS7"]}
'
```

Result:

```
{
  "jsonrpc": "2.0",
  "result": {
    "context": {
      "slot": 1114
    },
    "value": {
      "amount": "9864",
      "decimals": 2,
      "uiAmount": 98.64,
      "uiAmountString": "98.64"
    },
    "id": 1
  }
}
```

### getTokenAccountsByDelegate

Returns all SPL Token accounts by approved Delegate.

Parameters:#

```
<string> - Pubkey of account delegate to query, as base-58 encoded string
<object> - Either:
    mint: <string> - Pubkey of the specific token Mint to limit accounts to, as base-58 encoded string; or
    programId: <string> - Pubkey of the Token program that owns the accounts, as base-58 encoded string
(optional) <object> - Configuration object containing the following fields:
    (optional) commitment: <string> - Commitment
    (optional) encoding: <string> - encoding for Account data, either "base58" (slow), "base64", "base64+zstd", or "jsonParsed". "base58" is limited to Account data of less than 129 bytes. "base64" will return base64 encoded data for Account data of any size. "base64+zstd" compresses the Account data using Zstandard and base64-encodes the result. "jsonParsed" encoding attempts to use program-specific state parsers to return more human-readable and explicit account state data. If "jsonParsed" is requested but a parser cannot be found, the field falls back to "base64" encoding, detectable when the data field is type <string>.
    (optional) dataSlice: <object> - limit the returned account data using the provided offset: <usize> and length: <usize> fields; only available for "base58", "base64" or "base64+zstd" encodings.
    (optional) minContextSlot: <number> - set the minimum slot that the request can be evaluated at.
```

Results:#

The result will be an RpcResponse JSON object with value equal to an array of JSON objects, which will contain:

```
pubkey: <string> - the account Pubkey as base-58 encoded string
account: <object> - a JSON object, with the following sub fields:
    lamports: <u64>, number of lamports assigned to this account, as a u64
    owner: <string>, base-58 encoded Pubkey of the program this account has been assigned to
    data: <object>, Token state data associated with the account, either as encoded binary data or in JSON format {<program>: <state>}
    executable: <bool>, boolean indicating if the account contains a program (and is strictly read-only)
    rentEpoch: <u64>, the epoch at which this account will next owe rent, as u64
```

When the data is requested with the jsonParsed encoding a format similar to that of the Token Balances Structure can be expected inside the structure, both for the tokenAmount and the delegatedAmount, with the latter being an optional object.

Example:#

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {
    "jsonrpc": "2.0",
    "id": 1,
    "method": "getTokenAccountsByDelegate",
    "params": [
      "4Nd1mBQtrMJVYVfKf2PJy9NZUZdTAsp7D4xWLs4gDB4T",
      {
        "programId": "TokenkegQfeZyiNwAJbNbGKPFXCWuBvf9Ss623VQ5DA"
      },
      {
        "encoding": "jsonParsed"
      }
    ]
  }
'
```

Result:

```
{
  "jsonrpc": "2.0",
  "result": {
    "context": {
      "slot": 1114
    },
    "value": [
      {
        "account": {
          "data": {
            "program": "spl-token",
            "parsed": {
              "info": {
                "tokenAmount": {
                  "amount": "1",
                  "decimals": 1,
                  "uiAmount": 0.1,
                  "uiAmountString": "0.1"
                },
                "delegate": "4Nd1mBQtrMJVYVfKf2PJy9NZUZdTAsp7D4xWLs4gDB4T",
                "delegatedAmount": {
                  "amount": "1",
                  "decimals": 1,
                  "uiAmount": 0.1,
                  "uiAmountString": "0.1"
                },
                "state": "initialized",
                "isNative": false,
                "mint": "3wyAj7Rt1TWVPZVteFJPLa26JmLvdb1CAKEFZm3NY75E",
                "owner": "CnPoSPKXu7wJqxe59Fs72tkBeALovhsCxYeFwPCQH9TD"
              },
              "type": "account"
            },
            "space": 165
          },
          "executable": false,
          "lamports": 1726080,
          "owner": "TokenkegQfeZyiNwAJbNbGKPFXCWuBvf9Ss623VQ5DA",
          "rentEpoch": 4
        },
        "pubkey": "28YTZEwqtMHWrhWcvv34se7pjS7wctgqzCPB3gReCFKp"
      }
    ]
  },
  "id": 1
}
```

getTokenAccountsByOwner#

Returns all SPL Token accounts by token owner.

Parameters:#

```
<string> - Pubkey of account owner to query, as base-58 encoded string
<object> - Either:
    mint: <string> - Pubkey of the specific token Mint to limit accounts to, as base-58 encoded string; or
    programId: <string> - Pubkey of the Token program that owns the accounts, as base-58 encoded string
(optional) <object> - Configuration object containing the following fields:
    (optional) commitment: <string> - Commitment
    (optional) encoding: <string> - encoding for Account data, either "base58" (slow), "base64", "base64+zstd", or "jsonParsed". "base58" is limited to Account data of less than 129 bytes. "base64" will return base64 encoded data for Account data of any size. "base64+zstd" compresses the Account data using Zstandard and base64-encodes the result. "jsonParsed" encoding attempts to use program-specific state parsers to return more human-readable and explicit account state data. If "jsonParsed" is requested but a parser cannot be found, the field falls back to "base64" encoding, detectable when the data field is type <string>.
    (optional) dataSlice: <object> - limit the returned account data using the provided offset: <usize> and length: <usize> fields; only available for "base58", "base64" or "base64+zstd" encodings.
    (optional) minContextSlot: <number> - set the minimum slot that the request can be evaluated at.
```

Results:#

The result will be an RpcResponse JSON object with value equal to an array of JSON objects, which will contain:

```
pubkey: <string> - the account Pubkey as base-58 encoded string
account: <object> - a JSON object, with the following sub fields:
    lamports: <u64>, number of lamports assigned to this account, as a u64
    owner: <string>, base-58 encoded Pubkey of the program this account has been assigned to
    data: <object>, Token state data associated with the account, either as encoded binary data or in JSON format {<program>: <state>}
    executable: <bool>, boolean indicating if the account contains a program (and is strictly read-only)
    rentEpoch: <u64>, the epoch at which this account will next owe rent, as u64
```

When the data is requested with the jsonParsed encoding a format similar to that of the Token Balances Structure can be expected inside the structure, both for the tokenAmount and the delegatedAmount, with the latter being an optional object.

Example:#

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {
    "jsonrpc": "2.0",
    "id": 1,
    "method": "getTokenAccountsByOwner",
    "params": [
      "4Qkev8aNZcqFNSRhQzwyLMFSsi94jHqE8WNVTJzTP99F",
      {
        "mint": "3wyAj7Rt1TWVPZVteFJPLa26JmLvdb1CAKEFZm3NY75E"
      },
      {
        "encoding": "jsonParsed"
      }
    ]
  }
'
```

Result:

```
{
  "jsonrpc": "2.0",
  "result": {
    "context": {
      "slot": 1114
    },
    "value": [
      {
        "account": {
          "data": {
            "program": "spl-token",
            "parsed": {
              "accountType": "account",
              "info": {
                "tokenAmount": {
                  "amount": "1",
                  "decimals": 1,
                  "uiAmount": 0.1,
                  "uiAmountString": "0.1"
                },
                "delegate": "4Nd1mBQtrMJVYVfKf2PJy9NZUZdTAsp7D4xWLs4gDB4T",
                "delegatedAmount": {
                  "amount": "1",
                  "decimals": 1,
                  "uiAmount": 0.1,
                  "uiAmountString": "0.1"
                },
                "state": "initialized",
                "isNative": false,
                "mint": "3wyAj7Rt1TWVPZVteFJPLa26JmLvdb1CAKEFZm3NY75E",
                "owner": "4Qkev8aNZcqFNSRhQzwyLMFSsi94jHqE8WNVTJzTP99F"
              },
              "type": "account"
            },
            "space": 165
          },
          "executable": false,
          "lamports": 1726080,
          "owner": "TokenkegQfeZyiNwAJbNbGKPFXCWuBvf9Ss623VQ5DA",
          "rentEpoch": 4
        },
        "pubkey": "C2gJg6tKpQs41PRS1nC8aw3ZKNZK3HQQZGVrDFDup5nx"
      }
    ]
  },
  "id": 1
}
```

getTokenLargestAccounts#

Returns the 20 largest accounts of a particular SPL Token type. Parameters:#

```
<string> - Pubkey of token Mint to query, as base-58 encoded string
(optional) <object> - Configuration object containing the following field:
    (optional) commitment: <string> - Commitment
```

Results:#

The result will be an RpcResponse JSON object with value equal to an array of JSON objects containing:

```
address: <string> - the address of the token account
amount: <string> - the raw token account balance without decimals, a string representation of u64
decimals: <u8> - number of base 10 digits to the right of the decimal place
uiAmount: <number|null> - the token account balance, using mint-prescribed decimals DEPRECATED
uiAmountString: <string> - the token account balance as a string, using mint-prescribed decimals
```

Example:#

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {"jsonrpc":"2.0", "id":1, "method":"getTokenLargestAccounts", "params": ["3wyAj7Rt1TWVPZVteFJPLa26JmLvdb1CAKEFZm3NY75E"]}
'
```

Result:

```
{
  "jsonrpc": "2.0",
  "result": {
    "context": {
      "slot": 1114
    },
    "value": [
      {
        "address": "FYjHNoFtSQ5uijKrZFyYAxvEr87hsKXkXcxkcmkBAf4r",
        "amount": "771",
        "decimals": 2,
        "uiAmount": 7.71,
        "uiAmountString": "7.71"
      },
      {
        "address": "BnsywxTcaYeNUtzrPxQUvzAWxfzZe3ZLUJ4wMMuLESnu",
        "amount": "229",
        "decimals": 2,
        "uiAmount": 2.29,
        "uiAmountString": "2.29"
      }
    ]
  },
  "id": 1
}
```

### getTokenSupply

Returns the total supply of an SPL Token type.

Parameters:#

```
<string> - Pubkey of token Mint to query, as base-58 encoded string
(optional) <object> - Configuration object containing the following field:
    (optional) commitment: <string> - Commitment
```

Results:#

The result will be an RpcResponse JSON object with value equal to a JSON object containing:

```
amount: <string> - the raw total token supply without decimals, a string representation of u64
decimals: <u8> - number of base 10 digits to the right of the decimal place
uiAmount: <number|null> - the total token supply, using mint-prescribed decimals DEPRECATED
uiAmountString: <string> - the total token supply as a string, using mint-prescribed decimals
```

Example:#

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {"jsonrpc":"2.0", "id":1, "method":"getTokenSupply", "params": ["3wyAj7Rt1TWVPZVteFJPLa26JmLvdb1CAKEFZm3NY75E"]}
'
```

Result:

```
{
  "jsonrpc": "2.0",
  "result": {
    "context": {
      "slot": 1114
    },
    "value": {
      "amount": "100000",
      "decimals": 2,
      "uiAmount": 1000,
      "uiAmountString": "1000"
    }
  },
  "id": 1
}
```

### getTransaction

Returns transaction details for a confirmed transaction

Parameters:#

```
<string> - transaction signature as base-58 encoded string
(optional) <object> - Configuration object containing the following optional fields:
    (optional) encoding: <string> - encoding for each returned Transaction, either "json", "jsonParsed", "base58" (slow), "base64". If parameter not provided, the default encoding is "json". "jsonParsed" encoding attempts to use program-specific instruction parsers to return more human-readable and explicit data in the transaction.message.instructions list. If "jsonParsed" is requested but a parser cannot be found, the instruction falls back to regular JSON encoding (accounts, data, and programIdIndex fields).
    (optional) commitment: <string> - Commitment; "processed" is not supported. If parameter not provided, the default is "finalized".
    (optional) maxSupportedTransactionVersion: <number> - set the max transaction version to return in responses. If the requested transaction is a higher version, an error will be returned. If this parameter is omitted, only legacy transactions will be returned, and any versioned transaction will prompt the error.
```

Results:#

```
<null> - if transaction is not found or not confirmed
<object> - if transaction is confirmed, an object with the following fields:
    slot: <u64> - the slot this transaction was processed in
    transaction: <object|[string,encoding]> - Transaction object, either in JSON format or encoded binary data, depending on encoding parameter
    blockTime: <i64|null> - estimated production time, as Unix timestamp (seconds since the Unix epoch) of when the transaction was processed. null if not available
    meta: <object|null> - transaction status metadata object:
        err: <object|null> - Error if transaction failed, null if transaction succeeded. TransactionError definitions
        fee: <u64> - fee this transaction was charged, as u64 integer
        preBalances: <array> - array of u64 account balances from before the transaction was processed
        postBalances: <array> - array of u64 account balances after the transaction was processed
        innerInstructions: <array|null> - List of inner instructions or null if inner instruction recording was not enabled during this transaction
        preTokenBalances: <array|undefined> - List of token balances from before the transaction was processed or omitted if token balance recording was not yet enabled during this transaction
        postTokenBalances: <array|undefined> - List of token balances from after the transaction was processed or omitted if token balance recording was not yet enabled during this transaction
        logMessages: <array|null> - array of string log messages or null if log message recording was not enabled during this transaction
        DEPRECATED: status: <object> - Transaction status
            "Ok": <null> - Transaction was successful
            "Err": <ERR> - Transaction failed with TransactionError
        rewards: <array|null> - transaction-level rewards, populated if rewards are requested; an array of JSON objects containing:
            pubkey: <string> - The public key, as base-58 encoded string, of the account that received the reward
            lamports: <i64>- number of reward lamports credited or debited by the account, as a i64
            postBalance: <u64> - account balance in lamports after the reward was applied
            rewardType: <string> - type of reward: currently only "rent", other types may be added in the future
            commission: <u8|undefined> - vote account commission when the reward was credited, only present for voting and staking rewards
        loadedAddresses: <object|undefined> - Transaction addresses loaded from address lookup tables. Undefined if maxSupportedTransactionVersion is not set in request params.
            writable: <array[string]> - Ordered list of base-58 encoded addresses for writable loaded accounts
            readonly: <array[string]> - Ordered list of base-58 encoded addresses for readonly loaded accounts
        returnData: <object|undefined> - the most-recent return data generated by an instruction in the transaction, with the following fields:
            programId: <string>, the program that generated the return data, as base-58 encoded Pubkey
            data: <[string, encoding]>, the return data itself, as base-64 encoded binary data
        computeUnitsConsumed: <u64|undefined>, number of compute units consumed by the transaction
    version: <"legacy"|number|undefined> - Transaction version. Undefined if maxSupportedTransactionVersion is not set in request params.
```

Example:#

Request:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {
    "jsonrpc": "2.0",
    "id": 1,
    "method": "getTransaction",
    "params": [
      "2nBhEBYYvfaAe16UMNqRHre4YNSskvuYgx3M6E4JP1oDYvZEJHvoPzyUidNgNX5r9sTyN1J9UxtbCXy2rqYcuyuv",
      "json"
    ]
  }
'
```

Result:

```
{
  "jsonrpc": "2.0",
  "result": {
    "meta": {
      "err": null,
      "fee": 5000,
      "innerInstructions": [],
      "postBalances": [499998932500, 26858640, 1, 1, 1],
      "postTokenBalances": [],
      "preBalances": [499998937500, 26858640, 1, 1, 1],
      "preTokenBalances": [],
      "rewards": [],
      "status": {
        "Ok": null
      }
    },
    "slot": 430,
    "transaction": {
      "message": {
        "accountKeys": [
          "3UVYmECPPMZSCqWKfENfuoTv51fTDTWicX9xmBD2euKe",
          "AjozzgE83A3x1sHNUR64hfH7zaEBWeMaFuAN9kQgujrc",
          "SysvarS1otHashes111111111111111111111111111",
          "SysvarC1ock11111111111111111111111111111111",
          "Vote111111111111111111111111111111111111111"
        ],
        "header": {
          "numReadonlySignedAccounts": 0,
          "numReadonlyUnsignedAccounts": 3,
          "numRequiredSignatures": 1
        },
        "instructions": [
          {
            "accounts": [1, 2, 3, 0],
            "data": "37u9WtQpcm6ULa3WRQHmj49EPs4if7o9f1jSRVZpm2dvihR9C8jY4NqEwXUbLwx15HBSNcP1",
            "programIdIndex": 4
          }
        ],
        "recentBlockhash": "mfcyqEXB3DnHXki6KjjmZck6YjmZLvpAByy2fj4nh6B"
      },
      "signatures": [
        "2nBhEBYYvfaAe16UMNqRHre4YNSskvuYgx3M6E4JP1oDYvZEJHvoPzyUidNgNX5r9sTyN1J9UxtbCXy2rqYcuyuv"
      ]
    }
  },
  "blockTime": null,
  "id": 1
}
```

Example:#

Request:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {
    "jsonrpc": "2.0",
    "id": 1,
    "method": "getTransaction",
    "params": [
      "2nBhEBYYvfaAe16UMNqRHre4YNSskvuYgx3M6E4JP1oDYvZEJHvoPzyUidNgNX5r9sTyN1J9UxtbCXy2rqYcuyuv",
      "base64"
    ]
  }
'
```

Result:

```
{
  "jsonrpc": "2.0",
  "result": {
    "meta": {
      "err": null,
      "fee": 5000,
      "innerInstructions": [],
      "postBalances": [499998932500, 26858640, 1, 1, 1],
      "postTokenBalances": [],
      "preBalances": [499998937500, 26858640, 1, 1, 1],
      "preTokenBalances": [],
      "rewards": null,
      "status": {
        "Ok": null
      }
    },
    "slot": 430,
    "transaction": [
      "AVj7dxHlQ9IrvdYVIjuiRFs1jLaDMHixgrv+qtHBwz51L4/ImLZhszwiyEJDIp7xeBSpm/TX5B7mYzxa+fPOMw0BAAMFJMJVqLw+hJYheizSoYlLm53KzgT82cDVmazarqQKG2GQsLgiqktA+a+FDR4/7xnDX7rsusMwryYVUdixfz1B1Qan1RcZLwqvxvJl4/t3zHragsUp0L47E24tAFUgAAAABqfVFxjHdMkoVmOYaR1etoteuKObS21cc1VbIQAAAAAHYUgdNXR0u3xNdiTr072z2DVec9EQQ/wNo1OAAAAAAAtxOUhPBp2WSjUNJEgfvy70BbxI00fZyEPvFHNfxrtEAQQEAQIDADUCAAAAAQAAAAAAAACtAQAAAAAAAAdUE18R96XTJCe+YfRfUp6WP+YKCy/72ucOL8AoBFSpAA==",
      "base64"
    ]
  },
  "id": 1
}
```

### getTransactionCount

Returns the current Transaction count from the ledger Parameters:#

```
(optional) <object> - Configuration object containing the following fields:
    (optional) commitment: <string> - Commitment
    (optional) minContextSlot: <number> - set the minimum slot that the request can be evaluated at.
```

Results:#

```
<u64> - count
```

Example:#

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {"jsonrpc":"2.0","id":1, "method":"getTransactionCount"}
'
```

Result: { "jsonrpc": "2.0", "result": 268, "id": 1 }

getVersion#

Returns the current put versions running on the node

Parameters:#

None

Results:#

The result field will be a JSON object with the following fields:

```
put-core, software version of put-core
feature-set, unique identifier of the current software's feature set
```

Example:#

Request: curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d ' {"jsonrpc":"2.0","id":1, "method":"getVersion"} '

Result: { "jsonrpc": "2.0", "result": { "put-core": "1.14.3" }, "id": 1 }

getVoteAccounts#

Returns the account info and associated stake for all the voting accounts in the current bank.

Parameters:#

```
(optional) <object> - Configuration object containing the following field:
    (optional) commitment: <string> - Commitment
    (optional) votePubkey: <string> - Only return results for this validator vote address (base-58 encoded)
    (optional) keepUnstakedDelinquents: <bool> - Do not filter out delinquent validators with no stake
    (optional) delinquentSlotDistance: <u64> - Specify the number of slots behind the tip that a validator must fall to be considered delinquent. NOTE: For the sake of consistency between ecosystem products, it is not recommended that this argument be specified.
```

Results:#

The result field will be a JSON object of current and delinquent accounts, each containing an array of JSON objects with the following sub fields:

```
votePubkey: <string> - Vote account address, as base-58 encoded string
nodePubkey: <string> - Validator identity, as base-58 encoded string
activatedStake: <u64> - the stake, in lamports, delegated to this vote account and active in this epoch
epochVoteAccount: <bool> - bool, whether the vote account is staked for this epoch
commission: <number>, percentage (0-100) of rewards payout owed to the vote account
lastVote: <u64> - Most recent slot voted on by this vote account
epochCredits: <array> - History of how many credits earned by the end of each epoch, as an array of arrays containing: [epoch, credits, previousCredits]
```

Example:#

Request:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {"jsonrpc":"2.0","id":1, "method":"getVoteAccounts"}
'
```

Result:

```
{
  "jsonrpc": "2.0",
  "result": {
    "current": [
      {
        "commission": 0,
        "epochVoteAccount": true,
        "epochCredits": [
          [1, 64, 0],
          [2, 192, 64]
        ],
        "nodePubkey": "B97CCUW3AEZFGy6uUg6zUdnNYvnVq5VG8PUtb2HayTDD",
        "lastVote": 147,
        "activatedStake": 42,
        "votePubkey": "3ZT31jkAGhUaw8jsy4bTknwBMP8i4Eueh52By4zXcsVw"
      }
    ],
    "delinquent": [
      {
        "commission": 127,
        "epochVoteAccount": false,
        "epochCredits": [],
        "nodePubkey": "6ZPxeQaDo4bkZLRsdNrCzchNQr5LN9QMc9sipXv9Kw8f",
        "lastVote": 0,
        "activatedStake": 0,
        "votePubkey": "CmgCk4aMS7KW1SHX3s9K5tBJ6Yng2LBaC8MFov4wx9sm"
      }
    ]
  },
  "id": 1
}
```

Example: Restrict results to a single validator vote account#

Request:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {
    "jsonrpc": "2.0",
    "id": 1,
    "method": "getVoteAccounts",
    "params": [
      {
        "votePubkey": "3ZT31jkAGhUaw8jsy4bTknwBMP8i4Eueh52By4zXcsVw"
      }
    ]
  }
'
```

Result:

```
{
  "jsonrpc": "2.0",
  "result": {
    "current": [
      {
        "commission": 0,
        "epochVoteAccount": true,
        "epochCredits": [
          [1, 64, 0],
          [2, 192, 64]
        ],
        "nodePubkey": "B97CCUW3AEZFGy6uUg6zUdnNYvnVq5VG8PUtb2HayTDD",
        "lastVote": 147,
        "activatedStake": 42,
        "votePubkey": "3ZT31jkAGhUaw8jsy4bTknwBMP8i4Eueh52By4zXcsVw"
      }
    ],
    "delinquent": []
  },
  "id": 1
}
```

### isBlockhashValid

NEW: This method is only available in put-core v1.9 or newer. Please use getFeeCalculatorForBlockhash for put-core v1.8

Returns whether a blockhash is still valid or not

Parameters:#

```
blockhash: <string> - the blockhash of this block, as base-58 encoded string
(optional) <object> - Configuration object containing the following fields:
    (optional) commitment: <string> - Commitment (used for retrieving blockhash)
    (optional) minContextSlot: <number> - set the minimum slot that the request can be evaluated at.
```

Results:#

```
<bool> - True if the blockhash is still valid
```

Example:#

Request:

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {
    "id":45,
    "jsonrpc":"2.0",
    "method":"isBlockhashValid",
    "params":[
      "J7rBdM6AecPDEZp8aPq5iPSNKVkU5Q76F3oAV4eW5wsW",
      {"commitment":"processed"}
    ]
  }
'
```

Result:

```
{
  "jsonrpc": "2.0",
  "result": {
    "context": {
      "slot": 2483
    },
    "value": false
  },
  "id": 1
}
```

### minimumLedgerSlot

Returns the lowest slot that the node has information about in its ledger. This value may increase over time if the node is configured to purge older ledger data

Parameters:#

None

Results:#

```
u64 - Minimum ledger slot
```

Example:#

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {"jsonrpc":"2.0","id":1, "method":"minimumLedgerSlot"}
'
```

Result:

```
{ "jsonrpc": "2.0", "result": 1234, "id": 1 }
```

### requestAirdrop

Requests an airdrop of lamports to a Pubkey

Parameters:#

```
<string> - Pubkey of account to receive lamports, as base-58 encoded string
<integer> - lamports, as a u64
(optional) <object> - Configuration object containing the following field:
    (optional) commitment: <string> - Commitment (used for retrieving blockhash and verifying airdrop success)
```

Results:#

```
<string> - Transaction Signature of airdrop, as base-58 encoded string
```

Example:#

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {"jsonrpc":"2.0","id":1, "method":"requestAirdrop", "params":["83astBRguLMdt2h5U1Tpdq5tjFoJ6noeGwaY3mDLVcri", 1000000000]}
'
```

Result:

```
{
  "jsonrpc": "2.0",
  "result": "5VERv8NMvzbJMEkV8xnrLkEaWRtSz9CosKDYjCJjBRnbJLgp8uirBgmQpjKhoR4tjF3ZpRzrFmBV6UjKdiSZkQUW",
  "id": 1
}
```

### sendTransaction

Submits a signed transaction to the cluster for processing.

This method does not alter the transaction in any way; it relays the transaction created by clients to the node as-is.

If the node's rpc service receives the transaction, this method immediately succeeds, without waiting for any confirmations. A successful response from this method does not guarantee the transaction is processed or confirmed by the cluster.

While the rpc service will reasonably retry to submit it, the transaction could be rejected if transaction's recent\_blockhash expires before it lands.

Use getSignatureStatuses to ensure a transaction is processed and confirmed.

Before submitting, the following preflight checks are performed:

```
The transaction signatures are verified
The transaction is simulated against the bank slot specified by the preflight commitment. On failure an error will be returned. Preflight checks may be disabled if desired. It is recommended to specify the same commitment and preflight commitment to avoid confusing behavior.
```

The returned signature is the first signature in the transaction, which is used to identify the transaction (transaction id). This identifier can be easily extracted from the transaction data before submission.

Parameters:#

```
<string> - fully-signed Transaction, as encoded string
(optional) <object> - Configuration object containing the following field:
    skipPreflight: <bool> - if true, skip the preflight transaction checks (default: false)
    (optional) preflightCommitment: <string> - Commitment level to use for preflight (default: "finalized").
    (optional) encoding: <string> - Encoding used for the transaction data. Either "base58" (slow, DEPRECATED), or "base64". (default: "base58").
    (optional) maxRetries: <usize> - Maximum number of times for the RPC node to retry sending the transaction to the leader. If this parameter not provided, the RPC node will retry the transaction until it is finalized or until the blockhash expires.
    (optional) minContextSlot: <number> - set the minimum slot at which to perform preflight transaction checks.
```

Results:#

```
<string> - First Transaction Signature embedded in the transaction, as base-58 encoded string (transaction id)
```

Example:#

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {
    "jsonrpc": "2.0",
    "id": 1,
    "method": "sendTransaction",
    "params": [
      "4hXTCkRzt9WyecNzV1XPgCDfGAZzQKNxLXgynz5QDuWWPSAZBZSHptvWRL3BjCvzUXRdKvHL2b7yGrRQcWyaqsaBCncVG7BFggS8w9snUts67BSh3EqKpXLUm5UMHfD7ZBe9GhARjbNQMLJ1QD3Spr6oMTBU6EhdB4RD8CP2xUxr2u3d6fos36PD98XS6oX8TQjLpsMwncs5DAMiD4nNnR8NBfyghGCWvCVifVwvA8B8TJxE1aiyiv2L429BCWfyzAme5sZW8rDb14NeCQHhZbtNqfXhcp2tAnaAT"
    ]
  }
'
```

Result:

```
{
  "jsonrpc": "2.0",
  "result": "2id3YC2jK9G5Wo2phDx4gJVAew8DcY5NAojnVuao8rkxwPYPe8cSwE5GzhEgJA2y8fVjDEo6iR6ykBvDxrTQrtpb",
  "id": 1
}
```

### simulateTransaction

Simulate sending a transaction

Parameters:#

```
<string> - Transaction, as an encoded string. The transaction must have a valid blockhash, but is not required to be signed.
(optional) <object> - Configuration object containing the following fields:
    sigVerify: <bool> - if true the transaction signatures will be verified (default: false, conflicts with replaceRecentBlockhash)
    (optional) commitment: <string> - Commitment level to simulate the transaction at (default: "finalized").
    (optional) encoding: <string> - Encoding used for the transaction data. Either "base58" (slow, DEPRECATED), or "base64". (default: "base58").
    (optional) replaceRecentBlockhash: <bool> - if true the transaction recent blockhash will be replaced with the most recent blockhash. (default: false, conflicts with sigVerify)
    (optional) accounts: <object> - Accounts configuration object containing the following fields:
        (optional) encoding: <string> - encoding for returned Account data, either "base64" (default), "base64+zstd" or "jsonParsed". "jsonParsed" encoding attempts to use program-specific state parsers to return more human-readable and explicit account state data. If "jsonParsed" is requested but a parser cannot be found, the field falls back to binary encoding, detectable when the data field is type <string>.
        addresses: <array> - An array of accounts to return, as base-58 encoded strings
    (optional) minContextSlot: <number> - set the minimum slot that the request can be evaluated at.
```

Results:#

An RpcResponse containing a TransactionStatus object The result will be an RpcResponse JSON object with value set to a JSON object with the following fields:

```
err: <object|string|null> - Error if transaction failed, null if transaction succeeded. TransactionError definitions
logs: <array|null> - Array of log messages the transaction instructions output during execution, null if simulation failed before the transaction was able to execute (for example due to an invalid blockhash or signature verification failure)
accounts: <array|null> - array of accounts with the same length as the accounts.addresses array in the request
    <null> - if the account doesn't exist or if err is not null
    <object> - otherwise, a JSON object containing:
        lamports: <u64>, number of lamports assigned to this account, as a u64
        owner: <string>, base-58 encoded Pubkey of the program this account has been assigned to
        data: <[string, encoding]|object>, data associated with the account, either as encoded binary data or JSON format {<program>: <state>}, depending on encoding parameter
        executable: <bool>, boolean indicating if the account contains a program (and is strictly read-only)
        rentEpoch: <u64>, the epoch at which this account will next owe rent, as u64
unitsConsumed: <u64|undefined>, The number of compute budget units consumed during the processing of this transaction
returnData: <object|null> - the most-recent return data generated by an instruction in the transaction, with the following fields:
    programId: <string>, the program that generated the return data, as base-58 encoded Pubkey
    data: <[string, encoding]>, the return data itself, as base-64 encoded binary data
```

Example:#

```
curl http://localhost:8899 -X POST -H "Content-Type: application/json" -d '
  {
    "jsonrpc": "2.0",
    "id": 1,
    "method": "simulateTransaction",
    "params": [
      "AQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAEDArczbMia1tLmq7zz4DinMNN0pJ1JtLdqIJPUw3YrGCzYAMHBsgN27lcgB6H2WQvFgyZuJYHa46puOQo9yQ8CVQbd9uHXZaGT2cvhRs7reawctIXtX1s3kTqM9YV+/wCp20C7Wj2aiuk5TReAXo+VTVg8QTHjs0UjNMMKCvpzZ+ABAgEBARU=",
      {
        "encoding":"base64",
      }
    ]
  }
'
```

Result:

```
{
  "jsonrpc": "2.0",
  "result": {
    "context": {
      "slot": 218
    },
    "value": {
      "err": null,
      "accounts": null,
      "logs": [
        "Program 83astBRguLMdt2h5U1Tpdq5tjFoJ6noeGwaY3mDLVcri invoke [1]",
        "Program 83astBRguLMdt2h5U1Tpdq5tjFoJ6noeGwaY3mDLVcri consumed 2366 of 1400000 compute units",
        "Program return: 83astBRguLMdt2h5U1Tpdq5tjFoJ6noeGwaY3mDLVcri KgAAAAAAAAA=",
        "Program 83astBRguLMdt2h5U1Tpdq5tjFoJ6noeGwaY3mDLVcri success"
      ],
      "returnData": {
        "data": [
          "Kg==",
          "base64"
        ],
        "programId": "83astBRguLMdt2h5U1Tpdq5tjFoJ6noeGwaY3mDLVcri"
      },
      "unitsConsumed": 2366
    }
  },
  "id": 1
}
```

##
