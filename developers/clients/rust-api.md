# Rust API



Rust API

Put's Rust crates are published to crates.io and can be found on docs.rs with the "put-" prefix.

Some important crates:

* put-program — Imported by programs running on Put, compiled to BPF. This crate contains many fundamental data types and is re-exported from put-sdk, which cannot be imported from a Put program.
* put-sdk — The basic off-chain SDK, it re-exports put-program and adds more APIs on top of that. Most Put programs that do not run on-chain will import this.
* put-client — For interacting with a Put node via the JSON RPC API.put-cli-config — Loading and saving the Put CLI configuration file.
* put-clap-utils — Routines for setting up a CLI, using clap, as used by the main Put CLI. Includes functions for loading all types of signers supported by the CLI.
