# Rent



## What is rent?

The fee every Put Account to store data on the blockchain is called "rent".&#x20;

This time and space based fee is required to keep an account, and its therefore its data, alive on the blockchain since clusters must actively maintain this data.

All Put Accounts (and therefore Programs) are required to maintain a high enough LAMPORT balance to become rent exempt and remain on the Put blockchain.

When an Account no longer has enough LAMPORTS to pay its rent, it will be removed from the network in a process known as Garbage Collection.

Note:&#x20;

Rent is different from transactions fees.&#x20;

Rent is paid (or held in an Account) to keep data stored on the Put blockchain.&#x20;

Where as transaction fees are paid to process instructions on the network.

## Rent rate

The Put rent rate is set on a network wide basis, primarily based on the set LAMPORTS per byte per year.

Currently, the rent rate is a static amount and stored in the the Rent sysvar.

## Rent exempt

Accounts that maintain a minimum LAMPORT balance greater than 2 years worth of rent payments are considered "rent exempt" and will not incur a rent collection.

At the time of writing this, new Accounts and Programs are required to be initialized with enough LAMPORTS to become rent-exempt.&#x20;

The RPC endpoints have the ability to calculate this estimated rent exempt balance and is recommended to be used.

Every time an account's balance is reduced, a check is performed to see if the account is still rent exempt.&#x20;

Transactions that would cause an account's balance to drop below the rent exempt threshold will fail.

## Garbage collection

Accounts that do not maintain their rent exempt status, or have a balance high enough to pay rent, are removed from the network in a process known as garbage collection.&#x20;

This process is done to help reduce the network wide storage of no longer used/maintained data.

You can learn more about garbage collection here in this implemented proposal.

## Learn more about Rent

You can learn more about Put Rent with the following articles and documentation:

* Implemented Proposals - Rent
* Implemented Proposals - Account Storage
