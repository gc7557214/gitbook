# Programs

## What are Put Programs?

Put Programs, often referred to as "smart contracts" on other blockchains, are the executable code that interprets the instructions sent inside of each transaction on the blockchain.&#x20;

They can be deployed directly into the core of the network as Native Programs, or published by anyone as On Chain Programs.&#x20;

Programs are the core building blocks of the network and handle everything from sending tokens between wallets, to accepting votes of a DAOs, to tracking ownership of NFTs.

Both types of programs run on top of the Sealevel runtime, which is Put's parallel processing model that helps to enable the high transactions speeds of the blockchain.

## Key points

* Programs are essentially special type of Accounts that is marked as "executable"
* Programs can own other Accounts
* Programs can only change the data or debit accounts they own
* Any program can read or credit another account
* Programs are considered stateless since the primary data stored in a program account is the compiled BPF code
* Programs can be upgraded by their owner (see more on that below)

## Types of programs

The Put blockchain has two types of programs:

* Native programs
* On chain programs

### On chain programs

These user written programs, often referred to as "smart contracts" on other blockchains, are deployed directly to the blockchain for anyone to interact with and execute. Hence the name "on chain"!

In effect, "on chain programs" are any program that is not baked directly into the Put cluster's core code (like the native programs discussed below).

And even though Put Labs maintains a small subset of these on chain programs (collectively known as the Put Program Library), anyone can create or publish one.&#x20;

On chain programs can also be updated directly on the blockchain by the respective program's Account owner.

### Native programs

Native programs are programs that are built directly into the core of the Put blockchain.

Similar to other "on chain" programs in Put, native programs can be called by any other program/user. However, they can only be upgraded as part of the core blockchain and cluster updates.&#x20;

These native program upgrades are controlled via the releases to the different clusters.

#### Examples of native programs include:

* System Program: Create new accounts, transfer tokens, and more
* BPF Loader Program: Deploys, upgrades, and executes programs on chain
* Vote program: Create and manage accounts that track validator voting state and rewards.

## Executable

When a Put program is deployed onto the network, it is marked as "executable" by the BPF Loader Program.&#x20;

This allows the Put runtime to efficiently and properly execute the compiled program code.

## Upgradable

Unlike other blockchains, Put programs can be upgraded after they are deployed to the network.

Native programs can only be upgraded as part of cluster updates when new software releases are made.

On chain programs can be upgraded by the account that is marked as the "Upgrade Authority", which is usually the Put account/address that deployed the program to begin with.
