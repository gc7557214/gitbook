# Overview

The NFT deployment runs on the PUT chain and defines a generic Non-Fungible Token (NFT) implementation with reference to the ERC-721 standard.

PutDev Program Pubkey: An2DRyUtGBKYioLhHJEQ3nPcGgzzRJQ8vgdhyjdtC14H

NFT currently only deploys this function on the PutDev chain
