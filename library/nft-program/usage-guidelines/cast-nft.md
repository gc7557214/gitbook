# Cast NFT



```
$ ppl-nft mint --token 2ZYMxZojdocSbwcGg4W92H3hVE9qgMBo7XQg9aug6
uKs --token-uri "http://my.asset.uri.path" --owner /root/.config/put/id2.json
 
minting a nft 62Tt6NNUxQbBYx6x5P1s42t7KWQdDamn9ATb6Ls5HYVv
 
Address:  62Tt6NNUxQbBYx6x5P1s42t7KWQdDamn9ATb6Ls5HYVv
token_uri:  http://my.asset.uri.path
 
Signature: 3p2GETXA5R5YmSYTu3vSM82qQLSVfjiw3ZAxc8dCBh4EfLFeTmdhUAF9agfzPFXi72tVctvdpV6P8PPaYW4pMJkq
```

A unique NFT can be cast by using the mint instruction, and the NFT information can be queried after casting by using the nft-info instruction.

```
$ ppl-nft nft-info 62Tt6NNUxQbBYx6x5P1s42t7KWQdDamn9ATb6Ls5HYVv

Mint : 2ZYMxZojdocSbwcGg4W92H3hVE9qgMBo7XQg9aug6uKs
Owner: 97pqCNMq4NUBVa6DSabNiRPQAbtM8ixEDvXYWQCPLxBi
State Initialized
Token id: 1
Token url: http://my.asset.uri.path
Close authority: None
```

You can view the NFT mint address, owner address and status, token id indicates the NFT casting order number, while you can also see the NFT's asset URI address.&#x20;

After the NFT casting, the mint state information of the NFT will also be changed:

```
$ppl-nft mint-info 2ZYMxZojdocSbwcGg4W92H3hVE9qgMBo7XQg9aug6uKs
Already Supply: 1
Total Supply: 100
Mint: 2ZYMxZojdocSbwcGg4W92H3hVE9qgMBo7XQg9aug6uKs
Name: moon
Symbol: put-nft1
Icon uri: http://puttest.com/nft/icon/best-nft-icon
Mint authority: BRxWruwQv3berDJELRM73dtp7f4712aiodZ6KCY4RLkq
Freeze authority: None
Initialized: true
```

You can see that the attribute of Already Supply has changed from 0 to 1.
