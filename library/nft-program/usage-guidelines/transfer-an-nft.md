# Transfer an NFT

The transfer instruction transfers the ownership of one NFT to another address.

```
$ ppl-nft transfer --from /root/.config/put/id2.json –nft
62Tt6NNUxQbBYx6x5P1s42t7KWQdDamn9ATb6Ls5HYVv --recipient 
DySd8hi7Z4jrV3ixAk5dT1kPJQzKMiUyyCF38DiJypLY
	
program_id An2DRyUtGBKYioLhHJEQ3nPcGgzzRJQ8vgdhyjdtC14H
 recipient:  DySd8hi7Z4jrV3ixAk5dT1kPJQzKMiUyyCF38DiJypLY
nft:  62Tt6NNUxQbBYx6x5P1s42t7KWQdDamn9ATb6Ls5HYVv

Signature: 5yVM4LKkUfF7ybRoM97kP7Sf3bqCTrjg8Shud2zfKDe8QSogPwdXmS9WYRStKigvvUEKo6iUKHHRagcrsmRNiPFC
```

The transfer parameter must be the private key of the NFT.&#x20;

After the transfer transaction is completed on the chain, the owner of the NFT will change and the sender will not have the right to operate the NFT.

```
$ ppl-nft nft-info 62Tt6NNUxQbBYx6x5P1s42t7KWQdDamn9ATb6Ls5HYVv

Mint : 2ZYMxZojdocSbwcGg4W92H3hVE9qgMBo7XQg9aug6uKs
Owner: DySd8hi7Z4jrV3ixAk5dT1kPJQzKMiUyyCF38DiJypLY
State: Initialized
Token id: 1
Token url: http://my.asset.uri.path
Close authority: None
```

You can see that Owner has changed from the original 97pqCNMq4NUBVa6DSabNiRPQAbtM8ixEDvXYWQCPLxBi to DySd8hi7Z4jrV3ixAk5dT1kPJQzKMiUyyCF38DiJypLY.
