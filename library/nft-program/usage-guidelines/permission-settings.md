# Permission settings

There are three kinds of permissions in the PPL-NFT program, namely MINT\_AUTHORITY, FREEZE\_AUTHORITY, CLOSE\_AUTHORITY. FREEZE\_AUTHORITY and CLOSE\_AUTHORITY have been seen in the above example-change account status, which are mainly used to verify whether the freeze and burn instruction permissions are valid.&#x20;

MINT\_AUTHORITY is the operation permission of mint account, which can be used to perform mint\_to operation, and update FREEZE\_AUTHORITY and other mint account attribute operations.

&#x20;

Eg. Set FREEZE\_AUTHORITY. As NFT- 62Tt6NNUxQbBYx6x5P1s42t7KWQdDamn9ATb6Ls5HYVv account has been closed, we mint a new NFT account to complete the following example, the new NFT address- 8ZXhM6qBQe8GZJNs9dxvwM1RrmAnCr2y66P7azodR2gU. Use the authorize instruction to change the account permissions.

```
$ ppl-nft authorize --address 2ZYMxZojdocSbwcGg4W92H3hVE9qgMBo7
XQg9aug6uKs --type freeze --auth-keypair /root/.config/put/id2.
json 
authorize a FreezeAccount authority to  2ZYMxZojdocSbwcGg4W92H3hVE9qgMBo7XQg9aug6uKs
address 2ZYMxZojdocSbwcGg4W92H3hVE9qgMBo7XQg9aug6uKs auth_type authority change to 97pqCNMq4NUBVa6DSabNiRPQAbtM8ixEDvXYWQCPLxBi
Signature: 5wVUYysw645YqQMRLjnrBvoqEDQtz3cgih4xKEfqPNXVoHHZyZZnyJ12jELP5sP8yjx6jHR2WS7bj5LUUoqzpKaX
```

Authorize instruction parameter where address is mint account address, type specifies which type of permission is updated.&#x20;

The optional values are freeze, close, mint corresponding to FREEZE\_AUTHORITY, CLOSE\_AUTHORITY, MINT\_AUTHORITY permission. auth-keypair is the new permission signer file, if it is not set, the corresponding permission will become None.

To query mint account information after setting freeze permissions:

```
$ ppl-nft mint-info 2ZYMxZojdocSbwcGg4W92H3hVE9qgMBo7XQg9aug6uKs

Already Supply: 2
Total Supply: 100
Mint: 2ZYMxZojdocSbwcGg4W92H3hVE9qgMBo7XQg9aug6uKs
Name: moon
Symbol: put-nft1
Icon uri: http://puttest.com/nft/icon/best-nft-icon
Mint authority: BRxWruwQv3berDJELRM73dtp7f4712aiodZ6KCY4RLkq
Freeze authority: 97pqCNMq4NUBVa6DSabNiRPQAbtM8ixEDvXYWQCPLxBi
Initialized: true
```

After setting FREEZE\_AUTHORITY only through freeze permission to freeze NFT, using mint permission will report the error.

```
$ ppl-nft freeze --address 8ZXhM6qBQe8GZJNs9dxvwM1RrmAnCr2y66P7a
zodR2gU
...
[2022-07-13T07:51:06.912882097Z DEBUG put_client::nonblocking::
rpc_client]   2: Program log: Instruction: Freeze
[2022-07-13T07:51:06.912896667Z DEBUG put_client::nonblocking::
rpc_client]   3: Program log: Error: authority mismatched
...
Error: RPC response error -32002: Transaction simulation failed: Error processing Instruction 0: custom program error: 0x18 [5 log messages]
```

The permission does not match. The freeze operation can be completed with the freeze authority permission.

```
$ ppl-nft freeze --address 8ZXhM6qBQe8GZJNs9dxvwM1RrmAnCr2y
66P7azodR2gU --auth-keypair /root/.config/put/id2.json
freezing a nft 8ZXhM6qBQe8GZJNs9dxvwM1RrmAnCr2y66P7azodR2gU
freeze nft:  8ZXhM6qBQe8GZJNs9dxvwM1RrmAnCr2y66P7azodR2gU
Signature: 45Tjtk1fhqetRNDvLXR2qxQhcWG95EdAtSKLBiWQULYFWVEkwv3bucEMfA7XseAuJxto3RkHb2hjuLXUKcHK2Rab
```

Check the NFT status.

```
$ ppl-nft nft-info 8ZXhM6qBQe8GZJNs9dxvwM1RrmAnCr2y66P7azodR2gU

Mint : 2ZYMxZojdocSbwcGg4W92H3hVE9qgMBo7XQg9aug6uKs
Owner: BRxWruwQv3berDJELRM73dtp7f4712aiodZ6KCY4RLkq
State： Frozen
Token id: 2
Token url: www.google.com
Close authority: None
```

You can see that the freeze operation has taken effect.

Setting CLOSE\_AUTHORITY and MINT\_AUTHORITY is the same as the freeze method, please refer to the ppl-nft help information for details.
