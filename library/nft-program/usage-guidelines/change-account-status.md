# Change account status

In order to cope with the theft of the user's private key, we provide a freeze function for NFT accounts.&#x20;

Mint accounts can initiate a freeze operation on the NFT account, but the frozen NFT account can not perform transfer, close and other operations.&#x20;

In addition, users can also use burn instruction to destroy their own NFT, but the destruction is irreversible!

Freeze the NFT account.

```
$ ppl-nft freeze --address 62Tt6NNUxQbBYx6x5P1s42t7
KWQdDamn9ATb6Ls5HYVv

freezing a nft 62Tt6NNUxQbBYx6x5P1s42t7KWQdDamn9ATb6Ls5HYVv
freeze nft: 62Tt6NNUxQbBYx6x5P1s42t7KWQdDamn9ATb6Ls5HYVv
Signature: 4QMDpudrM916x5rt28XPvVwtGhANtcNarcVJ3WvpjzSHVnkezCGuRX8Q8remYcTaFyNP6iJBUDS3ZVTkCgrZkrRM
```

Afterwards, check the NFT account information again:

```
./ppl-nft nft-info 62Tt6NNUxQbBYx6x5P1s42t7KWQdDamn9ATb6Ls5HYVv
Mint : 2ZYMxZojdocSbwcGg4W92H3hVE9qgMBo7XQg9aug6uKs
Owner: DySd8hi7Z4jrV3ixAk5dT1kPJQzKMiUyyCF38DiJypLY
State: Frozen
Token id: 1
Token url: http://my.asset.uri.path
Close authority: None
```

The status of the NFT account has changed to Frozen, then the account transfer operation will not be allowed:

```
$ ppl-nft transfer --from ./id.json --nft 62Tt6NNUxQbBYx6x5P1s42
t7KWQdDamn9ATb6Ls5HYVv --recipient 97pqCNMq4NUBVa6DSabNi
RPQAbtM8ixEDvXYWQCPLxBi
transfer the nft 62Tt6NNUxQbBYx6x5P1s42t7KWQdDamn9ATb6Ls5HYVv from DySd8hi7Z4jrV3ixAk5dT1kPJQzKMiUyyCF38DiJypLY to 97pqCNMq4NUBVa6DSabNiRPQAbtM8ixEDvXYWQCPLxBi
program_id An2DRyUtGBKYioLhHJEQ3nPcGgzzRJQ8vgdhyjdtC14H
[2022-07-13T05:03:05.615165632Z DEBUG put_client::nonblocking
::rpc_client]   1: Program An2DRyUtGBKYioLhHJEQ3nPcGgzzRJQ8vgdhyjdtC14H invoke [1]
[2022-07-13T05:03:05.615180822Z DEBUG put_client::nonblocking::
rpc_client]   2: Program log: Instruction: Transfer
[2022-07-13T05:03:05.615194402Z DEBUG put_client::nonblocking::
rpc_client]   3: Program log: Error: Account is frozen
[2022-07-13T05:03:05.615208652Z DEBUG put_client::nonblocking::
rpc_client]   4: Program An2DRyUtGBKYioLhHJEQ3nPcGgzzRJQ8vgdhyjdtC14H consumed 2916 of 1400000 compute units
[2022-07-13T05:03:05.615223822Z DEBUG put_client::nonblocking::rpc_client]   5: Program An2DRyUtGBKYioLhHJEQ3nPcGgzzRJQ8vgdhyjdtC14H
 failed: custom program error: 0x11
[2022-07-13T05:03:05.615238842Z DEBUG put_client::nonblocking::
rpc_client] Error: RPC response error -32002: Transaction simulation failed: Error processing Instruction 0: custom program error: 0x11 [5 log messages]
```

The final transaction execution is failed and an error is reported.&#x20;

The third error message indicates that Account is Frozen, corresponding to error 0x11, so it is confirmed that the freeze operation has successfully frozen the NFT account. In response to the freeze operation, you can use the thaw instruction to thaw the program.



```
$ ppl-nft thaw --address 62Tt6NNUxQbBYx6x5P1s42t7KWQdDamn
9ATb6Ls5HYVv
thawing a nft 62Tt6NNUxQbBYx6x5P1s42t7KWQdDamn9ATb6Ls5HYVv
thaw nft:  62Tt6NNUxQbBYx6x5P1s42t7KWQdDamn9ATb6Ls5HYVv
Signature: 5Y2AuwhznFW5HCTz6oc4ReiSFjNC9pbyco5jSgaUjS6EXWSH7ogT676C1ddX4r9qtKpR6kHBnf6WUzPRHP9Nj1tL
```

After thawing, the account status will be restored to normal and the transfer operation will be successfully supported:

```
$ ppl-nft transfer --from ./id.json --nft 62Tt6NNUxQbBYx6x5P1
s42t7KWQdDamn9ATb6Ls5HYVv 
--recipient 97pqCNMq4NUBVa6DSabNiRPQAbtM8ixEDvXYWQCPLxBi
transfer the nft 62Tt6NNUxQbBYx6x5P1s42t7KWQdDamn9ATb6Ls5HYVv from DySd8hi7Z4jrV3ixAk5dT1kPJQzKMiUyyCF38DiJypLY to 97pqCNMq4NUBVa6DSabNiRPQAbtM8ixEDvXYWQCPLxBi
program_id An2DRyUtGBKYioLhHJEQ3nPcGgzzRJQ8vgdhyjdtC14H

sender:  DySd8hi7Z4jrV3ixAk5dT1kPJQzKMiUyyCF38DiJypLY
recipient:  97pqCNMq4NUBVa6DSabNiRPQAbtM8ixEDvXYWQCPLxBi
nft:  62Tt6NNUxQbBYx6x5P1s42t7KWQdDamn9ATb6Ls5HYVv

Signature: GDGHMxS7KLWn2ikqKJHBBZaUXRye6duKFDTg52v7cbpBbdPdVMUuSk2hjdHZBoaz6oaL3aCRFeJvhShmFJNY3se
```

The Burn operation is executed in the NFT account.&#x20;

You need to be cautious when using this instruction, because this irreversible operation will destroy the NFT you own:

```
$ ppl-nft burn --address 62Tt6NNUxQbBYx6x5P1s42t7KWQdDamn
9ATb6Ls5HYVv --auth-keypair /root/.config/put/id2.json
burning a nft 62Tt6NNUxQbBYx6x5P1s42t7KWQdDamn9ATb6Ls5HYVv
burn nft:  62Tt6NNUxQbBYx6x5P1s42t7KWQdDamn9ATb6Ls5HYVv
Signature: 57NRhFzSi55aCRSV14YYR8iHAdNwYezu2FidHsFnGNqXqT4erRfEmapbQ3G4SWTLnjUzqBvE7WtVChqkizpj7MRS
```

Re-check NFT information after account destruction.

```
$ ppl-nft nft-info 62Tt6NNUxQbBYx6x5P1s42t7KWQdDamn9ATb6Ls5HYVv
Error: ClientError { request: None, kind: RpcError(ForUser("AccountNotFound: pubkey=62Tt6NNUxQbBYx6x5P1s42t7KWQdDamn9ATb6Ls5HYVv")) }
```

You can see that the lookup failed and reported the error: AccountNotFound, which indicates that the NFT account has been destroyed.

&#x20;

Note:&#x20;

The freeze instruction requires the freeze authority permission, and the burn instruction requires the close authority permission, which can be viewed by the mint-info instruction and nft-info instruction respectively.&#x20;

If the corresponding permission is set to None, it will default to the account owner address.
