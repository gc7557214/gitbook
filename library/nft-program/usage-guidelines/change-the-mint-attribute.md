# Change the Mint attribute

The icon\_uri of the Mint account and the token\_uri of the NFT account can be updated with the update instruction.

```
./ppl-nft update 2ZYMxZojdocSbwcGg4W92H3hVE9qgMBo7XQg9aug6uKs --type icon --value  www.update.inco.com
61YTwr4s5yKNpFk5Zxfeicm4we5u1ZhZ17VCE2GADU2VCmm4a6AZpq8DdHNx8RquR6LEjPPwov2fMWySBmABSimf
```

Check mint-info again:

```
$ ppl-nft mint-info 2ZYMxZojdocSbwcGg4W92H3hVE9qgMBo7XQg9aug6uKs
Already Supply: 12
Total Supply: 100
Mint: 2ZYMxZojdocSbwcGg4W92H3hVE9qgMBo7XQg9aug6uKs
Name: moon
Symbol: put-nft1
Icon uri: www.update.inco.com
Mint authority: BRxWruwQv3berDJELRM73dtp7f4712aiodZ6KCY4RLkq
Freeze authority: 97pqCNMq4NUBVa6DSabNiRPQAbtM8ixEDvXYWQCPLxBi
Initialized: true
```

Update NFT icon\_uri is similar to update mint account token\_uri. Just set the value of type to asset, refer to help for details.
