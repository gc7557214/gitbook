# Create a new NFT-Mint



```
$ ppl-nft create-token --total-supply 100 --nft-name moon -nft-ymbol put-nft1 --icon-uri "http://puttest.com/nft/icon/bestnft-icon "

Creating token 2ZYMxZojdocSbwcGg4W92H3hVE9qgMBo7XQg9aug6uKs

program_id An2DRyUtGBKYioLhHJEQ3nPcGgzzRJQ8vgdhyjdtC14H

Address: 2ZYMxZojdocSbwcGg4W92H3hVE9qgMBo7XQg9aug6uKs

total_supply: 100

icon_uri: http://puttest.com/nft/icon/best-nft-icon

Signature: 2sKCphf2ZykBRcVPnZEDCgxcw88MgNv5QKNH8wBH3KeYLvDTWWuTbSqrLbP2BMyBq5kEBJRpps94be6bvaYURiax
```



The create-token instruction creates a unique Nft casting address which is called the mint account - 2ZYMxZojdocSbwcGg4W92H3hVE9qgMBo7XQg9aug6uKs.&#x20;

A unique NFT can then be cast based on this address. Unlike the ppl-token, this instruction must specify the supply quantity when the foundry is created .&#x20;

In the following example, a mint with a total supply of 100 is created, and its name, symbol and icon are set.

After that we can query the mint account through the mint-info instruction.

```
$ ppl-nft mint-info 2ZYMxZojdocSbwcGg4W92H3hVE9qgMBo7XQg9aug6uKs

Already Supply: 0
Total Supply: 100
Mint: 2ZYMxZojdocSbwcGg4W92H3hVE9qgMBo7XQg9aug6uKs
Name: moon
Symbol: put-nft1
Icon uri: http://puttest.com/nft/icon/best-nft-icon
Mint authority: BRxWruwQv3berDJELRM73dtp7f4712aiodZ6KCY4RLkq
Freeze authority: None
Initialized: true
```



This instruction allows you to view the casting information (current casting, total supply) for the Nft mint account, as well as some of the mint account's own properties.&#x20;

The Mint authority specifies the administrative account of the mint account (BRxWruwQv3berDJELRM73dtp7f4712aiodZ6KCY4RLkq).&#x20;

Only with the private key of the administrative account can you perform a series of operations on the mint account, such as mint.
