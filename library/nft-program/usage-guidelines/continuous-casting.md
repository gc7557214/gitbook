# Continuous casting

Casting one nft at a time may not be very efficient for mint owners, for this reason, we provide a multi-casting interface, you can cast more than one NFTs at once:

```
$ ppl-nft multi-mint --address 2ZYMxZojdocSbwcGg4W92H3hVE9qgMBo
7XQg9aug6uKs --nft-file . /multi-mint-file 
minting a nft 2rdRnwFgmNUn4vYX4xQd2iYbXmVocmq5dTHkf5CqWhk5
minted nft 2rdRnwFgmNUn4vYX4xQd2iYbXmVocmq5dTHkf5CqWhk5
minting a nft AN2xJXJN4WNTyT1CuVAXf3JL1fiKezHLdNR3uowSx4H
minted nft AN2xJXJN4WNTyT1CuVAXf3JL1fiKezHLdNR3uowSx4H
minting a nft FNVP9JPgpbNBMQhekGVUk2Y3h2JYynNL2rf52SgVdNsp
minted nft FNVP9JPgpbNBMQhekGVUk2Y3h2JYynNL2rf52SgVdNsp
minting a nft 5vzu4qga1gsSzCWh3MAaCC3nTocsMmCJ1Dy3CZct1KZy
minted nft 5vzu4qga1gsSzCWh3MAaCC3nTocsMmCJ1Dy3CZct1KZy
minting a nft 5BjLqrbQNG3QtmhTKsvx4yAX1Ree3YnQMFqgvp2tC7BU
minted nft 5BjLqrbQNG3QtmhTKsvx4yAX1Ree3YnQMFqgvp2tC7BU
```

This instruction reads the contents of the multi-mint-file file by line, casting as many Nfts as lines in the file (plus blank lines). The contents of the file are set to the token-uri of the NFT.
