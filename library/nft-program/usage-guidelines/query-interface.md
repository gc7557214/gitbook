# Query Interface

We have been exposed to two query interfaces: mint-info and nft-info in the previous examples.&#x20;

We can also query NFT information by token-id, and query the cast NFT of the mint address.

Query NFT information by token-id.

```
$ ppl-nft nft-index-info --mint 2ZYMxZojdocSbwcGg4W92H3hVE
9qgMBo7XQg9aug6uKs --token-id 2

Mint 2ZYMxZojdocSbwcGg4W92H3hVE9qgMBo7XQg9aug6uKs at index 2's Nft is: 8ZXhM6qBQe8GZJNs9dxvwM1RrmAnCr2y66P7azodR2gU 
The nft 8ZXhM6qBQe8GZJNs9dxvwM1RrmAnCr2y66P7azodR2gU owner is: BRxWruwQv3berDJELRM73dtp7f4712aiodZ6KCY4RLkq
Mint : 2ZYMxZojdocSbwcGg4W92H3hVE9qgMBo7XQg9aug6uKs
Owner: BRxWruwQv3berDJELRM73dtp7f4712aiodZ6KCY4RLkq
State: frozen
Token id: 2
Token url: www.google.com
Close authority: None
```

Query the cast NFT of the mint address:

```
$ ppl-nft mint-nfts 2ZYMxZojdocSbwcGg4W92H3hVE9qgMBo7XQg9aug6uKs
-------------------- nft list --------------------
62Tt6NNUxQbBYx6x5P1s42t7KWQdDamn9ATb6Ls5HYVv
8ZXhM6qBQe8GZJNs9dxvwM1RrmAnCr2y66P7azodR2gU
```
