# Create a new NFT-Mint

#### InitializeMint can create NFT by initializing a new Mint with the instruction. &#x20;

#### Mint is used to create or "cast" new NFT that are stored in the account, and each has a unique token id, which is incremented by the total supply from The token id is incremented from 1 according to the total supply.&#x20;

#### And Mint is also stored in the account, we will call the account stored mint as mint account, and the account stored NFT as NFT account.

Once Mint is initialized, NFT can be created using this instruction mint\_authority. mintTo can create NFT using the mint\_authority of the mint account at any time as long as the Mint contains valid mint\_authority and as long as the number of minted NFT has not reached the maximum supply.

NFT has been cast out of the NFT can be destroyed by Burn to reduce the supply of Mint.&#x20;

After the NFT destruction, mint account of the total supply and supply information remains unchanged, but NFT account has been unable to query.&#x20;

In the casting of new NFT, token id is still on the latest supply increase.
