# Freeze NFT accounts

Mint accounts may also contain a freeze\_authority (freeze Account) that can be used to issue instructions that render the account unusable.&#x20;

Any write instruction containing a token that freezes the account will fail until the account is unfrozen using the mint account Thaw instruction.&#x20;

The SetAuthority instruction can be used to change the mint's freeze\_authority.&#x20;

If the Mint account freeze\_authority is set to None, the mint\_authority account will be used by default to exercise freeze authority.&#x20;

If freeze\_authority is not None, using mint\_authority for freeze operations will not take effect.

In addition, mint\_authority can modify the freeze\_authority of the mint account.
