# Update

Mint account and NFT account can be updated, update the account attributes by using the update instruction interface.&#x20;

Now can update the attributes include mint account icon\_uri and NFT account token\_uri, corresponding to the type of icon and asset.&#x20;

Using this method is also convenient for extending the update attribute.
