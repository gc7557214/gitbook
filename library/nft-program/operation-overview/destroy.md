# Destroy

The Burn instruction removes the NFT account directly from the chain, effectively removing the NFT from circulation permanently.

There is no other way to reduce the provisioning on the chain.&#x20;

This is similar to transferring to an account with an unknown private key or destroying an account with a private key.&#x20;

But the act of destruction using the Burn instruction is more explicit and can be confirmed directly on the chain by either party.
