# Transfer NFT

NFT can use this Transfer instruction to transfer between accounts.&#x20;

When the source and target accounts are different, the owner of the source account must appear in the instruction as the signer.

It should be noted that when the source account and the target account are the same, the Transfer will still succeed.&#x20;

When transferring an NFT, a new NFT account is not created, but the address of the NFT owner account is changed, as the NFT is unique and no replication is created in the contract.
