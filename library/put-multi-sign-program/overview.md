# Overview

Running on the PUT chain, Multi-sig contract deployment defines and realizes a common multi-sig service, which provides online multi-sig service for contracts.&#x20;

Contracts that need to access multi-sig can call the multi-sig program through CPI (Cross Program Invocation), while the private keys required for multi-sig are stored separately to avoid severe security problems caused by private key leakage.

Multi-sig contracts mainly provide interfaces for creating multi-sig accounts, creating proposal accounts, as well as proposal voting and proposal verification.

CPI calls the interface for multi-signature contracts, which we call the multi-signature interface.

The normal flowchart for a multi-signature interface to access a multi-signature contract is roughly as follows.

<figure><img src="../../.gitbook/assets/multi-sign (1).jpg" alt=""><figcaption></figcaption></figure>



1. Create a multi-signature account
2. Get the address for multi-signature accounts
3. Contract developers bind the multisig interface to a multisig account (decide which multisig account to use when developing the multisig interface)
4. Account Alice initiates the first multi-signature interface call
5. A transaction proposal is created on the first call and an account is created in the multi-signer by means of a Cross Program Invocation. The content of the transaction proposal is defined by the contract developer, and we recommend that the proposal outlines the user's actions (the proposal content for an A -> B transfer transaction can be written as "Alice (the proposal initiator) init a A transfer 1 PUT to B proposal.")
6. Alice gets the address of the proposal account and gets the successful response of execution
7. Alice, the initiator of the transaction, informs other signatories of the proposal address (proposal\_address) offline, and other signatories can vote after viewing the proposal
8. After the proposal is approved, Alice initiates a second multi-signature interface call
9. The multi-signature interface method verifies that the proposal has passed by calling the multi-signature contract through Cross Program Invocation
10. If the proposal is approved, the multi-signature interface starts executing the real contract logic
11. Return a successful response

You can see that multi-signature contracts actually increase the complexity of the multi-signature interface, mainly because methods that were called once now need to be called twice by the initiator.&#x20;

However, multi-signature makes the contract interface more secure. From a security point of view, it is worth increasing code complexity.
