# Create a proposal account

Proposal accounts are typically created in a contract by calling a multi-signature contract across contracts.&#x20;

The actual business logic of the multi-sig interface is not executed when the proposal account is created. After the proposal account is created, the proposal has to be signed by voting using the signature account (singers) defined by the multi-signature account.&#x20;

The real multi-signature interface business logic is not executed until the signature account threshold is reached. The user who creates the proposal become, the proposal initiator.&#x20;

The content of the proposal is defined by the contract developer.
