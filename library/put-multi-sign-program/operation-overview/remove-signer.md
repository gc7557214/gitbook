# Remove-signer

Remove-singer is a multi-signature interface in a multi-signature contract, used to remove existing singers from multi-signature accounts.
