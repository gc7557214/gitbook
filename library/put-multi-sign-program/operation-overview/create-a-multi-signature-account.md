# Create a multi-signature account

A new multi-sig account is created by using the create-multi-sig instruction.&#x20;

The multi-sig account is used in the multi-sig interface to verify the signatures required by the multi-sig account, and the business logic of the multi-sig interface can be executed only after the verification is passed.
