# Verify

The Verify operation is typically invoked in the contract by calling the multi-signature contract in a Cross Program Invocation.&#x20;

The Vefify interface is only valid if called by the proposal initiator.&#x20;

After verifying the proposal signature successfully, the business logic of the multi-signature interface should be executed.
