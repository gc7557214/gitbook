# Vote

Holders of valid signature addresses in multi-signature accounts can vote on transaction proposals.
