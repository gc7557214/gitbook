# Create a proposal account





```
$ ppl-sig create-proposal -m FF7s3KDy5kSP1wzeZCaj4Ns5boP5cb94k
3APvZM2VvHp --summary "a transfer b 10 put"
creating proposal account 6NuA13zJvhA7GU86LzzrhwTVYCbkKqzvp
wHTzkejGDPg

Signature: 2nP3mke5JdzUB1t3fi4Uk5nfzpLLCgZbPQj7W9jnh14CaYkT4PNdC77KkD6WpoYnPB9gVnM1XLopPzp7UVfdfaui
```

Using the create-proposal instruction will create a proposal account (note: proposal accounts are generally created in custom contracts, so using the instruction line will not work, this is just to show the general process of multi-signature).&#x20;

To create a proposal, you need to specify a multi-signature account, which will be used for subsequent multi-sig signatures, and define the proposal summary.&#x20;

The proposal creation step corresponds to step 4 of the multi-signature flowchart, where Alice calls the multi-signature interface for the first time.

After creating a proposal, you can use the account instruction to query its details.

```
$ ppl-sig proposal-info 6NuA13zJvhA7GU86LzzrhwTVYCbkKqzvpwHT
zkejGDPg

Initiator:3AGrFExFviu6Gg6mUgdf4BUbi178Cg43ysbmYc5gbCYv 
Proposal summary: "a transfer b 10 put" 
----------------Voted account----------------
3AGrFExFviu6Gg6mUgdf4BUbi178Cg43ysbmYc5gbCYv
```

Proposal account information contains the initiator of the proposal, a list of voted accounts, and a summary of the transaction proposal, which serves as an important reference for other signatories to vote or not.

When a proposal is created, it indicates that the initiator of the proposal agrees to the proposal and the proposal initiator has signed it, so you can see that the signature originator has voted.&#x20;

Only the signature address specified in a multi-signature account can initiate a proposal.
