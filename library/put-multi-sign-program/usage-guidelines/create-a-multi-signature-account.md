# Create a multi-signature account

```
$ ppl-sig create-sig-account --accounts . /signers --threshold 49
creating multi-sig account FF7s3KDy5kSP1wzeZCaj4Ns5boP5cb94k3APv
ZM2VvHp
Signature:
37VQjKB7Em5WDFqh5NtxzFrUjmgUEQZBwabnzBFSPLvzebBXtUEBMhVch7eLhmk7oQpT8eaBN4RsiCGNJyx2mwoq
```

The create-sig-account instruction is used to create a multi-sig account FF7s3KDy5kSP1wze-ZCaj4Ns5boP5cb94k3APvZM2VvHp.&#x20;

This instruction requires specifying the accounts parameter, passing in a signature list file, separated by newlines.&#x20;

You also need to specify the multi-signature threshold (from 1 to 100).

After this, we can query the multi-info instruction for information on multi-signature accounts at

```
$ ppl-sig multi-info FF7s3KDy5kSP1wzeZCaj4Ns5boP5cb94k3APvZM2Vv
Hp
Pass Threshold: 49% 
----------------all signers----------------
DySd8hi7Z4jrV3ixAk5dT1kPJQzKMiUyyCF38DiJypLY
4vxfArUy35WeLMwBCe8FEUjWq7gXPpRyF8A85EL44CB8
3AGrFExFviu6Gg6mUgdf4BUbi178Cg43ysbmYc5gbCYv
```

This instruction queries the valid signature address of a multi-signature account and the threshold value for signature pass-through.
