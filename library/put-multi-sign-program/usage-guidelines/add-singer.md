# Add-singer



```
$ ppl-sig add-signer --msa FF7s3KDy5kSP1wzeZCaj4Ns5boP5cb94k3
APvZM2VvHp -s 9UxRmcoVtr3KMxK3j5WDo9tXEsdYGhwK8Tnqiaw97UBB

add new signer in multi sig account FF7s3KDy5kSP1wzeZCaj4Ns5
boP5cb94k3APvZM2VvHp 
Creating proposal account 2DT5HB9DsuK3DhejcmqGvYNAX6tunKa1ccC
JH3b28wND

Signature: 52vrvUoemrD6uvJEXPyfq4caD4oC4DXG61QKuseNFwq7p5Hw2surep8ru7Z5PM3FQKK5VQFMLAhK29UsAe1WgjoC
```

Use the add-signer instruction to add a new signer account to a multi-signer account. the add-signer interface is a multi-signature interface, so the proposal account is created when the interface is invoked.&#x20;

The new signature is only added when the proposal is called again after the proposal verification has passed.

Query add-singer proposal account information.

```
$ ppl-sig proposal-info 2DT5HB9DsuK3DhejcmqGvYNAX6tunKa1c
cCJH3b28wND

Initiator:3AGrFExFviu6Gg6mUgdf4BUbi178Cg43ysbmYc5gbCYv 
Proposal summary: "3AGrFExFviu6Gg6mUgdf4BUbi178Cg43ysbmYc
5gbCYv initiating add a new signer 9UxRmcoVtr3KMxK3j5WDo9tXE
sdYGhwK8Tnqiaw97UBB to multi sig account FF7s3KDy5kSP1wz
eZCaj4Ns5boP5cb94k3APvZM2VvHp" 
----------------Voted account----------------
3AGrFExFviu6Gg6mUgdf4BUbi178Cg43ysbmYc5gbCYv
```

You can see in the proposal summary information that indicates which singer initiated the proposal to add singer to which signature account.

&#x20;

The voting operation is then performed using other signature accounts at

```
$ ppl-sig vote --proposal 2DT5HB9DsuK3DhejcmqGvYNAX6tunKa1cc
CJH3b28wND --signer . /id2.json 

voting for a proposal 2DT5HB9DsuK3DhejcmqGvYNAX6tunKa1ccCJ
H3b28wND

Signature: 3ZApa9x22XFmW2vvo8TwDxLZgB2JcHwAzp13DAR4QyakLgb7KkdcXATRQe1jfFjqvKSBT3Yi4qKrfmePNG8ah5sw
```

To view the proposal information again after voting.

```
$ ppl-sig proposal-info 2DT5HB9DsuK3DhejcmqGvYNAX6tunKa1c
cCJH3b28wND

Initiator:3AGrFExFviu6Gg6mUgdf4BUbi178Cg43ysbmYc5gbCYv 
Proposal summary: "3AGrFExFviu6Gg6mUgdf4BUbi178Cg43ysbmYc
5gbCYv initiating add a new signer 9UxRmcoVtr3KMxK3j5WDo9tXE
sdYGhwK8Tnqiaw97UBB to multi sig account FF7s3KDy5kSP1wz
eZCaj4Ns5boP5cb94k3APvZM2VvHp" 
----------------Voted account----------------
3AGrFExFviu6Gg6mUgdf4BUbi178Cg43ysbmYc5gbCYv
4vxfArUy35WeLMwBCe8FEUjWq7gXPpRyF8A85EL44CB8
```

You can see that the list of voted signatures has changed.

The proposal initiator calls the multi-signer interface add-signer again.

```
$ ppl-sig add-signer --msa FF7s3KDy5kSP1wzeZCaj4Ns5boP5cb94k3
APvZM2VvHp -s 9UxRmcoVtr3KMxK3j5WDo9tXEsdYGhwK8Tnqiaw97UBB

add new signer in multi sig account FF7s3KDy5kSP1wzeZCaj4Ns5
boP5cb94k3APvZM2VvHp 
Creating proposal account 2DT5HB9DsuK3DhejcmqGvYNAX6tunKa1ccC
JH3b28wND

Signature: 5WoF6ohPC9FjDjegztSzv7wT8cwb1hjco1Xey6L1iBt5ZocnCVtUbttBBPDCKpEC7AFghPPPizeaDW8qEMZnZ7uW
```

This operation corresponds to step 9 of the multi-signature process.&#x20;

At this point, the business logic of the multi-signature interface has been executed and the multi-signature account information is queried and you can see that the new singer address has been added.

```
$ ppl-sig multi-info FF7s3KDy5kSP1wzeZCaj4Ns5boP5cb94k3APvZM2Vv
Hp
Pass Threshold: 49% 
----------------all signers----------------
DySd8hi7Z4jrV3ixAk5dT1kPJQzKMiUyyCF38DiJypLY
4vxfArUy35WeLMwBCe8FEUjWq7gXPpRyF8A85EL44CB8
3AGrFExFviu6Gg6mUgdf4BUbi178Cg43ysbmYc5gbCYv
9UxRmcoVtr3KMxK3j5WDo9tXEsdYGhwK8Tnqiaw97UBB
```

Add New Signature Interface is a well-defined multi-signature interface that demonstrates the call execution process for multi-signature contracts and can be referred to when developing custom contracts.
