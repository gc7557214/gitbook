# Remove-signer



```
$ ppl-sig remove-singer --msa FF7s3KDy5kSP1wzeZCaj4Ns5bo
P5cb94k3APvZM2VvHp 9UxRmcoVtr3KMxK3j5WDo9tXEsdYGhwK8Tnq
iaw97UBB

removing signer 9UxRmcoVtr3KMxK3j5WDo9tXEsdYGhwK8Tnqiaw97UBB from multi sig account FF7s3KDy5kSP1wzeZCaj4Ns5boP5cb94k3
APvZM2VvHp 
Creating proposal account DZNX5p76jbSTkhUxkFSp4zMdSCq2UGajLKKtDUFBsL2T

Signature: 2jMwMsZHg3gpUFmYDGPhr1f9BBVivKFRUprGnrW2ZhBVkNVxC838rwyCFjaiCxUwUtaKAs7dJwC9NwnXVSmvRji2
```

Use the remove-singer instruction to remove a valid signature address from an existing multi-signature account.&#x20;

The remove-singer interface is also a multi-signature interface, and the process is similar to the add-singer interface, so it is not repeated.

&#x20;

After executing the corresponding vote verification and revoke operations, check the account information of the multi-signature account again.

```
$ ppl-sig multi-info FF7s3KDy5kSP1wzeZCaj4Ns5boP5cb94k3AP
vZM2VvHp

Pass Threshold: 49% 
----------------all signers----------------

DySd8hi7Z4jrV3ixAk5dT1kPJQzKMiUyyCF38DiJypLY
4vxfArUy35WeLMwBCe8FEUjWq7gXPpRyF8A85EL44CB8
3AGrFExFviu6Gg6mUgdf4BUbi178Cg43ysbmYc5gbCYv
```

You can see that the address 9UxRmcoVtr3KMxK3j5WDo9tXEsdYGhwK8Tnqiaw97UBB has been removed.
