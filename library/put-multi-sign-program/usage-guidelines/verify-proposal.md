# Verify Proposal



```
$ ppl-sig verify --proposal 6NuA13zJvhA7GU86LzzrhwTVYCbkKqzvpwHT
zkejGDPg
verifying proposal 6NuA13zJvhA7GU86LzzrhwTVYCbkKqzvpwHTzkejG
DPg passed 

Signature: 5KeUaAf8hvjeiRSrVUyRYXKhYTy2TfTft5s3y6yi5TbTUgguFf6WdEQ3crHkDVfkmuToVB95nA6f62osWD2krnht
```

The proposal can be verified using the verify instruction after the proposal is approved, and the proposal account will be deleted after the verification is passed.&#x20;

This step corresponds to step 9 of the above multi-signature process and occurs in the second Alice call.&#x20;

The proposal validation interface must be called by a custom contract across contracts to serve its intended purpose, never call the verify instruction offline.&#x20;

For multi-signature interfaces, the contract developer, should execute the business logic of the interface after a successful verify call.

To prevent proposal account stacking, proposal accounts will be deleted after verify successfully.

To view information on the proposal account after successful Verify.

```
$ ppl-sig proposal-info 6NuA13zJvhA7GU86LzzrhwTVYCbkKqzvpwH
TzkejGDPg

thread 'main' panicked at 'called `Result::unwrap()` on an `Err` value: ClientError { request: None, kind: RpcError(ForUser("AccountNotFound: pubkey=6NuA13zJvhA7GU86LzzrhwTVYCbkKqzvpwHTzkejGDPg")) }', multi-sig/cli/src/main.rs:421:81
note: run with `RUST_BACKTRACE=1` environment variable to display a backtrace
```

As you can see, the proposal account is no longer available for searching.
