# Vote proposal



```
$ ppl-sig vote --proposal 6NuA13zJvhA7GU86LzzrhwTVYCbkKqzvpwHTzk
ejGDPg --signer . /id2.json 
voting for a proposal 6NuA13zJvhA7GU86LzzrhwTVYCbkKqzvpwHTzk
ejGDPg

Signature: RpKHvSFwiZhsjWxAmHrxcyREXCSnZtmzPm56z6VsX9R9XuLdHnaB7wk5N9it5JsJPUEVfkNJ2SSB9Hue1qfJH26
```

After the proposal is initialized, the signature address in the multi-signature account, can vote on the transaction proposal.&#x20;

Each valid signature address can only vote once, and multiple votes are not valid.&#x20;

After voting, the information of the transaction proposal will be changed.

Use the proposal-info instruction to query again.

```
$ ppl-sig proposal-info 6NuA13zJvhA7GU86LzzrhwTVYCbkKqzvpwHTzkejGDPg

Initiator:3AGrFExFviu6Gg6mUgdf4BUbi178Cg43ysbmYc5gbCYv 
Proposal summary: "a transfer b 10 put" 
----------------Voted account----------------

3AGrFExFviu6Gg6mUgdf4BUbi178Cg43ysbmYc5gbCYv
4vxfArUy35WeLMwBCe8FEUjWq7gXPpRyF8A85EL44CB8
```
